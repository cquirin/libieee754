#include <stdint.h>
#ifndef IEEE754_2008_H
#define IEEE754_2008_H

typedef float ieee754binary32;
typedef double ieee754binary64;

enum ieee754RadixEnum {
	BINARY	= 2,
	DECIMAL	= 10
};
typedef enum ieee754RadixEnum ieee754Radix;

enum ieee754FPClassEnum {
	signalingNaN,
	quietNaN,
	negativeInfinity,
	negativeNormal,
	negativeSubnormal,
	negativeZero,
	positiveZero,
	positiveSubnormal,
	positiveNormal,
	positiveInfinity
};
typedef enum ieee754FPClassEnum ieee754FPClass;

enum ieee754ExceptionEnum {
  invalidOperation   = 1,
  divideByZero	     = 2,
  overflow	     = 4,
  underflow	     = 8,
  inexact            = 16
};

typedef enum ieee754ExceptionEnum ieee754Exception;

typedef uint32_t ieee754ExceptionGroupType;

//add exceptions, structures for convertion to char, perhaps flags & constants

//Addition
extern ieee754binary32 ieee754Binary32AddBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern ieee754binary32 ieee754Binary32AddBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern ieee754binary32 ieee754Binary32AddBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern ieee754binary32 ieee754Binary32AddBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);
extern ieee754binary64 ieee754Binary64AddBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern ieee754binary64 ieee754Binary64AddBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern ieee754binary64 ieee754Binary64AddBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern ieee754binary64 ieee754Binary64AddBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

//Subtraction
extern ieee754binary32 ieee754Binary32SubBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern ieee754binary32 ieee754Binary32SubBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern ieee754binary32 ieee754Binary32SubBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern ieee754binary32 ieee754Binary32SubBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);
extern ieee754binary64 ieee754Binary64SubBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern ieee754binary64 ieee754Binary64SubBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern ieee754binary64 ieee754Binary64SubBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern ieee754binary64 ieee754Binary64SubBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

//Multiplication
extern ieee754binary32 ieee754Binary32MultBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern ieee754binary32 ieee754Binary32MultBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern ieee754binary32 ieee754Binary32MultBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern ieee754binary32 ieee754Binary32MultBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);
extern ieee754binary64 ieee754Binary64MultBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern ieee754binary64 ieee754Binary64MultBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern ieee754binary64 ieee754Binary64MultBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern ieee754binary64 ieee754Binary64MultBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

//Division
extern ieee754binary32 ieee754Binary32DivBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern ieee754binary32 ieee754Binary32DivBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern ieee754binary32 ieee754Binary32DivBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern ieee754binary32 ieee754Binary32DivBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);
extern ieee754binary64 ieee754Binary64DivBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern ieee754binary64 ieee754Binary64DivBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern ieee754binary64 ieee754Binary64DivBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern ieee754binary64 ieee754Binary64DivBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

//Square root
extern ieee754binary32 ieee754Binary32SqrtBinary32(ieee754binary32 b1);
extern ieee754binary32 ieee754Binary32SqrtBinary64(ieee754binary64 b1);
extern ieee754binary64 ieee754Binary64SqrtBinary32(ieee754binary32 b1);
extern ieee754binary64 ieee754Binary64SqrtBinary64(ieee754binary64 b1);

//Fused multiply add
extern ieee754binary32 ieee754Binary32FmaBinary32Binary32Binary32(ieee754binary32 b1, ieee754binary32 b2, ieee754binary32 b3);
extern ieee754binary32 ieee754Binary32FmaBinary32Binary32Binary64(ieee754binary32 b1, ieee754binary32 b2, ieee754binary64 b3);
extern ieee754binary32 ieee754Binary32FmaBinary32Binary64Binary32(ieee754binary32 b1, ieee754binary64 b2, ieee754binary32 b3);
extern ieee754binary32 ieee754Binary32FmaBinary32Binary64Binary64(ieee754binary32 b1, ieee754binary64 b2, ieee754binary64 b3);
extern ieee754binary32 ieee754Binary32FmaBinary64Binary32Binary32(ieee754binary64 b1, ieee754binary32 b2, ieee754binary32 b3);
extern ieee754binary32 ieee754Binary32FmaBinary64Binary32Binary64(ieee754binary64 b1, ieee754binary32 b2, ieee754binary64 b3);
extern ieee754binary32 ieee754Binary32FmaBinary64Binary64Binary32(ieee754binary64 b1, ieee754binary64 b2, ieee754binary32 b3);
extern ieee754binary32 ieee754Binary32FmaBinary64Binary64Binary64(ieee754binary64 b1, ieee754binary64 b2, ieee754binary64 b3);

extern ieee754binary64 ieee754Binary64FmaBinary32Binary32Binary32(ieee754binary32 b1, ieee754binary32 b2, ieee754binary32 b3);
extern ieee754binary64 ieee754Binary64FmaBinary32Binary32Binary64(ieee754binary32 b1, ieee754binary32 b2, ieee754binary64 b3);
extern ieee754binary64 ieee754Binary64FmaBinary32Binary64Binary32(ieee754binary32 b1, ieee754binary64 b2, ieee754binary32 b3);
extern ieee754binary64 ieee754Binary64FmaBinary32Binary64Binary64(ieee754binary32 b1, ieee754binary64 b2, ieee754binary64 b3);
extern ieee754binary64 ieee754Binary64FmaBinary64Binary32Binary32(ieee754binary64 b1, ieee754binary32 b2, ieee754binary32 b3);
extern ieee754binary64 ieee754Binary64FmaBinary64Binary32Binary64(ieee754binary64 b1, ieee754binary32 b2, ieee754binary64 b3);
extern ieee754binary64 ieee754Binary64FmaBinary64Binary64Binary32(ieee754binary64 b1, ieee754binary64 b2, ieee754binary32 b3);
extern ieee754binary64 ieee754Binary64FmaBinary64Binary64Binary64(ieee754binary64 b1, ieee754binary64 b2, ieee754binary64 b3);

//Quiet comparisons
extern int ieee754CompareQuietEqualBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareQuietEqualBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareQuietEqualBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareQuietEqualBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754CompareQuietNotEqualBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareQuietNotEqualBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareQuietNotEqualBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareQuietNotEqualBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754CompareQuietGreaterBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareQuietGreaterBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareQuietGreaterBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareQuietGreaterBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754CompareQuietGreaterEqualBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareQuietGreaterEqualBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareQuietGreaterEqualBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareQuietGreaterEqualBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754CompareQuietLessBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareQuietLessBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareQuietLessBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareQuietLessBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754CompareQuietLessEqualBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareQuietLessEqualBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareQuietLessEqualBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareQuietLessEqualBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754CompareQuietUnorderedBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareQuietUnorderedBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareQuietUnorderedBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareQuietUnorderedBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754CompareQuietNotGreaterBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareQuietNotGreaterBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareQuietNotGreaterBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareQuietNotGreaterBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754CompareQuietLessUnorderedBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareQuietLessUnorderedBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareQuietLessUnorderedBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareQuietLessUnorderedBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754CompareQuietNotLessBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareQuietNotLessBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareQuietNotLessBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareQuietNotLessBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754CompareQuietGreaterUnorderedBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareQuietGreaterUnorderedBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareQuietGreaterUnorderedBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareQuietGreaterUnorderedBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754CompareQuietOrderedBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareQuietOrderedBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareQuietOrderedBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareQuietOrderedBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

//Signaling comparisons
extern int ieee754CompareSignalingEqualBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareSignalingEqualBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareSignalingEqualBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareSignalingEqualBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754CompareSignalingNotEqualBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareSignalingNotEqualBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareSignalingNotEqualBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareSignalingNotEqualBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754CompareSignalingGreaterBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareSignalingGreaterBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareSignalingGreaterBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareSignalingGreaterBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754CompareSignalingGreaterEqualBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareSignalingGreaterEqualBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareSignalingGreaterEqualBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareSignalingGreaterEqualBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754CompareSignalingLessBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareSignalingLessBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareSignalingLessBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareSignalingLessBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754CompareSignalingLessEqualBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareSignalingLessEqualBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareSignalingLessEqualBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareSignalingLessEqualBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754CompareSignalingNotGreaterBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareSignalingNotGreaterBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareSignalingNotGreaterBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareSignalingNotGreaterBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754CompareSignalingLessUnorderedBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareSignalingLessUnorderedBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareSignalingLessUnorderedBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareSignalingLessUnorderedBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754CompareSignalingNotLessBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareSignalingNotLessBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareSignalingNotLessBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareSignalingNotLessBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754CompareSignalingGreaterUnorderedBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754CompareSignalingGreaterUnorderedBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2);
extern int ieee754CompareSignalingGreaterUnorderedBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2);
extern int ieee754CompareSignalingGreaterUnorderedBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

//General operations
extern ieee754FPClass ieee754ClassBinary32(ieee754binary32 b1);
extern ieee754FPClass ieee754ClassBinary64(ieee754binary64 b1);

extern int ieee754IsSignMinusBinary32(ieee754binary32 b1);
extern int ieee754IsSignMinusBinary64(ieee754binary64 b1);

extern int ieee754IsNormalBinary32(ieee754binary32 b1);
extern int ieee754IsNormalBinary64(ieee754binary64 b1);

extern int ieee754IsFiniteBinary32(ieee754binary32 b1);
extern int ieee754IsFiniteBinary64(ieee754binary64 b1);

extern int ieee754IsZeroBinary32(ieee754binary32 b1);
extern int ieee754IsZeroBinary64(ieee754binary64 b1);

extern int ieee754IsSubnormalBinary32(ieee754binary32 b1);
extern int ieee754IsSubnormalBinary64(ieee754binary64 b1);

extern int ieee754IsInfiniteBinary32(ieee754binary32 b1);
extern int ieee754IsInfiniteBinary64(ieee754binary64 b1);

extern int ieee754IsNaNBinary32(ieee754binary32 b1);
extern int ieee754IsNaNBinary64(ieee754binary64 b1);

extern int ieee754IsSignalingBinary32(ieee754binary32 b1);
extern int ieee754IsSignalingBinary64(ieee754binary64 b1);

extern int ieee754IsCanonicalBinary32(ieee754binary32 b1);
extern int ieee754IsCanonicalBinary64(ieee754binary64 b1);

extern ieee754Radix ieee754RadixBinary32(ieee754binary32 b1);
extern ieee754Radix ieee754RadixBinary64(ieee754binary64 b1);

extern int ieee754TotalOrderBinary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754TotalOrderBinary64(ieee754binary64 b1, ieee754binary64 b2);

extern int ieee754TotalOrderMagBinary32(ieee754binary32 b1, ieee754binary32 b2);
extern int ieee754TotalOrderMagBinary64(ieee754binary64 b1, ieee754binary64 b2);

//Conformance predicates
extern int ieee754Is754version1985();
extern int ieee754Is754version2008();

//Sign-bit operations

extern ieee754binary32 ieee754CopyBinary32(ieee754binary32 b1);
extern ieee754binary64 ieee754CopyBinary64(ieee754binary64 b1);

extern ieee754binary32 ieee754NegateBinary32(ieee754binary32 b1);
extern ieee754binary64 ieee754NegateBinary64(ieee754binary64 b1);

extern ieee754binary32 ieee754AbsBinary32(ieee754binary32 b1);
extern ieee754binary64 ieee754AbsBinary64(ieee754binary64 b1);

extern ieee754binary32 ieee754CopySignBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2);
extern ieee754binary64 ieee754CopySignBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2);

//Conversions
extern ieee754binary32 ieee754Binary32ConvertFormatBinary64(ieee754binary64 b1);
extern ieee754binary64 ieee754Binary64ConvertFormatBinary32(ieee754binary32 b1);

extern ieee754binary32 ieee754Binary32ConvertFromDecimalCharacter(char* a);
extern ieee754binary64 ieee754Binary64ConvertFromDecimalCharacter(char* a);

extern void ieee754ConvertToDecimalCharacterBinary32(char* s, ieee754binary32 b1);
extern void ieee754ConvertToDecimalCharacterBinary64(char* s, ieee754binary64 b1);

extern ieee754binary32 ieee754Binary32ConvertFromHexCharacter(char* a);
extern ieee754binary64 ieee754Binary64ConvertFromHexCharacter(char* a);

extern void ieee754ConvertToHexCharacterBinary32(char* s, ieee754binary32 b1);
extern void ieee754ConvertToHexCharacterBinary64(char* s, ieee754binary64 b1);

extern ieee754binary32 ieee754Binary32ConvertFromUInt8(uint8_t intParam);
extern ieee754binary64 ieee754Binary64ConvertFromUInt8(uint8_t intParam);

extern ieee754binary32 ieee754Binary32ConvertFromUInt16(uint16_t intParam);
extern ieee754binary64 ieee754Binary64ConvertFromUInt16(uint16_t intParam);

extern ieee754binary32 ieee754Binary32ConvertFromUInt32(uint32_t intParam);
extern ieee754binary64 ieee754Binary64ConvertFromUInt32(uint32_t intParam);

extern ieee754binary32 ieee754Binary32ConvertFromUInt64(uint64_t intParam);
extern ieee754binary64 ieee754Binary64ConvertFromUInt64(uint64_t intParam);

extern ieee754binary32 ieee754Binary32ConvertFromInt8(int8_t intParam);
extern ieee754binary64 ieee754Binary64ConvertFromInt8(int8_t intParam);

extern ieee754binary32 ieee754Binary32ConvertFromInt16(int16_t intParam);
extern ieee754binary64 ieee754Binary64ConvertFromInt16(int16_t intParam);

extern ieee754binary32 ieee754Binary32ConvertFromInt32(int32_t intParam);
extern ieee754binary64 ieee754Binary64ConvertFromInt32(int32_t intParam);

extern ieee754binary32 ieee754Binary32ConvertFromInt64(int64_t intParam);
extern ieee754binary64 ieee754Binary64ConvertFromInt64(int64_t intParam);

//ConvertToIntegerTiesToEven
extern uint8_t ieee754UInt8ConvertToIntegerTiesToEvenBinary32(ieee754binary32 b);
extern uint8_t ieee754UInt8ConvertToIntegerTiesToEvenBinary64(ieee754binary64 b);

extern uint16_t ieee754UInt16ConvertToIntegerTiesToEvenBinary32(ieee754binary32 b);
extern uint16_t ieee754UInt16ConvertToIntegerTiesToEvenBinary64(ieee754binary64 b);

extern uint32_t ieee754UInt32ConvertToIntegerTiesToEvenBinary32(ieee754binary32 b);
extern uint32_t ieee754UInt32ConvertToIntegerTiesToEvenBinary64(ieee754binary64 b);

extern uint64_t ieee754UInt64ConvertToIntegerTiesToEvenBinary32(ieee754binary32 b);
extern uint64_t ieee754UInt64ConvertToIntegerTiesToEvenBinary64(ieee754binary64 b);

extern int8_t ieee754Int8ConvertToIntegerTiesToEvenBinary32(ieee754binary32 b);
extern int8_t ieee754Int8ConvertToIntegerTiesToEvenBinary64(ieee754binary64 b);

extern int16_t ieee754Int16ConvertToIntegerTiesToEvenBinary32(ieee754binary32 b);
extern int16_t ieee754Int16ConvertToIntegerTiesToEvenBinary64(ieee754binary64 b);

extern int32_t ieee754Int32ConvertToIntegerTiesToEvenBinary32(ieee754binary32 b);
extern int32_t ieee754Int32ConvertToIntegerTiesToEvenBinary64(ieee754binary64 b);

extern int64_t ieee754Int64ConvertToIntegerTiesToEvenBinary32(ieee754binary32 b);
extern int64_t ieee754Int64ConvertToIntegerTiesToEvenBinary64(ieee754binary64 b);

//ConvertToIntegerTowardZero
extern uint8_t ieee754UInt8ConvertToIntegerTowardZeroBinary32(ieee754binary32 b);
extern uint8_t ieee754UInt8ConvertToIntegerTowardZeroBinary64(ieee754binary64 b);

extern uint16_t ieee754UInt16ConvertToIntegerTowardZeroBinary32(ieee754binary32 b);
extern uint16_t ieee754UInt16ConvertToIntegerTowardZeroBinary64(ieee754binary64 b);

extern uint32_t ieee754UInt32ConvertToIntegerTowardZeroBinary32(ieee754binary32 b);
extern uint32_t ieee754UInt32ConvertToIntegerTowardZeroBinary64(ieee754binary64 b);

extern uint64_t ieee754UInt64ConvertToIntegerTowardZeroBinary32(ieee754binary32 b);
extern uint64_t ieee754UInt64ConvertToIntegerTowardZeroBinary64(ieee754binary64 b);

extern int8_t ieee754Int8ConvertToIntegerTowardZeroBinary32(ieee754binary32 b);
extern int8_t ieee754Int8ConvertToIntegerTowardZeroBinary64(ieee754binary64 b);

extern int16_t ieee754Int16ConvertToIntegerTowardZeroBinary32(ieee754binary32 b);
extern int16_t ieee754Int16ConvertToIntegerTowardZeroBinary64(ieee754binary64 b);

extern int32_t ieee754Int32ConvertToIntegerTowardZeroBinary32(ieee754binary32 b);
extern int32_t ieee754Int32ConvertToIntegerTowardZeroBinary64(ieee754binary64 b);

extern int64_t ieee754Int64ConvertToIntegerTowardZeroBinary32(ieee754binary32 b);
extern int64_t ieee754Int64ConvertToIntegerTowardZeroBinary64(ieee754binary64 b);

//ConvertToIntegerTowardPositive
extern uint8_t ieee754UInt8ConvertToIntegerTowardPositiveBinary32(ieee754binary32 b);
extern uint8_t ieee754UInt8ConvertToIntegerTowardPositiveBinary64(ieee754binary64 b);

extern uint16_t ieee754UInt16ConvertToIntegerTowardPositiveBinary32(ieee754binary32 b);
extern uint16_t ieee754UInt16ConvertToIntegerTowardPositiveBinary64(ieee754binary64 b);

extern uint32_t ieee754UInt32ConvertToIntegerTowardPositiveBinary32(ieee754binary32 b);
extern uint32_t ieee754UInt32ConvertToIntegerTowardPositiveBinary64(ieee754binary64 b);

extern uint64_t ieee754UInt64ConvertToIntegerTowardPositiveBinary32(ieee754binary32 b);
extern uint64_t ieee754UInt64ConvertToIntegerTowardPositiveBinary64(ieee754binary64 b);

extern int8_t ieee754Int8ConvertToIntegerTowardPositiveBinary32(ieee754binary32 b);
extern int8_t ieee754Int8ConvertToIntegerTowardPositiveBinary64(ieee754binary64 b);

extern int16_t ieee754Int16ConvertToIntegerTowardPositiveBinary32(ieee754binary32 b);
extern int16_t ieee754Int16ConvertToIntegerTowardPositiveBinary64(ieee754binary64 b);

extern int32_t ieee754Int32ConvertToIntegerTowardPositiveBinary32(ieee754binary32 b);
extern int32_t ieee754Int32ConvertToIntegerTowardPositiveBinary64(ieee754binary64 b);

extern int64_t ieee754Int64ConvertToIntegerTowardPositiveBinary32(ieee754binary32 b);
extern int64_t ieee754Int64ConvertToIntegerTowardPositiveBinary64(ieee754binary64 b);

//ConvertToIntegerTowardNegative
extern uint8_t ieee754UInt8ConvertToIntegerTowardNegativeBinary32(ieee754binary32 b);
extern uint8_t ieee754UInt8ConvertToIntegerTowardNegativeBinary64(ieee754binary64 b);

extern uint16_t ieee754UInt16ConvertToIntegerTowardNegativeBinary32(ieee754binary32 b);
extern uint16_t ieee754UInt16ConvertToIntegerTowardNegativeBinary64(ieee754binary64 b);

extern uint32_t ieee754UInt32ConvertToIntegerTowardNegativeBinary32(ieee754binary32 b);
extern uint32_t ieee754UInt32ConvertToIntegerTowardNegativeBinary64(ieee754binary64 b);

extern uint64_t ieee754UInt64ConvertToIntegerTowardNegativeBinary32(ieee754binary32 b);
extern uint64_t ieee754UInt64ConvertToIntegerTowardNegativeBinary64(ieee754binary64 b);

extern int8_t ieee754Int8ConvertToIntegerTowardNegativeBinary32(ieee754binary32 b);
extern int8_t ieee754Int8ConvertToIntegerTowardNegativeBinary64(ieee754binary64 b);

extern int16_t ieee754Int16ConvertToIntegerTowardNegativeBinary32(ieee754binary32 b);
extern int16_t ieee754Int16ConvertToIntegerTowardNegativeBinary64(ieee754binary64 b);

extern int32_t ieee754Int32ConvertToIntegerTowardNegativeBinary32(ieee754binary32 b);
extern int32_t ieee754Int32ConvertToIntegerTowardNegativeBinary64(ieee754binary64 b);

extern int64_t ieee754Int64ConvertToIntegerTowardNegativeBinary32(ieee754binary32 b);
extern int64_t ieee754Int64ConvertToIntegerTowardNegativeBinary64(ieee754binary64 b);

//ConvertToIntegerTiesToAway
extern uint8_t ieee754UInt8ConvertToIntegerTiesToAwayBinary32(ieee754binary32 b);
extern uint8_t ieee754UInt8ConvertToIntegerTiesToAwayBinary64(ieee754binary64 b);

extern uint16_t ieee754UInt16ConvertToIntegerTiesToAwayBinary32(ieee754binary32 b);
extern uint16_t ieee754UInt16ConvertToIntegerTiesToAwayBinary64(ieee754binary64 b);

extern uint32_t ieee754UInt32ConvertToIntegerTiesToAwayBinary32(ieee754binary32 b);
extern uint32_t ieee754UInt32ConvertToIntegerTiesToAwayBinary64(ieee754binary64 b);

extern uint64_t ieee754UInt64ConvertToIntegerTiesToAwayBinary32(ieee754binary32 b);
extern uint64_t ieee754UInt64ConvertToIntegerTiesToAwayBinary64(ieee754binary64 b);

extern int8_t ieee754Int8ConvertToIntegerTiesToAwayBinary32(ieee754binary32 b);
extern int8_t ieee754Int8ConvertToIntegerTiesToAwayBinary64(ieee754binary64 b);

extern int16_t ieee754Int16ConvertToIntegerTiesToAwayBinary32(ieee754binary32 b);
extern int16_t ieee754Int16ConvertToIntegerTiesToAwayBinary64(ieee754binary64 b);

extern int32_t ieee754Int32ConvertToIntegerTiesToAwayBinary32(ieee754binary32 b);
extern int32_t ieee754Int32ConvertToIntegerTiesToAwayBinary64(ieee754binary64 b);

extern int64_t ieee754Int64ConvertToIntegerTiesToAwayBinary32(ieee754binary32 b);
extern int64_t ieee754Int64ConvertToIntegerTiesToAwayBinary64(ieee754binary64 b);

//ConvertToIntegerExactTiesToEven
extern uint8_t ieee754UInt8ConvertToIntegerExactTiesToEvenBinary32(ieee754binary32 b);
extern uint8_t ieee754UInt8ConvertToIntegerExactTiesToEvenBinary64(ieee754binary64 b);

extern uint16_t ieee754UInt16ConvertToIntegerExactTiesToEvenBinary32(ieee754binary32 b);
extern uint16_t ieee754UInt16ConvertToIntegerExactTiesToEvenBinary64(ieee754binary64 b);

extern uint32_t ieee754UInt32ConvertToIntegerExactTiesToEvenBinary32(ieee754binary32 b);
extern uint32_t ieee754UInt32ConvertToIntegerExactTiesToEvenBinary64(ieee754binary64 b);

extern uint64_t ieee754UInt64ConvertToIntegerExactTiesToEvenBinary32(ieee754binary32 b);
extern uint64_t ieee754UInt64ConvertToIntegerExactTiesToEvenBinary64(ieee754binary64 b);

extern int8_t ieee754Int8ConvertToIntegerExactTiesToEvenBinary32(ieee754binary32 b);
extern int8_t ieee754Int8ConvertToIntegerExactTiesToEvenBinary64(ieee754binary64 b);

extern int16_t ieee754Int16ConvertToIntegerExactTiesToEvenBinary32(ieee754binary32 b);
extern int16_t ieee754Int16ConvertToIntegerExactTiesToEvenBinary64(ieee754binary64 b);

extern int32_t ieee754Int32ConvertToIntegerExactTiesToEvenBinary32(ieee754binary32 b);
extern int32_t ieee754Int32ConvertToIntegerExactTiesToEvenBinary64(ieee754binary64 b);

extern int64_t ieee754Int64ConvertToIntegerExactTiesToEvenBinary32(ieee754binary32 b);
extern int64_t ieee754Int64ConvertToIntegerExactTiesToEvenBinary64(ieee754binary64 b);

//ConvertToIntegerExactTowardZero
extern uint8_t ieee754UInt8ConvertToIntegerExactTowardZeroBinary32(ieee754binary32 b);
extern uint8_t ieee754UInt8ConvertToIntegerExactTowardZeroBinary64(ieee754binary64 b);

extern uint16_t ieee754UInt16ConvertToIntegerExactTowardZeroBinary32(ieee754binary32 b);
extern uint16_t ieee754UInt16ConvertToIntegerExactTowardZeroBinary64(ieee754binary64 b);

extern uint32_t ieee754UInt32ConvertToIntegerExactTowardZeroBinary32(ieee754binary32 b);
extern uint32_t ieee754UInt32ConvertToIntegerExactTowardZeroBinary64(ieee754binary64 b);

extern uint64_t ieee754UInt64ConvertToIntegerExactTowardZeroBinary32(ieee754binary32 b);
extern uint64_t ieee754UInt64ConvertToIntegerExactTowardZeroBinary64(ieee754binary64 b);

extern int8_t ieee754Int8ConvertToIntegerExactTowardZeroBinary32(ieee754binary32 b);
extern int8_t ieee754Int8ConvertToIntegerExactTowardZeroBinary64(ieee754binary64 b);

extern int16_t ieee754Int16ConvertToIntegerExactTowardZeroBinary32(ieee754binary32 b);
extern int16_t ieee754Int16ConvertToIntegerExactTowardZeroBinary64(ieee754binary64 b);

extern int32_t ieee754Int32ConvertToIntegerExactTowardZeroBinary32(ieee754binary32 b);
extern int32_t ieee754Int32ConvertToIntegerExactTowardZeroBinary64(ieee754binary64 b);

extern int64_t ieee754Int64ConvertToIntegerExactTowardZeroBinary32(ieee754binary32 b);
extern int64_t ieee754Int64ConvertToIntegerExactTowardZeroBinary64(ieee754binary64 b);

//ConvertToIntegerExactTowardPositive
extern uint8_t ieee754UInt8ConvertToIntegerExactTowardPositiveBinary32(ieee754binary32 b);
extern uint8_t ieee754UInt8ConvertToIntegerExactTowardPositiveBinary64(ieee754binary64 b);

extern uint16_t ieee754UInt16ConvertToIntegerExactTowardPositiveBinary32(ieee754binary32 b);
extern uint16_t ieee754UInt16ConvertToIntegerExactTowardPositiveBinary64(ieee754binary64 b);

extern uint32_t ieee754UInt32ConvertToIntegerExactTowardPositiveBinary32(ieee754binary32 b);
extern uint32_t ieee754UInt32ConvertToIntegerExactTowardPositiveBinary64(ieee754binary64 b);

extern uint64_t ieee754UInt64ConvertToIntegerExactTowardPositiveBinary32(ieee754binary32 b);
extern uint64_t ieee754UInt64ConvertToIntegerExactTowardPositiveBinary64(ieee754binary64 b);

extern int8_t ieee754Int8ConvertToIntegerExactTowardPositiveBinary32(ieee754binary32 b);
extern int8_t ieee754Int8ConvertToIntegerExactTowardPositiveBinary64(ieee754binary64 b);

extern int16_t ieee754Int16ConvertToIntegerExactTowardPositiveBinary32(ieee754binary32 b);
extern int16_t ieee754Int16ConvertToIntegerExactTowardPositiveBinary64(ieee754binary64 b);

extern int32_t ieee754Int32ConvertToIntegerExactTowardPositiveBinary32(ieee754binary32 b);
extern int32_t ieee754Int32ConvertToIntegerExactTowardPositiveBinary64(ieee754binary64 b);

extern int64_t ieee754Int64ConvertToIntegerExactTowardPositiveBinary32(ieee754binary32 b);
extern int64_t ieee754Int64ConvertToIntegerExactTowardPositiveBinary64(ieee754binary64 b);

//ConvertToIntegerExactTowardNegative
extern uint8_t ieee754UInt8ConvertToIntegerExactTowardNegativeBinary32(ieee754binary32 b);
extern uint8_t ieee754UInt8ConvertToIntegerExactTowardNegativeBinary64(ieee754binary64 b);

extern uint16_t ieee754UInt16ConvertToIntegerExactTowardNegativeBinary32(ieee754binary32 b);
extern uint16_t ieee754UInt16ConvertToIntegerExactTowardNegativeBinary64(ieee754binary64 b);

extern uint32_t ieee754UInt32ConvertToIntegerExactTowardNegativeBinary32(ieee754binary32 b);
extern uint32_t ieee754UInt32ConvertToIntegerExactTowardNegativeBinary64(ieee754binary64 b);

extern uint64_t ieee754UInt64ConvertToIntegerExactTowardNegativeBinary32(ieee754binary32 b);
extern uint64_t ieee754UInt64ConvertToIntegerExactTowardNegativeBinary64(ieee754binary64 b);

extern int8_t ieee754Int8ConvertToIntegerExactTowardNegativeBinary32(ieee754binary32 b);
extern int8_t ieee754Int8ConvertToIntegerExactTowardNegativeBinary64(ieee754binary64 b);

extern int16_t ieee754Int16ConvertToIntegerExactTowardNegativeBinary32(ieee754binary32 b);
extern int16_t ieee754Int16ConvertToIntegerExactTowardNegativeBinary64(ieee754binary64 b);

extern int32_t ieee754Int32ConvertToIntegerExactTowardNegativeBinary32(ieee754binary32 b);
extern int32_t ieee754Int32ConvertToIntegerExactTowardNegativeBinary64(ieee754binary64 b);

extern int64_t ieee754Int64ConvertToIntegerExactTowardNegativeBinary32(ieee754binary32 b);
extern int64_t ieee754Int64ConvertToIntegerExactTowardNegativeBinary64(ieee754binary64 b);

//ConvertToIntegerExactTiesToAway
extern uint8_t ieee754UInt8ConvertToIntegerExactTiesToAwayBinary32(ieee754binary32 b);
extern uint8_t ieee754UInt8ConvertToIntegerExactTiesToAwayBinary64(ieee754binary64 b);

extern uint16_t ieee754UInt16ConvertToIntegerExactTiesToAwayBinary32(ieee754binary32 b);
extern uint16_t ieee754UInt16ConvertToIntegerExactTiesToAwayBinary64(ieee754binary64 b);

extern uint32_t ieee754UInt32ConvertToIntegerExactTiesToAwayBinary32(ieee754binary32 b);
extern uint32_t ieee754UInt32ConvertToIntegerExactTiesToAwayBinary64(ieee754binary64 b);

extern uint64_t ieee754UInt64ConvertToIntegerExactTiesToAwayBinary32(ieee754binary32 b);
extern uint64_t ieee754UInt64ConvertToIntegerExactTiesToAwayBinary64(ieee754binary64 b);

extern int8_t ieee754Int8ConvertToIntegerExactTiesToAwayBinary32(ieee754binary32 b);
extern int8_t ieee754Int8ConvertToIntegerExactTiesToAwayBinary64(ieee754binary64 b);

extern int16_t ieee754Int16ConvertToIntegerExactTiesToAwayBinary32(ieee754binary32 b);
extern int16_t ieee754Int16ConvertToIntegerExactTiesToAwayBinary64(ieee754binary64 b);

extern int32_t ieee754Int32ConvertToIntegerExactTiesToAwayBinary32(ieee754binary32 b);
extern int32_t ieee754Int32ConvertToIntegerExactTiesToAwayBinary64(ieee754binary64 b);

extern int64_t ieee754Int64ConvertToIntegerExactTiesToAwayBinary32(ieee754binary32 b);
extern int64_t ieee754Int64ConvertToIntegerExactTiesToAwayBinary64(ieee754binary64 b);

//logBFormat
extern ieee754binary32 ieee754Binary32ScaleBBinary32Int(ieee754binary32 b, int32_t iparam);
extern ieee754binary64 ieee754Binary64ScaleBBinary64Int(ieee754binary64 b, int32_t iparam);

extern ieee754binary32 ieee754Binary32ScaleBBinary32Binary32(ieee754binary32 b, ieee754binary32);
extern ieee754binary64 ieee754Binary64ScaleBBinary64Binary32(ieee754binary64 b, ieee754binary32);

extern int32_t ieee754IntLogBBinary32(ieee754binary32 b);
extern int32_t ieee754IntLogBBinary64(ieee754binary64 b);

extern ieee754binary32 ieee754Binary32LogBBinary32(ieee754binary32 b);
extern ieee754binary64 ieee754Binary64LogBBinary64(ieee754binary64 b);

//Roundings (homogeneous)
extern ieee754binary32 ieee754Binary32RoundToIntegralTiesToEvenBinary32(ieee754binary32 b);
extern ieee754binary64 ieee754Binary64RoundToIntegralTiesToEvenBinary64(ieee754binary64 b);

extern ieee754binary32 ieee754Binary32RoundToIntegralTiesToAwayBinary32(ieee754binary32 b);
extern ieee754binary64 ieee754Binary64RoundToIntegralTiesToAwayBinary64(ieee754binary64 b);

extern ieee754binary32 ieee754Binary32RoundToIntegralTowardZeroBinary32(ieee754binary32 b);
extern ieee754binary64 ieee754Binary64RoundToIntegralTowardZeroBinary64(ieee754binary64 b);

extern ieee754binary32 ieee754Binary32RoundToIntegralTowardPositiveBinary32(ieee754binary32 b);
extern ieee754binary64 ieee754Binary64RoundToIntegralTowardPositiveBinary64(ieee754binary64 b);

extern ieee754binary32 ieee754Binary32RoundToIntegralTowardNegativeBinary32(ieee754binary32 b);
extern ieee754binary64 ieee754Binary64RoundToIntegralTowardNegativeBinary64(ieee754binary64 b);

extern ieee754binary32 ieee754Binary32RoundToIntegralExactBinary32(ieee754binary32 b);
extern ieee754binary64 ieee754Binary64RoundToIntegralExactBinary64(ieee754binary64 b);

//other general homogeneous
extern ieee754binary32 ieee754Binary32NextUpBinary32(ieee754binary32);
extern ieee754binary64 ieee754Binary64NextUpBinary64(ieee754binary64);

extern ieee754binary32 ieee754Binary32NextDownBinary32(ieee754binary32);
extern ieee754binary64 ieee754Binary64NextDownBinary64(ieee754binary64);

extern ieee754binary32 ieee754Binary32MinNumBinary32Binary32(ieee754binary32, ieee754binary32);
extern ieee754binary64 ieee754Binary64MinNumBinary64Binary64(ieee754binary64, ieee754binary64);

extern ieee754binary32 ieee754Binary32MaxNumBinary32Binary32(ieee754binary32, ieee754binary32);
extern ieee754binary64 ieee754Binary64MaxNumBinary64Binary64(ieee754binary64, ieee754binary64);

extern ieee754binary32 ieee754Binary32MinNumMagBinary32Binary32(ieee754binary32, ieee754binary32);
extern ieee754binary64 ieee754Binary64MinNumMagBinary64Binary64(ieee754binary64, ieee754binary64);

extern ieee754binary32 ieee754Binary32MaxNumMagBinary32Binary32(ieee754binary32, ieee754binary32);
extern ieee754binary64 ieee754Binary64MaxNumMagBinary64Binary64(ieee754binary64, ieee754binary64);

extern ieee754binary32 ieee754Binary32RemainderBinary32Binary32(ieee754binary32, ieee754binary32);
extern ieee754binary64 ieee754Binary64RemainderBinary64Binary64(ieee754binary64, ieee754binary64);

//Flags
extern void ieee754LowerFlags(ieee754ExceptionGroupType exceptionGroup);
extern void ieee754RaiseFlags(ieee754ExceptionGroupType exceptionGroup);
extern void ieee754TestFlags(ieee754ExceptionGroupType exceptionGroup);
extern void ieee754TestSavedFlags(ieee754ExceptionGroupType exceptionGroup);
extern void ieee754RestoreFlags(ieee754ExceptionGroupType exceptionGroup);
extern void ieee754SaveAllFlags(ieee754ExceptionGroupType exceptionGroup);

#endif
