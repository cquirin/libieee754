//This is a file for implementation general, conformance and sign-bit operations, declared in IEEE754-2008 standard

#include <ieee754-2008.h>
#include <ieee754-2008-macros.h>
#include <inttypes.h>
#include <stdio.h>
#include <fenv.h>

//sign-bit operations
ieee754binary32 ieee754CopySignBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
	ieee754binary32 b32_res;
	COPY_SIGN32(b1, b2, &b32_res);	
	return b32_res;
}
ieee754binary64 ieee754CopySignBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
	ieee754binary64 b64_res;
	COPY_SIGN64(b1, b2, &b64_res);	
	return b64_res;
}

ieee754binary32 ieee754NegateBinary32(ieee754binary32 b1) {
	return NEGATE(b1);
}
ieee754binary64 ieee754NegateBinary64(ieee754binary64 b1) {
	return NEGATE(b1);
}

ieee754binary32 ieee754AbsBinary32(ieee754binary32 b1) {
  uint32_t absvalue;
  ABS_QUIET32(b1, &absvalue);
  return absvalue;
}
ieee754binary64 ieee754AbsBinary64(ieee754binary64 b1) {
  uint64_t absvalue;
  ABS_QUIET64(b1, &absvalue);
  return absvalue;
}

//General operations
int ieee754IsFiniteBinary32(ieee754binary32 b1) {
	return IS_FINITE32(b1);	
}
int ieee754IsFiniteBinary64(ieee754binary64 b1) {
	return IS_FINITE64(b1);
}

int ieee754IsNormalBinary32(ieee754binary32 b1) {
	return IS_NORMAL32(b1);
}
int ieee754IsNormalBinary64(ieee754binary64 b1) {
	return IS_NORMAL32(b1);
}

int ieee754IsNaNBinary32(ieee754binary32 b1) {
  int nan32;
  IS_NAN_QUIET32(b1, &nan32);
  return nan32;
}

int ieee754IsNaNBinary64(ieee754binary64 b1) {
  int nan64;
  IS_NAN_QUIET64(b1, &nan64);
  return nan64;
}

int ieee754IsInfiniteBinary32(ieee754binary32 b1) {
	return IS_INF32(b1);
}
int ieee754IsInfiniteBinary64(ieee754binary64 b1) {
	return IS_INF64(b1);
}

int ieee754IsSignMinusBinary32(ieee754binary32 b1) {
  int sign;
  IS_SIGN_MINUS_QUIET32(b1, &sign);
  return sign != 0;
}
int ieee754IsSignMinusBinary64(ieee754binary64 b1){
 int sign;
 IS_SIGN_MINUS_QUIET64(b1, &sign);
 return sign != 0;
}

int ieee754IsZeroBinary32(ieee754binary32 b1) {
	return IS_ZERO(b1);
}
int ieee754IsZeroBinary64(ieee754binary64 b1) {
	return IS_ZERO(b1);
}

ieee754Radix ieee754RadixBinary32(ieee754binary32 b1) {
	return BINARY;
}
ieee754Radix ieee754RadixBinary64(ieee754binary64 b1) {
	return BINARY;
}

//logBFormat
int32_t ieee754IntLogBBinary32(ieee754binary32 b1) {
	return ILOGB32(b1);
}
int32_t ieee754IntLogBBinary64(ieee754binary64 b1) {
	return ILOGB64(b1);
}

ieee754binary32 ieee754Binary32ScaleBBinary32Int(ieee754binary32 b, int32_t iparam) {
	return SCALEB32(b, iparam);
}
ieee754binary64 ieee754Binary64ScaleBBinary64Int(ieee754binary64 b, int32_t iparam) {
	return SCALEB64(b, iparam);
}

int ieee754TotalOrderBinary32(ieee754binary32 b1, ieee754binary32 b2) {
  binary32wrapper b1wrap, b2wrap;
  int signb1 = 0, signb2 = 0;
  int b1NaN = 0, b2NaN = 0;
  IS_NAN_QUIET32(b1, &b1NaN);
  IS_NAN_QUIET32(b2, &b2NaN);
  b1wrap.bin32 = b1;
  b2wrap.bin32 = b2;
  IS_SIGN_MINUS_QUIET32(b1wrap.int32, &signb1);
  IS_SIGN_MINUS_QUIET32(b2wrap.int32, &signb2);
  
  if (b1wrap.int32 == b2wrap.int32) {    
    return 1;
  }
  if (!b1NaN && !b2NaN) { //Not NaN case    
    if (b1 != b2) { 
      return b1 < b2;   
    } else { //for zero only
      if (b1 == 0) {
	return b1wrap.int32 > b2wrap.int32;
      } 
    }
  } else { //NaN case
    if (b1NaN && b2NaN) {           
      //Both NaN case          
      ABS_QUIET32(b1, &b1wrap.int32);
      ABS_QUIET32(b2, &b2wrap.int32);         
      if (b1wrap.int32 == b2wrap.int32) {//same NaNs of different signs. Negative should be smaller then positive	
	b1wrap.bin32 = b1;
	b2wrap.bin32 = b2;	
	return b1wrap.int32 > b2wrap.int32;
      } else {		
	b1wrap.bin32 = b1;
	b2wrap.bin32 = b2;	
	if (!signb1 ^ signb2) {	  		 	  
	  return b1wrap.int32 > b2wrap.int32 * signb1;
	}
	//different NaNs of different signs	
    	else {	 
	  return b1wrap.int32 > b2wrap.int32;	     
	}
      }               
      return -1;
    }
    if (b1NaN) return signb1 != 0;
    if (b2NaN) return signb2 == 0;
  }  
  return -1;
}

int ieee754TotalOrderBinary64(ieee754binary64 b1, ieee754binary64 b2) {
  binary64wrapper b1wrap, b2wrap;
  int signb1 = 0, signb2 = 0;
  int b1NaN = 0, b2NaN = 0;
  IS_NAN_QUIET64(b1, &b1NaN);
  IS_NAN_QUIET64(b2, &b2NaN);
  b1wrap.bin64 = b1;
  b2wrap.bin64 = b2;
  IS_SIGN_MINUS_QUIET64(b1wrap.int64, &signb1);
  IS_SIGN_MINUS_QUIET64(b2wrap.int64, &signb2);
  
  if (b1wrap.int64 == b2wrap.int64) {   
    return 1;
  }
  if (!b1NaN && !b2NaN) { //Not NaN case    
    if (b1 != b2) { 
      return b1 < b2;   
    } else { //for zero only
      if (b1 == 0) {
	return b1wrap.int64 > b2wrap.int64;
      } 
    }
  } else { //NaN case
    if (b1NaN && b2NaN) {           
      //Both NaN case          
      ABS_QUIET32(b1, &b1wrap.int64);
      ABS_QUIET32(b2, &b2wrap.int64);         
      if (b1wrap.int64 == b2wrap.int64) {//same NaNs of different signs. Negative should be smaller then positive	
	b1wrap.bin64 = b1;
	b2wrap.bin64 = b2;	
	return b1wrap.int64 > b2wrap.int64;
      } else {		
	b1wrap.bin64 = b1;
	b2wrap.bin64 = b2;	
	if (!signb1 ^ signb2) {	  		 	  
	  return b1wrap.int64 > b2wrap.int64 * signb1;
	}
	//different NaNs of different signs	
    	else {	 
	  return b1wrap.int64 > b2wrap.int64;	     
	}
      }               
      return -1;
    }
    if (b1NaN) return signb1 != 0;
    if (b2NaN) return signb2 == 0;
  }  
  return -1;
}


int ieee754TotalOrderMagBinary32(ieee754binary32 b1, ieee754binary32 b2) {
  int32_t abs1, abs2;
  binary32wrapper b1w, b2w;
  ABS_QUIET32(b1, &abs1);
  ABS_QUIET32(b2, &abs2);
  b1w.int32 = abs1;
  b2w.int32 = abs2;
  return ieee754TotalOrderBinary32(b1w.bin32, b2w.bin32);
}

int ieee754TotalOrderMagBinary64(ieee754binary64 b1, ieee754binary64 b2) {
  int64_t abs1, abs2;
  binary64wrapper b1w, b2w;
  ABS_QUIET64(b1, &abs1);
  ABS_QUIET64(b2, &abs2);
  b1w.int64 = abs1;
  b2w.int64 = abs2;
  return ieee754TotalOrderBinary64(b1w.bin64, b2w.bin64);
}

