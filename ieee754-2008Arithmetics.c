//This is a file for implementation the basic arithmetics operations: +, -, *, /, sqrt, fma of IEEE754-2008 standard

#include <ieee754-2008.h>
#include <config.h>
#include <ieee754-2008-macros.h>
#include <math.h>

//Addition
ieee754binary32 ieee754Binary32AddBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
	return ADD_BINARY32(b1, b2);
}

//TODO reference is not implemented now!
ieee754binary32 ieee754Binary32AddBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
	ieee754binary64 b64;
	CONVERT_BIN32_TO_BIN64(&b64, b1);
	return ieee754Binary32AddBinary64Binary64(b64, b2);
}

//TODO reference is not implemented now!
ieee754binary32 ieee754Binary32AddBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
	ieee754binary64 b64;
	CONVERT_BIN32_TO_BIN64(&b64, b2);
	return ieee754Binary32AddBinary64Binary64(b1, b64);
}

//TODO implement later!!!
ieee754binary32 ieee754Binary32AddBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
	return 0;
}

ieee754binary64 ieee754Binary64AddBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
	ieee754binary64	b64;
	CONVERT_BIN32_TO_BIN64(&b64, b2);
	return ieee754Binary64AddBinary32Binary64(b1, b64);
}

ieee754binary64 ieee754Binary64AddBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
	ieee754binary64 b64;
	CONVERT_BIN32_TO_BIN64(&b64, b1);
	return ADD_BINARY64(b64, b2);
}

ieee754binary64 ieee754Binary64AddBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
	ieee754binary64 b64;
	CONVERT_BIN32_TO_BIN64(&b64, b2);
	return ADD_BINARY64(b1, b64);
}

ieee754binary64 ieee754Binary64AddBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
	return ADD_BINARY64(b1, b2);
}

ieee754binary32 ieee754Binary32SubBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
	return SUB_BINARY32(b1, b2);
}

//TODO reference is not implemented now!
ieee754binary32 ieee754Binary32SubBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
	ieee754binary64 b64;	
	CONVERT_BIN32_TO_BIN64(&b64, b1);
	return ieee754Binary64SubBinary32Binary64(b64, b2);
}

//TODO reference is not implemented now!
ieee754binary32 ieee754Binary32SubBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
	ieee754binary64 b64;	
	CONVERT_BIN32_TO_BIN64(&b64, b2);
	return ieee754Binary64SubBinary32Binary64(b1, b64);
}

//TODO implement later!!!
ieee754binary32 ieee754Binary32SubBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
	return 0;
}

//Subtraction
ieee754binary64 ieee754Binary64SubBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
	ieee754binary64 b64;	
	CONVERT_BIN32_TO_BIN64(&b64, b1);
	return ieee754Binary64SubBinary32Binary64(b64, b2);
}

ieee754binary64 ieee754Binary64SubBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
	ieee754binary64 b64;	
	CONVERT_BIN32_TO_BIN64(&b64, b1);
	return SUB_BINARY64(b64, b2);
}

ieee754binary64 ieee754Binary64SubBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
	ieee754binary64 b64;	
	CONVERT_BIN32_TO_BIN64(&b64, b2);
	return SUB_BINARY64(b1, b64);
}

ieee754binary64 ieee754Binary64SubBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
	return SUB_BINARY64(b1, b2);
}

//Multiplication
ieee754binary32 ieee754Binary32MultBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
	return MULT_BINARY32(b1, b2);
}
//TODO reference is not implemented now!
ieee754binary32 ieee754Binary32MultBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
	ieee754binary64 b64;	
	CONVERT_BIN32_TO_BIN64(&b64, b1);
	return ieee754Binary32MultBinary64Binary64(b64, b2);
}
//TODO reference is not implemented now!
ieee754binary32 ieee754Binary32MultBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2){
	ieee754binary64 b64;	
	CONVERT_BIN32_TO_BIN64(&b64, b2);
	return ieee754Binary32MultBinary64Binary64(b1, b64);
}
//TODO implement later!!!
ieee754binary32 ieee754Binary32MultBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2){
	return 0;
}

ieee754binary64 ieee754Binary64MultBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2){
	ieee754binary64 b64;	
	CONVERT_BIN32_TO_BIN64(&b64, b1);
	return ieee754Binary64MultBinary64Binary32(b64, b2);
}

ieee754binary64 ieee754Binary64MultBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2){
	ieee754binary64 b64;
	CONVERT_BIN32_TO_BIN64(&b64, b1);
	return MULT_BINARY64(b64, b2);
}

ieee754binary64 ieee754Binary64MultBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2){
	ieee754binary64 b64;	
	CONVERT_BIN32_TO_BIN64(&b64, b2);
	return MULT_BINARY64(b1, b64);
}

ieee754binary64 ieee754Binary64MultBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2){
	return MULT_BINARY64(b1, b2);
}

//Division
ieee754binary32 ieee754Binary32DivBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
	return DIV_BINARY32(b1, b2);
}
//TODO reference is not implemented now!
ieee754binary32 ieee754Binary32DivBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
	ieee754binary64 b64;
	CONVERT_BIN32_TO_BIN64(&b64, b1);
	return ieee754Binary32DivBinary64Binary64(b64, b2);
}
//TODO reference is not implemented now!
ieee754binary32 ieee754Binary32DivBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
	ieee754binary64 b64;
	CONVERT_BIN32_TO_BIN64(&b64, b2);
	return ieee754Binary32DivBinary64Binary64(b1, b64);
}
//TODO implement later!!!
ieee754binary32 ieee754Binary32DivBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
	return 0;
}

ieee754binary64 ieee754Binary64DivBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
	ieee754binary64 b64;
	CONVERT_BIN32_TO_BIN64(&b64, b1);
	return ieee754Binary64DivBinary64Binary32(b1, b2);	
}

ieee754binary64 ieee754Binary64DivBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
	ieee754binary64 b64;
	CONVERT_BIN32_TO_BIN64(&b64, b1);	
	return DIV_BINARY64(b64, b2);
}

ieee754binary64 ieee754Binary64DivBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
	ieee754binary64 b64;
	CONVERT_BIN32_TO_BIN64(&b64, b2);	
	return DIV_BINARY64(b1, b64);
}

ieee754binary64 ieee754Binary64DivBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
	return DIV_BINARY64(b1, b2);
}

//Square root
ieee754binary32 ieee754Binary32SqrtBinary32(ieee754binary32 b1) {
	return SQRT_BINARY32(b1);
}

//TODO not implemented yet!
ieee754binary32 ieee754Binary32SqrtBinary64(ieee754binary64 b1) {
	return 0;
}


ieee754binary64 ieee754Binary64SqrtBinary32(ieee754binary32 b1) {
	ieee754binary64 b64;
	CONVERT_BIN32_TO_BIN64(&b64, b1);
	return SQRT_BINARY64(b64);
}

ieee754binary64 ieee754Binary64SqrtBinary64(ieee754binary64 b1) {
	return SQRT_BINARY64(b1);
}

//FMA
ieee754binary64 ieee754Binary64FmaBinary32Binary32Binary32(ieee754binary32 b1, ieee754binary32 b2, ieee754binary32 b3) {
	ieee754binary64 b64 = ieee754Binary64MultBinary32Binary32(b1, b2);
	return ieee754Binary64AddBinary64Binary32(b64, b3);
}

ieee754binary64 ieee754Binary64FmaBinary32Binary32Binary64(ieee754binary32 b1, ieee754binary32 b2, ieee754binary64 b3) {
        ieee754binary64 b64_1, b64_2;
	CONVERT_BIN32_TO_BIN64(&b64_1, b1);
	CONVERT_BIN32_TO_BIN64(&b64_2, b2);
	ieee754binary64 b64 = MULT_BINARY64(b64_1, b64_2);
	return ieee754Binary64AddBinary64Binary64(b64, b3);
}

ieee754binary64 ieee754Binary64FmaBinary32Binary64Binary32(ieee754binary32 b1, ieee754binary64 b2, ieee754binary32 b3) {
  return 1;
}

ieee754binary64 ieee754Binary64FmaBinary32Binary64Binary64(ieee754binary32 b1, ieee754binary64 b2, ieee754binary64 b3){

	return 1;
}

ieee754binary64 ieee754Binary64FmaBinary64Binary32Binary32(ieee754binary64 b1, ieee754binary32 b2, ieee754binary32 b3){
	
  return 1;
}

//TODO use mult double
ieee754binary64 ieee754Binary64FmaBinary64Binary32Binary64(ieee754binary64 b1, ieee754binary32 b2, ieee754binary64 b3){
	
  return 1;
}


