#include <ieee754-2008.h>
#include <ieee754-2008-macros.h>
#include <float.h>

#define threshold32        8388608.0f             // 2^23
#define threshold64        4503599627370496.0     // 2^52

#define precision32        23u
#define precision64        52u

#define expbias32          127u
#define expbias64          1023u

//12 rounding functions

ieee754binary32 ieee754Binary32RoundToIntegralExactBinary32(ieee754binary32 b) {
  int isInf, isNaN, isZero, qNaN;
  uint32_t abs;
  binary32wrapper b32w;
  ieee754binary32 y, n;
  IS_INF_QUIET32(b, &isInf);
  IS_NAN_QUIET32(b, &isNaN);
  IS_ZERO_QUIET32(b, &isZero);
  IS_QNAN_QUIET64(b, &qNaN);
  
  if (isInf || isNaN || isZero) {
    if (!qNaN)
      SIGNAL_INVALID();
    return b;
  }
  ABS_QUIET32(b, &abs);
  b32w.int32 = abs; 
  if (b32w.bin32 > threshold32) {   
    return b;
  }    
  y = (b + threshold32 * ((b > 0) ? 1.0f : -1.0f));
  n = (y - threshold32 * ((b > 0) ? 1.0f : -1.0f));
  return n;
}


ieee754binary64 ieee754Binary64RoundToIntegralExactBinary64(ieee754binary64 b) {  
  int isInf, isNaN, isZero, qNaN;
  uint64_t abs;
  binary64wrapper b64w;
  ieee754binary64 y, n;  
  IS_INF_QUIET64(b, &isInf);
  IS_NAN_QUIET64(b, &isNaN);
  IS_ZERO_QUIET64(b, &isZero);  
  IS_QNAN_QUIET64(b, &qNaN);
 
  if (isInf || isNaN || isZero) {
    if (!qNaN)
      SIGNAL_INVALID();
    return b;
  }
  ABS_QUIET64(b, &abs);
  b64w.int64 = abs; 
  if (b64w.bin64 > threshold64) {   
    return b;
  } 
  y = (b + threshold64 * ((b > 0) ? 1.0 : -1.0));
  n = (y - threshold64 * ((b > 0) ? 1.0 : -1.0));
  return n;
}

ieee754binary32 ieee754Binary32RoundToIntegralTowardZeroBinary32(ieee754binary32 b)
{
  uint8_t isNaN, sign, qNaN;
  uint32_t abs;
  binary32wrapper b32w;
  IS_SIGN_MINUS_QUIET32(b, &sign);
  IS_NAN_QUIET32(b, &isNaN);
  ABS_QUIET32(b, &abs);  
  IS_QNAN_QUIET32(b, &qNaN);
  if (isNaN) {
    if (!qNaN)
      SIGNAL_INVALID();
    return b + 1.0f; 
  }

  b32w.int32 = abs; 
  if (b32w.bin32 > threshold32) {   
    return b;
  }    
  
  if (b32w.bin32 < 1.0f) {
    return ( (sign == 0) ? 0.0f : -0.0f );
  }

  b32w.bin32 = b;
  uint32_t exp = b32w.int32 & exponent32;  
  uint32_t keepMask = ~((1 << (precision32 - ((exp >> precision32) - expbias32))) - 1);  
  b32w.int32 = b32w.int32 & keepMask;
  return b32w.bin32; 
}


ieee754binary64 ieee754Binary64RoundToIntegralTowardZeroBinary64(ieee754binary64 b)
{
  uint8_t isNaN, sign, qNaN;
  uint64_t abs;
  binary64wrapper b64w;
  IS_SIGN_MINUS_QUIET64(b, &sign);
  IS_NAN_QUIET64(b, &isNaN);
  ABS_QUIET64(b, &abs);  
  IS_QNAN_QUIET64(b, &qNaN);
  if (isNaN) {
    if (!qNaN)
      SIGNAL_INVALID();
    return b + 1.0; 
  }

  b64w.int64 = abs; 
  if (b64w.bin64 > threshold64) {   
    return b;
  }    
  
  if (b64w.bin64 < 1.0l) {
    return ( (sign == 0) ? 0.0 : -0.0 );
  }

  b64w.bin64 = b;
  uint64_t exp = b64w.int64 & exponent64;   
  uint64_t keepMask = ~((1l << (precision64 - ((exp >> precision64) - expbias64))) - 1);  
  b64w.int64 = b64w.int64 & keepMask;
  return b64w.bin64; 
}

ieee754binary32 ieee754Binary32RoundToIntegralTowardPositiveBinary32(ieee754binary32 b) {
  uint8_t isNaN, sign, isZero, qNaN;
  uint32_t abs;
  binary32wrapper b32w;
  IS_SIGN_MINUS_QUIET32(b, &sign);
  IS_ZERO_QUIET32(b, &isZero);
  IS_NAN_QUIET32(b, &isNaN);
  ABS_QUIET32(b, &abs);  
  IS_QNAN_QUIET32(b, &qNaN);
  if (isNaN) {
    if (!qNaN)      
      SIGNAL_INVALID();      
    return b + 1.0f; 
  }

  b32w.int32 = abs; 
  if (b32w.bin32 > threshold32 || isZero) {   
    return b;
  }      

  if (b32w.bin32 < 1.0f) {
    return (!sign) ? 1.0f : -0.0f;
  }

  b32w.bin32 = b;
  uint32_t exp = b32w.int32 & exponent32;  
  uint32_t keepMask = ~((1 << (precision32 - ((exp >> precision32) - expbias32))) - 1);    

  b32w.int32 = b32w.int32 & keepMask;  
  return b32w.bin32 + 1.0f * !sign * (b32w.bin32 - b != 0.0f);
}

ieee754binary64 ieee754Binary64RoundToIntegralTowardPositiveBinary64(ieee754binary64 b)
{
  uint8_t isNaN, sign, isZero, qNaN;
  uint64_t abs;
  binary64wrapper b64w;
  IS_SIGN_MINUS_QUIET64(b, &sign);
  IS_ZERO_QUIET64(b, &isZero);
  IS_NAN_QUIET64(b, &isNaN);
  ABS_QUIET64(b, &abs);
  IS_QNAN_QUIET64(b, &qNaN);
  if (isNaN) {
    if (!qNaN) 
      SIGNAL_INVALID();
    return b + 1.0; 
  }

  b64w.int64 = abs; 
  if (b64w.bin64 > threshold64 || isZero) {   
    return b;
  }    
  
  if (b64w.bin64 < 1.0) {
    return (!sign) ? 1.0 : -0.0;
  }

  b64w.bin64 = b;
  uint64_t exp = b64w.int64 & exponent64;  
  uint64_t keepMask = ~((1l << (precision64 - ((exp >> precision64) - expbias64))) - 1);    
  b64w.int64 = b64w.int64 & keepMask;
  return b64w.bin64 + 1.0l * !sign * (b64w.bin64 - b != 0.0);  
}

ieee754binary32 ieee754Binary32RoundToIntegralTowardNegativeBinary32(ieee754binary32 b) {
  uint8_t isNaN, sign, isZero, qNaN;
  uint32_t abs;
  binary32wrapper b32w;
  IS_SIGN_MINUS_QUIET32(b, &sign);
  IS_ZERO_QUIET32(b, &isZero);
  IS_NAN_QUIET32(b, &isNaN);
  ABS_QUIET32(b, &abs);
  IS_QNAN_QUIET32(b, &qNaN);
  if (isNaN) {
    if (!qNaN)
      SIGNAL_INVALID();
    return b + 1.0f; 
  }
  b32w.int32 = abs; 
  if (b32w.bin32 > threshold32 || isZero) {   
    return b;
  }      
  if (b32w.bin32 < 1.0f) {
    return (!sign) ? 0.0f : -1.0f ;    
  } 
  b32w.bin32 = b;
  uint32_t exp = b32w.int32 & exponent32;  
  uint32_t keepMask = ~((1 << (precision32 - ((exp >> precision32) - expbias32))) - 1);    
  b32w.int32 = b32w.int32 & keepMask;  
  return b32w.bin32 - 1.0f * sign * (b32w.bin32 - b != 0.0f);
}

ieee754binary64 ieee754Binary64RoundToIntegralTowardNegativeBinary64(ieee754binary64 b) {
  uint8_t isNaN, sign, isZero, qNaN;
  uint64_t abs;
  binary64wrapper b64w;
  IS_SIGN_MINUS_QUIET64(b, &sign);
  IS_ZERO_QUIET64(b, &isZero);
  IS_NAN_QUIET64(b, &isNaN);
  ABS_QUIET64(b, &abs);
  IS_QNAN_QUIET64(b, &qNaN);
  if (isNaN) {
    if (!qNaN)
      SIGNAL_INVALID();
    return b + 1.0; 
  }
  b64w.int64 = abs; 
  if (b64w.bin64 > threshold64 || isZero) {   
    return b;
  }      
  if (b64w.bin64 < 1.0) {
    return (!sign) ? 0.0 : -1.0;
  }
  b64w.bin64 = b;
  uint64_t exp = b64w.int64 & exponent64;  
  uint64_t keepMask = ~((1l << (precision64 - ((exp >> precision64) - expbias64))) - 1);    
  b64w.int64 = b64w.int64 & keepMask;
  return b64w.bin64 - 1.0l * sign * (b64w.bin64 - b != 0.0); 
}

ieee754binary32 ieee754Binary32RoundToIntegralTiesToAwayBinary32(ieee754binary32 b) {
  uint8_t isNaN, sign, isZero, qNaN;
  uint32_t abs;
  binary32wrapper b32w, half;
  half.bin32 = 0.5f;

  IS_SIGN_MINUS_QUIET32(b, &sign);
  IS_ZERO_QUIET32(b, &isZero);
  IS_NAN_QUIET32(b, &isNaN);
  ABS_QUIET32(b, &abs);
  IS_QNAN_QUIET32(b, &qNaN);
  if (isNaN) {
    if (!qNaN)
      SIGNAL_INVALID();
    return b + 1.0f; 
  }

  b32w.int32 = abs; 
  if (b32w.bin32 > threshold32 || isZero) {   
    return b;
  }    
  if (b32w.bin32 < 0.5f)  {
    return ((!sign) ? 0.0f : -0.0f);
  }
    
  if (b32w.bin32 < 1.0f) {
    return ((!sign) ? 1.0f : -1.0f);
  } 

  b32w.bin32 = b;
  uint32_t exp = b32w.int32 & exponent32;  
  uint32_t keepMask = ~((1 << (precision32 - ((exp >> precision32) - expbias32))) - 1);    
  b32w.int32 = b32w.int32 & keepMask;   
  abs = 0;
  ieee754binary32 tmp = b - b32w.bin32;
  ABS_QUIET32(tmp, &abs);  
  if (abs >= half.int32) 
    return b32w.bin32 + ((!sign) ? 1.0f : -1.0f);
  return b32w.bin32;
}

ieee754binary64 ieee754Binary64RoundToIntegralTiesToAwayBinary64(ieee754binary64 b) {
  uint8_t isNaN, sign, isZero, qNaN;
  uint64_t abs;
  binary64wrapper b64w, half;
  half.bin64 = 0.5;

  IS_SIGN_MINUS_QUIET64(b, &sign);
  IS_ZERO_QUIET64(b, &isZero);
  IS_NAN_QUIET64(b, &isNaN);
  ABS_QUIET64(b, &abs);
  IS_QNAN_QUIET64(b, &qNaN);
  if (isNaN) {
    if (!qNaN)
      SIGNAL_INVALID();
    return b + 1.0; 
  }

  b64w.int64 = abs; 
  if (b64w.bin64 > threshold64 || isZero) {   
    return b;
  }    
  if (b64w.bin64 < 0.5)  {
    return ((!sign) ? 0.0 : -0.0);
  }
    
  if (b64w.bin64 < 1.0) {
    return ((!sign) ? 1.0 : -1.0);
  } 

  b64w.bin64 = b;
  uint64_t exp = b64w.int64 & exponent64;  
  uint64_t keepMask = ~((1l << (precision64 - ((exp >> precision64) - expbias64))) - 1);    
  b64w.int64 = b64w.int64 & keepMask;   
  abs = 0;
  ieee754binary64 tmp = b - b64w.bin64;
  ABS_QUIET64(tmp, &abs);  
  if (abs >= half.int64) 
    return b64w.bin64 + ((!sign) ? 1.0 : -1.0);
  return b64w.bin64;
}

ieee754binary32 ieee754Binary32RoundToIntegralTiesToEvenBinary32(ieee754binary32 b) {
  uint8_t isNaN, sign, isZero, qNaN;
  uint32_t abs;
  binary32wrapper b32w, half;
  half.bin32 = 0.5f;
  IS_SIGN_MINUS_QUIET32(b, &sign);
  IS_ZERO_QUIET32(b, &isZero);
  IS_NAN_QUIET32(b, &isNaN);
  ABS_QUIET32(b, &abs);
  IS_QNAN_QUIET32(b, &qNaN);
  if (isNaN) {
    if (!qNaN)
      SIGNAL_INVALID();
    return b + 1.0f; 
  }

  b32w.int32 = abs; 
  if (b32w.bin32 > threshold32 || isZero) {   
    return b;
  }    
   
  if (b32w.bin32 <= 0.5f) {
    return ((!sign) ? 0.0f : -0.0f) ;    
  } 
  if (b32w.bin32 < 1.0f) {
    return ((!sign) ? 1.0f : -1.0f) ;    
  } 

  b32w.bin32 = b;
  uint32_t exp = b32w.int32 & exponent32;  
  uint32_t keepMask = ~((1 << (precision32 - ((exp >> precision32) - expbias32))) - 1);   
  b32w.int32 = b32w.int32 & keepMask;   

  abs = 0;
  ieee754binary32 tmp = b - b32w.bin32;
  int even;
  ABS_QUIET32(tmp, &abs); 
 
  if (abs > half.int32) {
    return b32w.bin32 + ((!sign) ? 1.0f : -1.0f);
  }

  if (abs == half.int32) {   
    keepMask = ~((1 << (precision32 - ((exp >> precision32) - expbias32)) + 1 ) - 1);            
    even = (b32w.int32 & keepMask) == b32w.int32;      
    return b32w.bin32 + ((!sign) ? 1.0f : -1.0f) * (!even); 
  }
  return b32w.bin32;
}


ieee754binary64 ieee754Binary64RoundToIntegralTiesToEvenBinary64(ieee754binary64 b) {
  uint8_t isNaN, sign, isZero, qNaN;
  uint64_t abs;
  binary64wrapper b64w, half;
  half.bin64 = 0.5;
  IS_SIGN_MINUS_QUIET64(b, &sign);
  IS_ZERO_QUIET64(b, &isZero);
  IS_NAN_QUIET64(b, &isNaN);
  IS_QNAN_QUIET64(b, &qNaN);
  ABS_QUIET64(b, &abs);
  if (isNaN) {
    if (!qNaN) 
      SIGNAL_INVALID();
    return b + 1.0; 
  }

  b64w.int64 = abs; 
  if (b64w.bin64 > threshold64 || isZero) {   
    return b;
  }    
   
  if (b64w.bin64 <= 0.5) {
    return ((!sign) ? 0.0 : -0.0) ;    
  } 
  if (b64w.bin64 < 1.0) {   
    return ((!sign) ? 1.0 : -1.0) ;    
  } 

  b64w.bin64 = b;
  uint64_t exp = b64w.int64 & exponent64;  
  uint64_t keepMask = ~((1l << (precision64 - ((exp >> precision64) - expbias64))) - 1);   
  b64w.int64 = b64w.int64 & keepMask;   

  abs = 0;
  ieee754binary64 tmp = b - b64w.bin64;  
  int even;
  ABS_QUIET64(tmp, &abs); 
  
  if (abs > half.int64) {    
    return b64w.bin64 + ((!sign) ? 1.0 : -1.0);
  }

  if (abs == half.int64) {   
    keepMask = ~((1l << (precision64 - ((exp >> precision64) - expbias64)) + 1 ) - 1);            
    even = (b64w.int64 & keepMask) == b64w.int64;      
    return b64w.bin64 + ((!sign) ? 1.0 : -1.0) * (!even); 
  }
  return b64w.bin64;
}
