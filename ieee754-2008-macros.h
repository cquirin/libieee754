#ifndef IEEE754_2008_MACROS_H
#define IEEE754_2008_MACROS_H

#include <math.h>

typedef union {
  ieee754binary32 bin32;
  uint32_t int32;
} binary32wrapper;

typedef union {
  ieee754binary64 bin64;
  uint64_t int64;
} binary64wrapper;

#define signMask32 0x80000000
#define withoutSignMask32 0x7fffffff

#define signMask64 0x8000000000000000
#define withoutSignMask64 signMask64 - 1 
//#define const signMask64 2 * signMask32 * signMask32;

#define exponent32 0x7f800000u
#define exponent64 0x7ff0000000000000ull

#define TWO_TO_22 4194304u 
#define TWO_TO_51 2251799813685248ull

#define significand32 0x007fffffu
#define significand64 0x000fffffffffffffull 

#define CONVERT_BIN32_TO_BIN64(b64,b32) do { *(b64) = ((ieee754binary64) (b32)); } while (0)
#define ADD_BINARY32(b32_1, b32_2) ((b32_1) + (b32_2))
#define ADD_BINARY64(b64_1, b64_2) ((b64_1) + (b64_2))
#define SUB_BINARY32(b32_1, b32_2) ((b32_1) - (b32_2))
#define SUB_BINARY64(b64_1, b64_2) ((b64_1) - (b64_2))
#define MULT_BINARY32(b32_1, b32_2) ((b32_1) * (b32_2))
#define MULT_BINARY64(b64_1, b64_2) ((b64_1) * (b64_2))
#define DIV_BINARY32(b32_1, b32_2) ((b32_1) / (b32_2))
#define DIV_BINARY64(b64_1, b64_2) ((b64_1) / (b64_2))
#define SQRT_BINARY64(b64) (sqrt((b64)))
#define SQRT_BINARY32(b32) (sqrtf((b32)))
#define COPY_SIGN32(b32_1, b32_2, b32_res) do { *(b32_res) = copysignf(b32_1, b32_2); } while (0)
#define COPY_SIGN64(b64_1, b64_2, b64_res) do { *(b64_res) = copysign(b64_1, b64_2); } while (0)

#define IS_FINITE32(b32) (isfinite(b32))
#define IS_FINITE64(b64) (isfinite(b64))
#define IS_NORMAL32(b32) (isnormal(b32))
#define IS_NORMAL64(b64) (isnormal(b64))
#define IS_NAN32(b32) (isnanf(b32))
#define IS_NAN64(b64) (isnan(b64))

#define IS_NAN_QUIET32(b32, boo) do                                                                          \
{ binary32wrapper b32w;                                                                                      \
  b32w.bin32 = (b32);                                                                                        \
  *(boo) = ((b32w.int32 & exponent32) == exponent32) && ((b32w.int32 & significand32) != 0u);                \
} while(0)

#define IS_QNAN_QUIET32(b32, boo) do                                                                          \
{ binary32wrapper b32w;                                                                                      \
  b32w.bin32 = (b32);                                                                                        \
  *(boo) = ((b32w.int32 & exponent32) == exponent32) && ((b32w.int32 & significand32) != 0u) && ((b32w.int32 & TWO_TO_22) == TWO_TO_22) ; \
} while(0)

#define IS_NAN_QUIET64(b64, boo) do                                                                          \
{ binary64wrapper b64w;                                                                                      \
  b64w.bin64 = (b64);                                                                                        \
  *(boo) = ((b64w.int64 & exponent64) == exponent64) && ((b64w.int64 & significand64) != 0ull);              \
} while(0)

#define IS_QNAN_QUIET64(b64, boo) do                                                                          \
{ binary64wrapper b64w;                                                                                      \
  b64w.bin64 = (b64);                                                                                        \
  *(boo) = ((b64w.int64 & exponent64) == exponent64) && ((b64w.int64 & significand64) != 0ull) && ((b64w.int64 & TWO_TO_51) == TWO_TO_51); \
} while(0)

#define IS_INF_QUIET32(b32, boo) do                                                                          \
{ binary32wrapper b32w;                                                                                      \
  b32w.bin32 = (b32);                                                                                        \
  *(boo) = ((b32w.int32 & exponent32) == exponent32) && ((b32w.int32 & significand32) == 0u);                \
} while(0)

#define IS_INF_QUIET64(b64, boo) do                                                                          \
{ binary64wrapper b64w;                                                                                      \
  b64w.bin64 = (b64);                                                                                        \
  *(boo) = ((b64w.int64 & exponent64) == exponent64) && ((b64w.int64 & significand64) == 0ull);              \
} while(0)   

#define IS_ZERO_QUIET32(b32, boo) do                                                                         \
{ binary32wrapper b32w;                                                                                      \
  b32w.bin32 = (b32);                                                                                        \
  *(boo) = ((b32w.int32 & exponent32) == 0u) && ((b32w.int32 & significand32) == 0u);                        \
} while(0)

#define IS_ZERO_QUIET64(b64, boo) do                                                                         \
{ binary64wrapper b64w;                                                                                      \
  b64w.bin64 = (b64);                                                                                        \
  *(boo) = ((b64w.int64 & exponent64) == 0ull) && ((b64w.int64 & significand64) == 0ull);                    \
} while(0)   

#define IS_INF32(b32) ( (isinff(b32)==0) ? 0 : 1 )
#define IS_INF64(b64) ( (isinf(b64)==0) ? 0 : 1 )
//TODO implement!
#define IS_SIGN_MINUS(b_32_or_64) (signbit(b_32_or_64))

#define IS_SIGN_MINUS_QUIET32(b32, boo) do {                                                                \
 binary32wrapper b32w;                                                                                      \
  b32w.bin32 = (b32);                                                                                       \
 *(boo) = ((b32w.int32) & signMask32) == signMask32;                                                        \
 } while (0)

#define IS_SIGN_MINUS_QUIET64(b64, boo) do                                                                   \
{ binary64wrapper b64w;                                                                                      \
  b64w.bin64 = (b64);                                                                                        \
  *(boo) = ((b64w.int64) & signMask64) == signMask64;	                                                     \
} while (0)

#define ABS_QUIET32(b32, ui32) do                                                                            \
{                                                                                                            \
  binary32wrapper b32w;                                                                                      \
  b32w.bin32 = (b32);                                                                                        \
  *(ui32) = (b32w.int32) & withoutSignMask32;                                                                \
} while (0)                                                                                                  

#define ABS_QUIET64(b64, ui64) do                                                                            \
{                                                                                                            \
  binary64wrapper b64w;                                                                                      \
  b64w.bin64 = (b64);                                                                                        \
  *(ui64) = (b64w.int64) & withoutSignMask64;                                                                \
} while (0)
 
#define SIGNAL_INVALID() do                                                                                  \
{                                                                                                            \
  volatile double x,y,z;                                                                                     \
  x = 0.0;                                                                                                   \
  y = 0.0;                                                                                                   \
  z = x / y;                                                                                                 \
} while (0)                 

#define SIGNAL_INEXACT() do                                                                                  \
{                                                                                                            \
  volatile float x,y,z;                                                                                      \
  x = 1.0;                                                                                                   \
  y = TWO_TO_22 * 256.0;                                                                                     \
  z = x + y;                                                                                                 \
} while (0)                 


#define GET_EXP64(GET_EXP64_B64, GET_EXP64_EXP64) (*(GET_EXP64_EXP64) = (GET_EXP64_B64) & exponent64)
#define GET_EXP32(GET_EXP32_B32, GET_EXP32_EXP32) (*(GET_EXP32_EXP32) = (GET_EXP32_B32) & exponent32

#define GET_SIGNIFICAND64(b64, mantissa64) (*(mantissa64) = (b64) & significand64)
#define GET_SIGNIFICAND32(b32, mantissa32) (*(mantissa32) = (b32) & significand32)

#define NEGATE(b_32_or_64) (-(b_32_or_64))
#define IS_ZERO(b_32_or_64) (fpclassify(b_32_or_64) == FP_ZERO)
#define ILOGB32(b32) (ilogbf(b32))
#define ILOGB64(b64) (ilogb(b64))
#define SCALEB32(b32, iparam) (scalblnf((b32), (iparam)))
#define SCALEB64(b64, iparam) (scalbln((b64), (iparam)))
#define ABS32(b32) (fabsf(b32))
#define ABS64(b64) (fabs(b64))
#define NEXT_UP32(b32) (nextafterf((b32), (b32)+1.0))
#define NEXT_UP64(b64) (nextafter((b64), (b64)+1.0))

#define MIN32(b32_1, b32_2) (fminf(b32_1, b32_2))
#define MIN64(b64_1, b64_2) (fmin(b64_1, b64_2))
#define MAX32(b32_1, b32_2) (fmaxf(b32_1, b32_2))
#define MAX64(b64_1, b64_2) (fmax(b64_1, b64_2))

//fmod vs. remainder
#define REMAINDER32(b32, b32_2) (fmodf((b32), (b32_2)))
#define REMAINDER64(b64, b64_2) (fmod((b64), (b64_2)))

#endif
