//This is a file for implementation the comparisons, declared in IEEE754-2008 standard

#include <ieee754-2008.h>
#include <ieee754-2008-macros.h>
#include <stdint.h>


//Signaling comparisons
int ieee754CompareSignalingEqualBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
  if (b1 != b1 || b2 != b2) {
    return b1 < b2;
  }
  return b1 == b2;
}

int ieee754CompareSignalingEqualBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b1);
  if (b1 != b1 || b2 != b2) {
    return b64 < b2;
  }
  return b64 == b2;
}

int ieee754CompareSignalingEqualBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b2);
  if (b1 != b1 || b2 != b2) {
    return b1 < b64;
  }
  return b1 == b2;
}

int ieee754CompareSignalingEqualBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2)  {
  if (b1 != b1 || b2 != b2) {
    return b1 < b2;
  }
  return b1 == b2;
}

int ieee754CompareSignalingNotEqualBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) { 
  if (b1 != b1 || b2 != b2) {
    return b1 < b2;
  }
  return b1 != b2;
}

int ieee754CompareSignalingNotEqualBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) { 
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b1);
  if (b1 != b1 || b2 != b2) {
    return b64 < b2;
  }
  return b64 != b2;
}

int ieee754CompareSignalingNotEqualBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {  
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b2);
  if (b1 != b1 || b2 != b2) {
    return b1 < b64;
  }
  return b1 != b64;
}

int ieee754CompareSignalingNotEqualBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) { 
  if (b1 != b1 || b2 != b2) {
    return b1 < b2;
  }
  return b1 != b2;
}

int ieee754CompareSignalingGreaterBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
  return b1 > b2;
}
int ieee754CompareSignalingGreaterBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b1);
  return b64 > b2;
}

int ieee754CompareSignalingGreaterBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b2);
  return b1 > b64;
}

int ieee754CompareSignalingGreaterBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
  return b1 > b2;
}

int ieee754CompareSignalingGreaterEqualBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
  return b1 >= b2;
}

int ieee754CompareSignalingGreaterEqualBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b1);
  return b64 >= b2;
}

int ieee754CompareSignalingGreaterEqualBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
    ieee754binary64 b64;
    CONVERT_BIN32_TO_BIN64(&b64, b2);
    return b1 >= b64;
}

int ieee754CompareSignalingGreaterEqualBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
  return b1 >= b2;
}

int ieee754CompareSignalingLessBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
  return b1 < b2;
}

int ieee754CompareSignalingLessBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b1);
  return b64 < b2;
}

int ieee754CompareSignalingLessBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b2);
  return b1 < b64;
}

int ieee754CompareSignalingLessBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
  return b1 < b2;
}

int ieee754CompareSignalingLessEqualBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
  return b1 <= b2;
}

int ieee754CompareSignalingLessEqualBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b1);
  return b64 <= b2;
}

int ieee754CompareSignalingLessEqualBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b2);
  return b1 <= b64;
}

int ieee754CompareSignalingLessEqualBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
  return b1 <= b2;
}

int ieee754CompareSignalingNotGreaterBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
  if (b1 != b1 || b2 != b2) {
    return 1;
  }
  return b1 <= b2;
}

int ieee754CompareSignalingNotGreaterBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
  if (b1 != b1 || b2 != b2) {
    return 1;
  }
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b1);
  return b64 <= b2;
}

int ieee754CompareSignalingNotGreaterBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
  if (b1 != b1 || b2 != b2) {
    return 1;
  }
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b2);
  return b1 <= b64;
}
int ieee754CompareSignalingNotGreaterBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
  if (b1 != b1 || b2 != b2) {
    return 1;
  }
  return b1 <= b2;
}

int ieee754CompareSignalingLessUnorderedBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
  if (b1 != b1 || b2 != b2) {
    return 1;
  }
  return b1 < b2;
}

int ieee754CompareSignalingLessUnorderedBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
  if (b1 != b1 || b2 != b2) {
    return 1;
  }
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b1);
  return b64 < b2;
}

int ieee754CompareSignalingLessUnorderedBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
  if (b1 != b1 || b2 != b2) {
    return 1;
  }
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b2);
  return b1 < b64;
}

int ieee754CompareSignalingLessUnorderedBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
  if (b1 != b1 || b2 != b2) {
    return 1;
  }
  return b1 < b2;
}

int ieee754CompareSignalingNotLessBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
  if (b1 != b1 || b2 != b2) {
    return 1;
  }
  return b1 >= b2;
}

int ieee754CompareSignalingNotLessBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
  if (b1 != b1 || b2 != b2) {
    return 1;
  }
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b1);
  return b64 >= b2;
}

int ieee754CompareSignalingNotLessBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
  if (b1 != b1 || b2 != b2) {
    return 1;
  }
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b2);
  return b1 >= b64;
}

int ieee754CompareSignalingNotLessBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
  if (b1 != b1 || b2 != b2) {
    return 1;
  }
  return b1 >= b2;
}

int ieee754CompareSignalingGreaterUnorderedBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) { 
  return b1 > b2;
}

int ieee754CompareSignalingGreaterUnorderedBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b1);
  return b64 > b2;
}

int ieee754CompareSignalingGreaterUnorderedBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {  
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b2);
  return b64 > b2;
}

int ieee754CompareSignalingGreaterUnorderedBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
  return b1 > b2;
}

//Quiet comparisons

int ieee754CompareQuietEqualBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) { 
    return b1 == b2; 
}

int ieee754CompareQuietEqualBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b1);
  return b64 == b2;
}

int ieee754CompareQuietEqualBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b2);  
  return b1 == b64;
}

int ieee754CompareQuietEqualBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
   return b1 == b2;
}

int ieee754CompareQuietNotEqualBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
  return b1 != b2;
}

int ieee754CompareQuietNotEqualBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
  return b1 != b2;
}

int ieee754CompareQuietNotEqualBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
  return b1 != b2;
}

int ieee754CompareQuietNotEqualBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {  
  return b1 != b2;
}

int ieee754CompareQuietGreaterBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
  if (b1 != b1 || b2 != b2) 
    return 0;
  else 
    return b1 > b2;
}

int ieee754CompareQuietGreaterBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
  if (b1 != b1 || b2 != b2) 
    return 0;
  else {
    ieee754binary64 b64;
    CONVERT_BIN32_TO_BIN64(&b64, b1);
    return b64 > b2;
  }
}

int ieee754CompareQuietGreaterBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
  if (b1 != b1 || b2 != b2) 
    return 0;
  else {
    ieee754binary64 b64;
    CONVERT_BIN32_TO_BIN64(&b64, b2);
    return b1 > b64;
  }
}

int ieee754CompareQuietGreaterBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
  if (b1 != b1 || b2 != b2) 
    return 0;
  else 
   return b1 > b2;  
}

int ieee754CompareQuietGreaterEqualBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
  if (b1 != b1 || b2 != b2) 
    return 0;
  else {
   return b1 >= b2;  
  }
}

int ieee754CompareQuietGreaterEqualBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
  if (b1 != b1 || b2 != b2) 
    return 0;
  else {
    ieee754binary64 b64;
    CONVERT_BIN32_TO_BIN64(&b64, b1);
    return b64 >= b2;
  }  
}

int ieee754CompareQuietGreaterEqualBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
  if (b1 != b1 || b2 != b2) 
    return 0;
  else {
    ieee754binary64 b64;
    CONVERT_BIN32_TO_BIN64(&b64, b2);
    return b1 >= b64;
  }
}

int ieee754CompareQuietGreaterEqualBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
  if (b1 != b1 || b2 != b2) 
    return 0;
  else {   
    return b1 > b2;
  }
}

int ieee754CompareQuietLessBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
  if (b1 != b1 || b2 != b2) 
    return 0;
  else {   
    return b1 < b2;
  }
}
int ieee754CompareQuietLessBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
  if (b1 != b1 || b2 != b2) 
    return 0;
  else {   
    ieee754binary64 b64;
    CONVERT_BIN32_TO_BIN64(&b64, b1);
    return b64 < b2;
  }
}
int ieee754CompareQuietLessBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
  if (b1 != b1 || b2 != b2) 
    return 0;
  else {   
    ieee754binary64 b64;
    CONVERT_BIN32_TO_BIN64(&b64, b2);
    return b1 < b64;
  }
}
int ieee754CompareQuietLessBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
  if (b1 != b1 || b2 != b2) 
    return 0;
  else {   
    return b1 < b2;
  }
}

int ieee754CompareQuietLessEqualBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
  if (b1 != b1 || b2 != b2) 
    return 0;
  else {   
    return b1 <= b2;
  }
}

int ieee754CompareQuietLessEqualBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
 if (b1 != b1 || b2 != b2) 
    return 0;
  else {   
    ieee754binary64 b64;
    CONVERT_BIN32_TO_BIN64(&b64, b1);
    return b64 <= b2;
  }
}

int ieee754CompareQuietLessEqualBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
  if (b1 != b1 || b2 != b2) 
    return 0;
  else {   
    ieee754binary64 b64;
    CONVERT_BIN32_TO_BIN64(&b64, b2);
    return b1 <= b64;
  }
}

int ieee754CompareQuietLessEqualBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
 if (b1 != b1 || b2 != b2) 
    return 0;
  else {   
    return b1 <= b2;
  }
}

int ieee754CompareQuietUnorderedBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
  return b1 != b1 || b2 != b2;
}
int ieee754CompareQuietUnorderedBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
  return b1 != b1 || b2 != b2;
}
int ieee754CompareQuietUnorderedBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
  return b1 != b1 || b2 != b2;
}
int ieee754CompareQuietUnorderedBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
  return b1 != b1 || b2 != b2;
}

int ieee754CompareQuietNotGreaterBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
  return b1 != b1 || b2 != b2 || b1 <= b2;
}
int ieee754CompareQuietNotGreaterBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b1);
  return b1 != b1 || b2 != b2 || b64 <= b2;
}
int ieee754CompareQuietNotGreaterBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b2);
  return b1 != b1 || b2 != b2 || b1 <= b64;
}
int ieee754CompareQuietNotGreaterBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
  return b1 != b1 || b2 != b2 || b1 <= b2;
}

int ieee754CompareQuietLessUnorderedBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
  return b1 != b1 || b2 != b2 || b1 < b2;
}

int ieee754CompareQuietLessUnorderedBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b1);
  return b1 != b1 || b2 != b2 || b64 < b2;
}

int ieee754CompareQuietLessUnorderedBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b2);
  return b1 != b1 || b2 != b2 || b1 < b2;
}

int ieee754CompareQuietLessUnorderedBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
  return b1 != b1 || b2 != b2 || b1 < b2;
}

int ieee754CompareQuietNotLessBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
  return b1 != b1 || b2 != b2 || b1 >= b2;
}
int ieee754CompareQuietNotLessBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b1);
  return b1 != b1 || b2 != b2 || b64 >= b2;
}
int ieee754CompareQuietNotLessBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b2);
  return b1 != b1 || b2 != b2 || b1 >= b64;
}
int ieee754CompareQuietNotLessBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
  return b1 != b1 || b2 != b2 || b1 >= b2;
}

int ieee754CompareQuietGreaterUnorderedBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
  return b1 != b1 || b2 != b2 || b1 > b2;
}

int ieee754CompareQuietGreaterUnorderedBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b1);
  return b1 != b1 || b2 != b2 || b64 > b2;
}

int ieee754CompareQuietGreaterUnorderedBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
  ieee754binary64 b64;
  CONVERT_BIN32_TO_BIN64(&b64, b2);
  return b1 != b1 || b2 != b2 || b1 > b64;
}

int ieee754CompareQuietGreaterUnorderedBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
  return b1 != b1 || b2 != b2 || b1 >= b2;
}

int ieee754CompareQuietOrderedBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
  return b1 == b1 && b2 == b2;
}
int ieee754CompareQuietOrderedBinary32Binary64(ieee754binary32 b1, ieee754binary64 b2) {
  return b1 == b1 && b2 == b2;
}
int ieee754CompareQuietOrderedBinary64Binary32(ieee754binary64 b1, ieee754binary32 b2) {
  return b1 == b1 && b2 == b2;
}
int ieee754CompareQuietOrderedBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
  return b1 == b1 && b2 == b2;
}

