#include <ieee754-2008.h>
#include <ieee754-2008-macros.h>
#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <fenv.h>
#include <float.h>

//TODO: write here all the functions!
//try to commit
enum ieee754IOTypeEnum {
  typeieee754binary32,
  typeieee754binary64,
  typeint,
  typeuint8_t,
  typeuint16_t, 
  typeuint32_t,  
  typeuint64_t,
  typeint8_t,
  typeint16_t, 
  typeint32_t,  
  typeint64_t,
  typeexception,
  typecharptr,
  typefpclass,
  typeempty,
};

typedef enum ieee754IOTypeEnum ieee754IOType;

struct funcDeclarationStruct {
  char* name;
  void* funcPointer;
  ieee754IOType   outputType;
  ieee754IOType inputTypes[3];
  //  ieee754IOType inputType2;
  // ieee754IOType inputType3;
};
typedef struct funcDeclarationStruct funcDeclaration;

typedef union {
  double d;
  uint64_t l;
} bin64Caster;

typedef union {
  float f;
  uint32_t i;
} bin32Caster;

typedef union  {
  ieee754binary32 pbin32;   
  ieee754binary64 pbin64;
  uint8_t puint8;
  uint16_t puint16;
  uint32_t puint32;
  uint64_t puint64;
  int8_t pint8;
  int16_t pint16;
  int32_t pint32;
  int64_t pint64;
  int pint;
  ieee754FPClass pfpclass;
  char* pcharptr;
  ieee754ExceptionGroupType pexception;  
} possibleParams;



struct paramWithTypeStruct {
  ieee754IOType type;
  possibleParams param;
};

typedef struct paramWithTypeStruct paramWithType;

static const funcDeclaration funcMapTable [] = {

{"ieee754Binary32AddBinary32Binary32", ieee754Binary32AddBinary32Binary32, typeieee754binary32, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754Binary32AddBinary32Binary64", ieee754Binary32AddBinary32Binary64, typeieee754binary32, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754Binary32AddBinary64Binary32", ieee754Binary32AddBinary64Binary32, typeieee754binary32, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754Binary32AddBinary64Binary64", ieee754Binary32AddBinary64Binary64, typeieee754binary32, {typeieee754binary64, typeieee754binary64, typeempty}},
{"ieee754Binary64AddBinary32Binary32", ieee754Binary64AddBinary32Binary32, typeieee754binary64, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754Binary64AddBinary32Binary64", ieee754Binary64AddBinary32Binary64, typeieee754binary64, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754Binary64AddBinary64Binary32", ieee754Binary64AddBinary64Binary32, typeieee754binary64, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754Binary64AddBinary64Binary64", ieee754Binary64AddBinary64Binary64, typeieee754binary64, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754Binary32SubBinary32Binary32", ieee754Binary32SubBinary32Binary32, typeieee754binary32, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754Binary32SubBinary32Binary64", ieee754Binary32SubBinary32Binary64, typeieee754binary32, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754Binary32SubBinary64Binary32", ieee754Binary32SubBinary64Binary32, typeieee754binary32, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754Binary32SubBinary64Binary64", ieee754Binary32SubBinary64Binary64, typeieee754binary32, {typeieee754binary64, typeieee754binary64, typeempty}},
{"ieee754Binary64SubBinary32Binary32", ieee754Binary64SubBinary32Binary32, typeieee754binary64, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754Binary64SubBinary32Binary64", ieee754Binary64SubBinary32Binary64, typeieee754binary64, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754Binary64SubBinary64Binary32", ieee754Binary64SubBinary64Binary32, typeieee754binary64, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754Binary64SubBinary64Binary64", ieee754Binary64SubBinary64Binary64, typeieee754binary64, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754Binary32MultBinary32Binary32", ieee754Binary32MultBinary32Binary32, typeieee754binary32, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754Binary32MultBinary32Binary64", ieee754Binary32MultBinary32Binary64, typeieee754binary32, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754Binary32MultBinary64Binary32", ieee754Binary32MultBinary64Binary32, typeieee754binary32, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754Binary32MultBinary64Binary64", ieee754Binary32MultBinary64Binary64, typeieee754binary32, {typeieee754binary64, typeieee754binary64, typeempty}},
{"ieee754Binary64MultBinary32Binary32", ieee754Binary64MultBinary32Binary32, typeieee754binary64, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754Binary64MultBinary32Binary64", ieee754Binary64MultBinary32Binary64, typeieee754binary64, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754Binary64MultBinary64Binary32", ieee754Binary64MultBinary64Binary32, typeieee754binary64, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754Binary64MultBinary64Binary64", ieee754Binary64MultBinary64Binary64, typeieee754binary64, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754Binary32DivBinary32Binary32", ieee754Binary32DivBinary32Binary32, typeieee754binary32, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754Binary32DivBinary32Binary64", ieee754Binary32DivBinary32Binary64, typeieee754binary32, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754Binary32DivBinary64Binary32", ieee754Binary32DivBinary64Binary32, typeieee754binary32, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754Binary32DivBinary64Binary64", ieee754Binary32DivBinary64Binary64, typeieee754binary32, {typeieee754binary64, typeieee754binary64, typeempty}},
{"ieee754Binary64DivBinary32Binary32", ieee754Binary64DivBinary32Binary32, typeieee754binary64, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754Binary64DivBinary32Binary64", ieee754Binary64DivBinary32Binary64, typeieee754binary64, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754Binary64DivBinary64Binary32", ieee754Binary64DivBinary64Binary32, typeieee754binary64, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754Binary64DivBinary64Binary64", ieee754Binary64DivBinary64Binary64, typeieee754binary64, {typeieee754binary64, typeieee754binary64, typeempty}},


/*
{"ieee754Binary32FmaBinary32Binary32Binary32", ieee754Binary32FmaBinary32Binary32Binary32, typeieee754binary32, typeieee754binary32, typeieee754binary32, typeieee754binary32},
{"ieee754Binary32FmaBinary32Binary32Binary64", ieee754Binary32FmaBinary32Binary32Binary64, typeieee754binary32, typeieee754binary32, typeieee754binary32, typeieee754binary64},
{"ieee754Binary32FmaBinary32Binary64Binary32", ieee754Binary32FmaBinary32Binary64Binary32, typeieee754binary32, typeieee754binary32, typeieee754binary64, typeieee754binary32},
{"ieee754Binary32FmaBinary32Binary64Binary64", ieee754Binary32FmaBinary32Binary64Binary64, typeieee754binary32, typeieee754binary32, typeieee754binary64, typeieee754binary64},
{"ieee754Binary32FmaBinary64Binary32Binary32", ieee754Binary32FmaBinary64Binary32Binary32, typeieee754binary32, typeieee754binary64, typeieee754binary32, typeieee754binary32},
{"ieee754Binary32FmaBinary64Binary32Binary64", ieee754Binary32FmaBinary64Binary32Binary64, typeieee754binary32, typeieee754binary64, typeieee754binary32, typeieee754binary64},
{"ieee754Binary32FmaBinary64Binary64Binary32", ieee754Binary32FmaBinary64Binary64Binary32, typeieee754binary32, typeieee754binary64, typeieee754binary64, typeieee754binary32},
{"ieee754Binary32FmaBinary64Binary64Binary64", ieee754Binary32FmaBinary64Binary64Binary64, typeieee754binary32, typeieee754binary64, typeieee754binary64, typeieee754binary64},
{"ieee754Binary64FmaBinary32Binary32Binary32", ieee754Binary64FmaBinary32Binary32Binary32, typeieee754binary64, typeieee754binary32, typeieee754binary32, typeieee754binary32},
{"ieee754Binary64FmaBinary32Binary32Binary64", ieee754Binary64FmaBinary32Binary32Binary64, typeieee754binary64, typeieee754binary32, typeieee754binary32, typeieee754binary64},
{"ieee754Binary64FmaBinary32Binary64Binary32", ieee754Binary64FmaBinary32Binary64Binary32, typeieee754binary64, typeieee754binary32, typeieee754binary64, typeieee754binary32},
{"ieee754Binary64FmaBinary32Binary64Binary64", ieee754Binary64FmaBinary32Binary64Binary64, typeieee754binary64, typeieee754binary32, typeieee754binary64, typeieee754binary64},
{"ieee754Binary64FmaBinary64Binary32Binary32", ieee754Binary64FmaBinary64Binary32Binary32, typeieee754binary64, typeieee754binary64, typeieee754binary32, typeieee754binary32},
{"ieee754Binary64FmaBinary64Binary32Binary64", ieee754Binary64FmaBinary64Binary32Binary64, typeieee754binary64, typeieee754binary64, typeieee754binary32, typeieee754binary64},
{"ieee754Binary64FmaBinary64Binary64Binary32", ieee754Binary64FmaBinary64Binary64Binary32, typeieee754binary64, typeieee754binary64, typeieee754binary64, typeieee754binary32},
{"ieee754Binary64FmaBinary64Binary64Binary64", ieee754Binary64FmaBinary64Binary64Binary64, typeieee754binary64, typeieee754binary64, typeieee754binary64, typeieee754binary64},
  */
 
  //signaling comparisons
{"ieee754CompareSignalingEqualBinary32Binary32", ieee754CompareSignalingEqualBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingEqualBinary32Binary64", ieee754CompareSignalingEqualBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754CompareSignalingEqualBinary64Binary32", ieee754CompareSignalingEqualBinary64Binary32, typeint, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingEqualBinary64Binary64", ieee754CompareSignalingEqualBinary64Binary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754CompareSignalingNotEqualBinary32Binary32", ieee754CompareSignalingNotEqualBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingNotEqualBinary32Binary64", ieee754CompareSignalingNotEqualBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754CompareSignalingNotEqualBinary64Binary32", ieee754CompareSignalingNotEqualBinary64Binary32, typeint, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingNotEqualBinary64Binary64", ieee754CompareSignalingNotEqualBinary64Binary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754CompareSignalingGreaterBinary32Binary32", ieee754CompareSignalingGreaterBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingGreaterBinary32Binary64", ieee754CompareSignalingGreaterBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754CompareSignalingGreaterBinary64Binary32", ieee754CompareSignalingGreaterBinary64Binary32, typeint, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingGreaterBinary64Binary64", ieee754CompareSignalingGreaterBinary64Binary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754CompareSignalingGreaterEqualBinary32Binary32", ieee754CompareSignalingGreaterEqualBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingGreaterEqualBinary32Binary64", ieee754CompareSignalingGreaterEqualBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754CompareSignalingGreaterEqualBinary64Binary32", ieee754CompareSignalingGreaterEqualBinary64Binary32, typeint, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingGreaterEqualBinary64Binary64", ieee754CompareSignalingGreaterEqualBinary64Binary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754CompareSignalingLessBinary32Binary32",ieee754CompareSignalingLessBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingLessBinary32Binary64",ieee754CompareSignalingLessBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754CompareSignalingLessBinary64Binary32",ieee754CompareSignalingLessBinary64Binary32, typeint, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingLessBinary64Binary64",ieee754CompareSignalingLessBinary64Binary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754CompareSignalingLessEqualBinary32Binary32", ieee754CompareSignalingLessEqualBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingLessEqualBinary32Binary64", ieee754CompareSignalingLessEqualBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingLessEqualBinary64Binary32", ieee754CompareSignalingLessEqualBinary64Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingLessEqualBinary64Binary64", ieee754CompareSignalingLessEqualBinary64Binary64, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},

{"ieee754CompareSignalingNotGreaterBinary32Binary32", ieee754CompareSignalingNotGreaterBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingNotGreaterBinary32Binary64", ieee754CompareSignalingNotGreaterBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754CompareSignalingNotGreaterBinary64Binary32", ieee754CompareSignalingNotGreaterBinary64Binary32, typeint, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingNotGreaterBinary64Binary64", ieee754CompareSignalingNotGreaterBinary64Binary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754CompareSignalingLessUnorderedBinary32Binary32", ieee754CompareSignalingLessUnorderedBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingLessUnorderedBinary32Binary64", ieee754CompareSignalingLessUnorderedBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754CompareSignalingLessUnorderedBinary64Binary32", ieee754CompareSignalingLessUnorderedBinary64Binary32, typeint, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingLessUnorderedBinary64Binary64", ieee754CompareSignalingLessUnorderedBinary64Binary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754CompareSignalingNotLessBinary32Binary32", ieee754CompareSignalingNotLessBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingNotLessBinary32Binary64", ieee754CompareSignalingNotLessBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754CompareSignalingNotLessBinary64Binary32", ieee754CompareSignalingNotLessBinary64Binary32, typeint, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingNotLessBinary64Binary64", ieee754CompareSignalingNotLessBinary64Binary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754CompareSignalingGreaterUnorderedBinary32Binary32", ieee754CompareSignalingGreaterUnorderedBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingGreaterUnorderedBinary32Binary64", ieee754CompareSignalingGreaterUnorderedBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754CompareSignalingGreaterUnorderedBinary64Binary32", ieee754CompareSignalingGreaterUnorderedBinary64Binary32, typeint, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754CompareSignalingGreaterUnorderedBinary64Binary64", ieee754CompareSignalingGreaterUnorderedBinary64Binary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},

  //Quiet comparisons
{"ieee754CompareQuietEqualBinary32Binary32", ieee754CompareQuietEqualBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareQuietEqualBinary32Binary64", ieee754CompareQuietEqualBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754CompareQuietEqualBinary64Binary32", ieee754CompareQuietEqualBinary64Binary32, typeint, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754CompareQuietEqualBinary64Binary64", ieee754CompareQuietEqualBinary64Binary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754CompareQuietNotEqualBinary32Binary32", ieee754CompareQuietNotEqualBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareQuietNotEqualBinary32Binary64", ieee754CompareQuietNotEqualBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754CompareQuietNotEqualBinary64Binary32", ieee754CompareQuietNotEqualBinary64Binary32, typeint, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754CompareQuietNotEqualBinary64Binary64", ieee754CompareQuietNotEqualBinary64Binary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754CompareQuietGreaterBinary32Binary32", ieee754CompareQuietGreaterBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareQuietGreaterBinary32Binary64", ieee754CompareQuietGreaterBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754CompareQuietGreaterBinary64Binary32", ieee754CompareQuietGreaterBinary64Binary32, typeint, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754CompareQuietGreaterBinary64Binary64", ieee754CompareQuietGreaterBinary64Binary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754CompareQuietGreaterEqualBinary32Binary32", ieee754CompareQuietGreaterEqualBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareQuietGreaterEqualBinary32Binary64", ieee754CompareQuietGreaterEqualBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754CompareQuietGreaterEqualBinary64Binary32", ieee754CompareQuietGreaterEqualBinary64Binary32, typeint, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754CompareQuietGreaterEqualBinary64Binary64", ieee754CompareQuietGreaterEqualBinary64Binary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754CompareQuietLessBinary32Binary32",ieee754CompareQuietLessBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareQuietLessBinary32Binary64",ieee754CompareQuietLessBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754CompareQuietLessBinary64Binary32",ieee754CompareQuietLessBinary64Binary32, typeint, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754CompareQuietLessBinary64Binary64",ieee754CompareQuietLessBinary64Binary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754CompareQuietLessEqualBinary32Binary32", ieee754CompareQuietLessEqualBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareQuietLessEqualBinary32Binary64", ieee754CompareQuietLessEqualBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareQuietLessEqualBinary64Binary32", ieee754CompareQuietLessEqualBinary64Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareQuietLessEqualBinary64Binary64", ieee754CompareQuietLessEqualBinary64Binary64, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},

{"ieee754CompareQuietNotGreaterBinary32Binary32", ieee754CompareQuietNotGreaterBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareQuietNotGreaterBinary32Binary64", ieee754CompareQuietNotGreaterBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754CompareQuietNotGreaterBinary64Binary32", ieee754CompareQuietNotGreaterBinary64Binary32, typeint, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754CompareQuietNotGreaterBinary64Binary64", ieee754CompareQuietNotGreaterBinary64Binary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754CompareQuietLessUnorderedBinary32Binary32", ieee754CompareQuietLessUnorderedBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareQuietLessUnorderedBinary32Binary64", ieee754CompareQuietLessUnorderedBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754CompareQuietLessUnorderedBinary64Binary32", ieee754CompareQuietLessUnorderedBinary64Binary32, typeint, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754CompareQuietLessUnorderedBinary64Binary64", ieee754CompareQuietLessUnorderedBinary64Binary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754CompareQuietNotLessBinary32Binary32", ieee754CompareQuietNotLessBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareQuietNotLessBinary32Binary64", ieee754CompareQuietNotLessBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754CompareQuietNotLessBinary64Binary32", ieee754CompareQuietNotLessBinary64Binary32, typeint, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754CompareQuietNotLessBinary64Binary64", ieee754CompareQuietNotLessBinary64Binary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754CompareQuietGreaterUnorderedBinary32Binary32", ieee754CompareQuietGreaterUnorderedBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareQuietGreaterUnorderedBinary32Binary64", ieee754CompareQuietGreaterUnorderedBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754CompareQuietGreaterUnorderedBinary64Binary32", ieee754CompareQuietGreaterUnorderedBinary64Binary32, typeint, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754CompareQuietGreaterUnorderedBinary64Binary64", ieee754CompareQuietGreaterUnorderedBinary64Binary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754CompareQuietOrderedBinary32Binary32", ieee754CompareQuietOrderedBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareQuietOrderedBinary32Binary64", ieee754CompareQuietOrderedBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754CompareQuietOrderedBinary64Binary32", ieee754CompareQuietOrderedBinary64Binary32, typeint, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754CompareQuietOrderedBinary64Binary64", ieee754CompareQuietOrderedBinary64Binary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754CompareQuietUnorderedBinary32Binary32", ieee754CompareQuietUnorderedBinary32Binary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CompareQuietUnorderedBinary32Binary64", ieee754CompareQuietUnorderedBinary32Binary64, typeint, {typeieee754binary32, typeieee754binary64, typeempty}},
{"ieee754CompareQuietUnorderedBinary64Binary32", ieee754CompareQuietUnorderedBinary64Binary32, typeint, {typeieee754binary64, typeieee754binary32, typeempty}},
{"ieee754CompareQuietUnorderedBinary64Binary64", ieee754CompareQuietUnorderedBinary64Binary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},


//{"ieee754IsSignMinusBinary32", ieee754IsSignMinusBinary32, typeint, {typeieee754binary32, typeempty, typeempty}},

//{"ieee754IsSignMinusBinary32", ieee754IsSignMinusBinary32, typeint32_t, {typeieee754binary32, typeempty, typeempty}},


//roundings

{"ieee754Binary32RoundToIntegralExactBinary32", ieee754Binary32RoundToIntegralExactBinary32, typeieee754binary32, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Binary64RoundToIntegralExactBinary64", ieee754Binary64RoundToIntegralExactBinary64, typeieee754binary64, {typeieee754binary64, typeempty, typeempty}},

{"ieee754Binary32RoundToIntegralTowardZeroBinary32",ieee754Binary32RoundToIntegralTowardZeroBinary32, typeieee754binary32, {typeieee754binary32,typeempty,typeempty}},
{"ieee754Binary64RoundToIntegralTowardZeroBinary64",ieee754Binary64RoundToIntegralTowardZeroBinary64,typeieee754binary64,{typeieee754binary64,typeempty,typeempty}},

{"ieee754Binary32RoundToIntegralTowardPositiveBinary32", ieee754Binary32RoundToIntegralTowardPositiveBinary32, typeieee754binary32, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Binary64RoundToIntegralTowardPositiveBinary64", ieee754Binary64RoundToIntegralTowardPositiveBinary64, typeieee754binary64, {typeieee754binary64, typeempty, typeempty}},


{"ieee754Binary32RoundToIntegralTowardNegativeBinary32", ieee754Binary32RoundToIntegralTowardNegativeBinary32, typeieee754binary32, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Binary64RoundToIntegralTowardNegativeBinary64", ieee754Binary64RoundToIntegralTowardNegativeBinary64, typeieee754binary64, {typeieee754binary64, typeempty, typeempty}},


{"ieee754Binary32RoundToIntegralTiesToAwayBinary32", ieee754Binary32RoundToIntegralTiesToAwayBinary32, typeieee754binary32, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Binary64RoundToIntegralTiesToAwayBinary64", ieee754Binary64RoundToIntegralTiesToAwayBinary64, typeieee754binary64, {typeieee754binary64, typeempty, typeempty}},


{"ieee754Binary32RoundToIntegralTiesToEvenBinary32", ieee754Binary32RoundToIntegralTiesToEvenBinary32, typeieee754binary32, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Binary64RoundToIntegralTiesToEvenBinary64", ieee754Binary64RoundToIntegralTiesToEvenBinary64, typeieee754binary64, {typeieee754binary64, typeempty, typeempty}},


//Conversions to (u)intXX_t

//TowardZero
{"ieee754UInt8ConvertToIntegerTowardZeroBinary32", ieee754UInt8ConvertToIntegerTowardZeroBinary32, typeuint8_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt8ConvertToIntegerTowardZeroBinary64", ieee754UInt8ConvertToIntegerTowardZeroBinary64, typeuint8_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int8ConvertToIntegerTowardZeroBinary32", ieee754Int8ConvertToIntegerTowardZeroBinary32, typeint8_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int8ConvertToIntegerTowardZeroBinary64", ieee754Int8ConvertToIntegerTowardZeroBinary64, typeint8_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt16ConvertToIntegerTowardZeroBinary32", ieee754UInt16ConvertToIntegerTowardZeroBinary32, typeuint16_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt16ConvertToIntegerTowardZeroBinary64", ieee754UInt16ConvertToIntegerTowardZeroBinary64, typeuint16_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int16ConvertToIntegerTowardZeroBinary32", ieee754Int16ConvertToIntegerTowardZeroBinary32, typeint16_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int16ConvertToIntegerTowardZeroBinary64", ieee754Int16ConvertToIntegerTowardZeroBinary64, typeint16_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt32ConvertToIntegerTowardZeroBinary32", ieee754UInt32ConvertToIntegerTowardZeroBinary32, typeuint32_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt32ConvertToIntegerTowardZeroBinary64", ieee754UInt32ConvertToIntegerTowardZeroBinary64, typeuint32_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int32ConvertToIntegerTowardZeroBinary32", ieee754Int32ConvertToIntegerTowardZeroBinary32, typeint32_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int32ConvertToIntegerTowardZeroBinary64", ieee754Int32ConvertToIntegerTowardZeroBinary64, typeint32_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt64ConvertToIntegerTowardZeroBinary32", ieee754UInt64ConvertToIntegerTowardZeroBinary32, typeuint64_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt64ConvertToIntegerTowardZeroBinary64", ieee754UInt64ConvertToIntegerTowardZeroBinary64, typeuint64_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int64ConvertToIntegerTowardZeroBinary32", ieee754Int64ConvertToIntegerTowardZeroBinary32, typeint64_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int64ConvertToIntegerTowardZeroBinary64", ieee754Int64ConvertToIntegerTowardZeroBinary64, typeint64_t, {typeieee754binary64, typeempty, typeempty}},
//TowardPositive
{"ieee754UInt8ConvertToIntegerTowardPositiveBinary32", ieee754UInt8ConvertToIntegerTowardPositiveBinary32, typeuint8_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int8ConvertToIntegerTowardPositiveBinary32", ieee754Int8ConvertToIntegerTowardPositiveBinary32, typeint8_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt16ConvertToIntegerTowardPositiveBinary32", ieee754UInt16ConvertToIntegerTowardPositiveBinary32, typeuint16_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int16ConvertToIntegerTowardPositiveBinary32", ieee754Int16ConvertToIntegerTowardPositiveBinary32, typeint16_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt32ConvertToIntegerTowardPositiveBinary32", ieee754UInt32ConvertToIntegerTowardPositiveBinary32, typeuint32_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int32ConvertToIntegerTowardPositiveBinary32", ieee754Int32ConvertToIntegerTowardPositiveBinary32, typeint32_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt64ConvertToIntegerTowardPositiveBinary32", ieee754UInt64ConvertToIntegerTowardPositiveBinary32, typeuint64_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int64ConvertToIntegerTowardPositiveBinary32", ieee754Int64ConvertToIntegerTowardPositiveBinary32, typeint64_t, {typeieee754binary32, typeempty, typeempty}},

{"ieee754UInt8ConvertToIntegerTowardPositiveBinary64", ieee754UInt8ConvertToIntegerTowardPositiveBinary64, typeuint8_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int8ConvertToIntegerTowardPositiveBinary64", ieee754Int8ConvertToIntegerTowardPositiveBinary64, typeint8_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt16ConvertToIntegerTowardPositiveBinary64", ieee754UInt16ConvertToIntegerTowardPositiveBinary64, typeuint16_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int16ConvertToIntegerTowardPositiveBinary64", ieee754Int16ConvertToIntegerTowardPositiveBinary64, typeint16_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt32ConvertToIntegerTowardPositiveBinary64", ieee754UInt32ConvertToIntegerTowardPositiveBinary64, typeuint32_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int32ConvertToIntegerTowardPositiveBinary64", ieee754Int32ConvertToIntegerTowardPositiveBinary64, typeint32_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt64ConvertToIntegerTowardPositiveBinary64", ieee754UInt64ConvertToIntegerTowardPositiveBinary64, typeuint64_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int64ConvertToIntegerTowardPositiveBinary64", ieee754Int64ConvertToIntegerTowardPositiveBinary64, typeint64_t, {typeieee754binary64, typeempty, typeempty}},

//TowardNegative
{"ieee754UInt8ConvertToIntegerTowardNegativeBinary32", ieee754UInt8ConvertToIntegerTowardNegativeBinary32, typeuint8_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int8ConvertToIntegerTowardNegativeBinary32", ieee754Int8ConvertToIntegerTowardNegativeBinary32, typeint8_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt16ConvertToIntegerTowardNegativeBinary32", ieee754UInt16ConvertToIntegerTowardNegativeBinary32, typeuint16_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int16ConvertToIntegerTowardNegativeBinary32", ieee754Int16ConvertToIntegerTowardNegativeBinary32, typeint16_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt32ConvertToIntegerTowardNegativeBinary32", ieee754UInt32ConvertToIntegerTowardNegativeBinary32, typeuint32_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int32ConvertToIntegerTowardNegativeBinary32", ieee754Int32ConvertToIntegerTowardNegativeBinary32, typeint32_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt64ConvertToIntegerTowardNegativeBinary32", ieee754UInt64ConvertToIntegerTowardNegativeBinary32, typeuint64_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int64ConvertToIntegerTowardNegativeBinary32", ieee754Int64ConvertToIntegerTowardNegativeBinary32, typeint64_t, {typeieee754binary32, typeempty, typeempty}},

{"ieee754UInt8ConvertToIntegerTowardNegativeBinary64", ieee754UInt8ConvertToIntegerTowardNegativeBinary64, typeuint8_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int8ConvertToIntegerTowardNegativeBinary64", ieee754Int8ConvertToIntegerTowardNegativeBinary64, typeint8_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt16ConvertToIntegerTowardNegativeBinary64", ieee754UInt16ConvertToIntegerTowardNegativeBinary64, typeuint16_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int16ConvertToIntegerTowardNegativeBinary64", ieee754Int16ConvertToIntegerTowardNegativeBinary64, typeint16_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt32ConvertToIntegerTowardNegativeBinary64", ieee754UInt32ConvertToIntegerTowardNegativeBinary64, typeuint32_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int32ConvertToIntegerTowardNegativeBinary64", ieee754Int32ConvertToIntegerTowardNegativeBinary64, typeint32_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt64ConvertToIntegerTowardNegativeBinary64", ieee754UInt64ConvertToIntegerTowardNegativeBinary64, typeuint64_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int64ConvertToIntegerTowardNegativeBinary64", ieee754Int64ConvertToIntegerTowardNegativeBinary64, typeint64_t, {typeieee754binary64, typeempty, typeempty}},

//ToEven
{"ieee754UInt8ConvertToIntegerTiesToEvenBinary32", ieee754UInt8ConvertToIntegerTiesToEvenBinary32, typeuint8_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int8ConvertToIntegerTiesToEvenBinary32", ieee754Int8ConvertToIntegerTiesToEvenBinary32, typeint8_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt16ConvertToIntegerTiesToEvenBinary32", ieee754UInt16ConvertToIntegerTiesToEvenBinary32, typeuint16_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int16ConvertToIntegerTiesToEvenBinary32", ieee754Int16ConvertToIntegerTiesToEvenBinary32, typeint16_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt32ConvertToIntegerTiesToEvenBinary32", ieee754UInt32ConvertToIntegerTiesToEvenBinary32, typeuint32_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int32ConvertToIntegerTiesToEvenBinary32", ieee754Int32ConvertToIntegerTiesToEvenBinary32, typeint32_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt64ConvertToIntegerTiesToEvenBinary32", ieee754UInt64ConvertToIntegerTiesToEvenBinary32, typeuint64_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int64ConvertToIntegerTiesToEvenBinary32", ieee754Int64ConvertToIntegerTiesToEvenBinary32, typeint64_t, {typeieee754binary32, typeempty, typeempty}},

{"ieee754UInt8ConvertToIntegerTiesToEvenBinary64", ieee754UInt8ConvertToIntegerTiesToEvenBinary64, typeuint8_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int8ConvertToIntegerTiesToEvenBinary64", ieee754Int8ConvertToIntegerTiesToEvenBinary64, typeint8_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt16ConvertToIntegerTiesToEvenBinary64", ieee754UInt16ConvertToIntegerTiesToEvenBinary64, typeuint16_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int16ConvertToIntegerTiesToEvenBinary64", ieee754Int16ConvertToIntegerTiesToEvenBinary64, typeint16_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt32ConvertToIntegerTiesToEvenBinary64", ieee754UInt32ConvertToIntegerTiesToEvenBinary64, typeuint32_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int32ConvertToIntegerTiesToEvenBinary64", ieee754Int32ConvertToIntegerTiesToEvenBinary64, typeint32_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt64ConvertToIntegerTiesToEvenBinary64", ieee754UInt64ConvertToIntegerTiesToEvenBinary64, typeuint64_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int64ConvertToIntegerTiesToEvenBinary64", ieee754Int64ConvertToIntegerTiesToEvenBinary64, typeint64_t, {typeieee754binary64, typeempty, typeempty}},

//ToAway

{"ieee754UInt8ConvertToIntegerTiesToAwayBinary32", ieee754UInt8ConvertToIntegerTiesToAwayBinary32, typeuint8_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int8ConvertToIntegerTiesToAwayBinary32", ieee754Int8ConvertToIntegerTiesToAwayBinary32, typeint8_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt16ConvertToIntegerTiesToAwayBinary32", ieee754UInt16ConvertToIntegerTiesToAwayBinary32, typeuint16_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int16ConvertToIntegerTiesToAwayBinary32", ieee754Int16ConvertToIntegerTiesToAwayBinary32, typeint16_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt32ConvertToIntegerTiesToAwayBinary32", ieee754UInt32ConvertToIntegerTiesToAwayBinary32, typeuint32_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int32ConvertToIntegerTiesToAwayBinary32", ieee754Int32ConvertToIntegerTiesToAwayBinary32, typeint32_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt64ConvertToIntegerTiesToAwayBinary32", ieee754UInt64ConvertToIntegerTiesToAwayBinary32, typeuint64_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int64ConvertToIntegerTiesToAwayBinary32", ieee754Int64ConvertToIntegerTiesToAwayBinary32, typeint64_t, {typeieee754binary32, typeempty, typeempty}},

{"ieee754UInt8ConvertToIntegerTiesToAwayBinary64", ieee754UInt8ConvertToIntegerTiesToAwayBinary64, typeuint8_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int8ConvertToIntegerTiesToAwayBinary64", ieee754Int8ConvertToIntegerTiesToAwayBinary64, typeint8_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt16ConvertToIntegerTiesToAwayBinary64", ieee754UInt16ConvertToIntegerTiesToAwayBinary64, typeuint16_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int16ConvertToIntegerTiesToAwayBinary64", ieee754Int16ConvertToIntegerTiesToAwayBinary64, typeint16_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt32ConvertToIntegerTiesToAwayBinary64", ieee754UInt32ConvertToIntegerTiesToAwayBinary64, typeuint32_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int32ConvertToIntegerTiesToAwayBinary64", ieee754Int32ConvertToIntegerTiesToAwayBinary64, typeint32_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt64ConvertToIntegerTiesToAwayBinary64", ieee754UInt64ConvertToIntegerTiesToAwayBinary64, typeuint64_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int64ConvertToIntegerTiesToAwayBinary64", ieee754Int64ConvertToIntegerTiesToAwayBinary64, typeint64_t, {typeieee754binary64, typeempty, typeempty}},

//Exact Conversions


//TowardZero
{"ieee754UInt8ConvertToIntegerExactTowardZeroBinary32", ieee754UInt8ConvertToIntegerExactTowardZeroBinary32, typeuint8_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt8ConvertToIntegerExactTowardZeroBinary64", ieee754UInt8ConvertToIntegerExactTowardZeroBinary64, typeuint8_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int8ConvertToIntegerExactTowardZeroBinary32", ieee754Int8ConvertToIntegerExactTowardZeroBinary32, typeint8_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int8ConvertToIntegerExactTowardZeroBinary64", ieee754Int8ConvertToIntegerExactTowardZeroBinary64, typeint8_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt16ConvertToIntegerExactTowardZeroBinary32", ieee754UInt16ConvertToIntegerExactTowardZeroBinary32, typeuint16_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt16ConvertToIntegerExactTowardZeroBinary64", ieee754UInt16ConvertToIntegerExactTowardZeroBinary64, typeuint16_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int16ConvertToIntegerExactTowardZeroBinary32", ieee754Int16ConvertToIntegerExactTowardZeroBinary32, typeint16_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int16ConvertToIntegerExactTowardZeroBinary64", ieee754Int16ConvertToIntegerExactTowardZeroBinary64, typeint16_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt32ConvertToIntegerExactTowardZeroBinary32", ieee754UInt32ConvertToIntegerExactTowardZeroBinary32, typeuint32_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt32ConvertToIntegerExactTowardZeroBinary64", ieee754UInt32ConvertToIntegerExactTowardZeroBinary64, typeuint32_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int32ConvertToIntegerExactTowardZeroBinary32", ieee754Int32ConvertToIntegerExactTowardZeroBinary32, typeint32_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int32ConvertToIntegerExactTowardZeroBinary64", ieee754Int32ConvertToIntegerExactTowardZeroBinary64, typeint32_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt64ConvertToIntegerExactTowardZeroBinary32", ieee754UInt64ConvertToIntegerExactTowardZeroBinary32, typeuint64_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt64ConvertToIntegerExactTowardZeroBinary64", ieee754UInt64ConvertToIntegerExactTowardZeroBinary64, typeuint64_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int64ConvertToIntegerExactTowardZeroBinary32", ieee754Int64ConvertToIntegerExactTowardZeroBinary32, typeint64_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int64ConvertToIntegerExactTowardZeroBinary64", ieee754Int64ConvertToIntegerExactTowardZeroBinary64, typeint64_t, {typeieee754binary64, typeempty, typeempty}},


//TowardPositive
{"ieee754UInt8ConvertToIntegerExactTowardPositiveBinary32", ieee754UInt8ConvertToIntegerExactTowardPositiveBinary32, typeuint8_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int8ConvertToIntegerExactTowardPositiveBinary32", ieee754Int8ConvertToIntegerExactTowardPositiveBinary32, typeint8_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt16ConvertToIntegerExactTowardPositiveBinary32", ieee754UInt16ConvertToIntegerExactTowardPositiveBinary32, typeuint16_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int16ConvertToIntegerExactTowardPositiveBinary32", ieee754Int16ConvertToIntegerExactTowardPositiveBinary32, typeint16_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt32ConvertToIntegerExactTowardPositiveBinary32", ieee754UInt32ConvertToIntegerExactTowardPositiveBinary32, typeuint32_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int32ConvertToIntegerExactTowardPositiveBinary32", ieee754Int32ConvertToIntegerExactTowardPositiveBinary32, typeint32_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt64ConvertToIntegerExactTowardPositiveBinary32", ieee754UInt64ConvertToIntegerExactTowardPositiveBinary32, typeuint64_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int64ConvertToIntegerExactTowardPositiveBinary32", ieee754Int64ConvertToIntegerExactTowardPositiveBinary32, typeint64_t, {typeieee754binary32, typeempty, typeempty}},

{"ieee754UInt8ConvertToIntegerExactTowardPositiveBinary64", ieee754UInt8ConvertToIntegerExactTowardPositiveBinary64, typeuint8_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int8ConvertToIntegerExactTowardPositiveBinary64", ieee754Int8ConvertToIntegerExactTowardPositiveBinary64, typeint8_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt16ConvertToIntegerExactTowardPositiveBinary64", ieee754UInt16ConvertToIntegerExactTowardPositiveBinary64, typeuint16_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int16ConvertToIntegerExactTowardPositiveBinary64", ieee754Int16ConvertToIntegerExactTowardPositiveBinary64, typeint16_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt32ConvertToIntegerExactTowardPositiveBinary64", ieee754UInt32ConvertToIntegerExactTowardPositiveBinary64, typeuint32_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int32ConvertToIntegerExactTowardPositiveBinary64", ieee754Int32ConvertToIntegerExactTowardPositiveBinary64, typeint32_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt64ConvertToIntegerExactTowardPositiveBinary64", ieee754UInt64ConvertToIntegerExactTowardPositiveBinary64, typeuint64_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int64ConvertToIntegerExactTowardPositiveBinary64", ieee754Int64ConvertToIntegerExactTowardPositiveBinary64, typeint64_t, {typeieee754binary64, typeempty, typeempty}},

//TowardNegative
{"ieee754UInt8ConvertToIntegerExactTowardNegativeBinary32", ieee754UInt8ConvertToIntegerExactTowardNegativeBinary32, typeuint8_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int8ConvertToIntegerExactTowardNegativeBinary32", ieee754Int8ConvertToIntegerExactTowardNegativeBinary32, typeint8_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt16ConvertToIntegerExactTowardNegativeBinary32", ieee754UInt16ConvertToIntegerExactTowardNegativeBinary32, typeuint16_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int16ConvertToIntegerExactTowardNegativeBinary32", ieee754Int16ConvertToIntegerExactTowardNegativeBinary32, typeint16_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt32ConvertToIntegerExactTowardNegativeBinary32", ieee754UInt32ConvertToIntegerExactTowardNegativeBinary32, typeuint32_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int32ConvertToIntegerExactTowardNegativeBinary32", ieee754Int32ConvertToIntegerExactTowardNegativeBinary32, typeint32_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt64ConvertToIntegerExactTowardNegativeBinary32", ieee754UInt64ConvertToIntegerExactTowardNegativeBinary32, typeuint64_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int64ConvertToIntegerExactTowardNegativeBinary32", ieee754Int64ConvertToIntegerExactTowardNegativeBinary32, typeint64_t, {typeieee754binary32, typeempty, typeempty}},

{"ieee754UInt8ConvertToIntegerExactTowardNegativeBinary64", ieee754UInt8ConvertToIntegerExactTowardNegativeBinary64, typeuint8_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int8ConvertToIntegerExactTowardNegativeBinary64", ieee754Int8ConvertToIntegerExactTowardNegativeBinary64, typeint8_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt16ConvertToIntegerExactTowardNegativeBinary64", ieee754UInt16ConvertToIntegerExactTowardNegativeBinary64, typeuint16_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int16ConvertToIntegerExactTowardNegativeBinary64", ieee754Int16ConvertToIntegerExactTowardNegativeBinary64, typeint16_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt32ConvertToIntegerExactTowardNegativeBinary64", ieee754UInt32ConvertToIntegerExactTowardNegativeBinary64, typeuint32_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int32ConvertToIntegerExactTowardNegativeBinary64", ieee754Int32ConvertToIntegerExactTowardNegativeBinary64, typeint32_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt64ConvertToIntegerExactTowardNegativeBinary64", ieee754UInt64ConvertToIntegerExactTowardNegativeBinary64, typeuint64_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int64ConvertToIntegerExactTowardNegativeBinary64", ieee754Int64ConvertToIntegerExactTowardNegativeBinary64, typeint64_t, {typeieee754binary64, typeempty, typeempty}},

//ToEven
{"ieee754UInt8ConvertToIntegerExactTiesToEvenBinary32", ieee754UInt8ConvertToIntegerExactTiesToEvenBinary32, typeuint8_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int8ConvertToIntegerExactTiesToEvenBinary32", ieee754Int8ConvertToIntegerExactTiesToEvenBinary32, typeint8_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt16ConvertToIntegerExactTiesToEvenBinary32", ieee754UInt16ConvertToIntegerExactTiesToEvenBinary32, typeuint16_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int16ConvertToIntegerExactTiesToEvenBinary32", ieee754Int16ConvertToIntegerExactTiesToEvenBinary32, typeint16_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt32ConvertToIntegerExactTiesToEvenBinary32", ieee754UInt32ConvertToIntegerExactTiesToEvenBinary32, typeuint32_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int32ConvertToIntegerExactTiesToEvenBinary32", ieee754Int32ConvertToIntegerExactTiesToEvenBinary32, typeint32_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt64ConvertToIntegerExactTiesToEvenBinary32", ieee754UInt64ConvertToIntegerExactTiesToEvenBinary32, typeuint64_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int64ConvertToIntegerExactTiesToEvenBinary32", ieee754Int64ConvertToIntegerExactTiesToEvenBinary32, typeint64_t, {typeieee754binary32, typeempty, typeempty}},

{"ieee754UInt8ConvertToIntegerExactTiesToEvenBinary64", ieee754UInt8ConvertToIntegerExactTiesToEvenBinary64, typeuint8_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int8ConvertToIntegerExactTiesToEvenBinary64", ieee754Int8ConvertToIntegerExactTiesToEvenBinary64, typeint8_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt16ConvertToIntegerExactTiesToEvenBinary64", ieee754UInt16ConvertToIntegerExactTiesToEvenBinary64, typeuint16_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int16ConvertToIntegerExactTiesToEvenBinary64", ieee754Int16ConvertToIntegerExactTiesToEvenBinary64, typeint16_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt32ConvertToIntegerExactTiesToEvenBinary64", ieee754UInt32ConvertToIntegerExactTiesToEvenBinary64, typeuint32_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int32ConvertToIntegerExactTiesToEvenBinary64", ieee754Int32ConvertToIntegerExactTiesToEvenBinary64, typeint32_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt64ConvertToIntegerExactTiesToEvenBinary64", ieee754UInt64ConvertToIntegerExactTiesToEvenBinary64, typeuint64_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int64ConvertToIntegerExactTiesToEvenBinary64", ieee754Int64ConvertToIntegerExactTiesToEvenBinary64, typeint64_t, {typeieee754binary64, typeempty, typeempty}},

//ToAway

{"ieee754UInt8ConvertToIntegerExactTiesToAwayBinary32", ieee754UInt8ConvertToIntegerExactTiesToAwayBinary32, typeuint8_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int8ConvertToIntegerExactTiesToAwayBinary32", ieee754Int8ConvertToIntegerExactTiesToAwayBinary32, typeint8_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt16ConvertToIntegerExactTiesToAwayBinary32", ieee754UInt16ConvertToIntegerExactTiesToAwayBinary32, typeuint16_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int16ConvertToIntegerExactTiesToAwayBinary32", ieee754Int16ConvertToIntegerExactTiesToAwayBinary32, typeint16_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt32ConvertToIntegerExactTiesToAwayBinary32", ieee754UInt32ConvertToIntegerExactTiesToAwayBinary32, typeuint32_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int32ConvertToIntegerExactTiesToAwayBinary32", ieee754Int32ConvertToIntegerExactTiesToAwayBinary32, typeint32_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754UInt64ConvertToIntegerExactTiesToAwayBinary32", ieee754UInt64ConvertToIntegerExactTiesToAwayBinary32, typeuint64_t, {typeieee754binary32, typeempty, typeempty}},
{"ieee754Int64ConvertToIntegerExactTiesToAwayBinary32", ieee754Int64ConvertToIntegerExactTiesToAwayBinary32, typeint64_t, {typeieee754binary32, typeempty, typeempty}},

{"ieee754UInt8ConvertToIntegerExactTiesToAwayBinary64", ieee754UInt8ConvertToIntegerExactTiesToAwayBinary64, typeuint8_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int8ConvertToIntegerExactTiesToAwayBinary64", ieee754Int8ConvertToIntegerExactTiesToAwayBinary64, typeint8_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt16ConvertToIntegerExactTiesToAwayBinary64", ieee754UInt16ConvertToIntegerExactTiesToAwayBinary64, typeuint16_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int16ConvertToIntegerExactTiesToAwayBinary64", ieee754Int16ConvertToIntegerExactTiesToAwayBinary64, typeint16_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt32ConvertToIntegerExactTiesToAwayBinary64", ieee754UInt32ConvertToIntegerExactTiesToAwayBinary64, typeuint32_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int32ConvertToIntegerExactTiesToAwayBinary64", ieee754Int32ConvertToIntegerExactTiesToAwayBinary64, typeint32_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754UInt64ConvertToIntegerExactTiesToAwayBinary64", ieee754UInt64ConvertToIntegerExactTiesToAwayBinary64, typeuint64_t, {typeieee754binary64, typeempty, typeempty}},
{"ieee754Int64ConvertToIntegerExactTiesToAwayBinary64", ieee754Int64ConvertToIntegerExactTiesToAwayBinary64, typeint64_t, {typeieee754binary64, typeempty, typeempty}},



//Symbol conversions
{"ieee754Binary64ConvertFromDecimalCharacter", ieee754Binary64ConvertFromDecimalCharacter, typeieee754binary64, {typecharptr,  typeempty, typeempty}},


{"ieee754TotalOrderBinary32", ieee754TotalOrderBinary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754TotalOrderBinary64", ieee754TotalOrderBinary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754TotalOrderMagBinary32", ieee754TotalOrderMagBinary32, typeint, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754TotalOrderMagBinary64", ieee754TotalOrderMagBinary64, typeint, {typeieee754binary64, typeieee754binary64, typeempty}},


{"ieee754Binary32ScaleBBinary32Int", ieee754Binary32ScaleBBinary32Int, typeieee754binary32, {typeieee754binary32, typeint32_t, typeempty}},
{"ieee754Binary64ScaleBBinary64Int", ieee754Binary64ScaleBBinary64Int, typeieee754binary64, {typeieee754binary64, typeint32_t, typeempty}},

/*
{"ieee754Binary32ScaleBBinary32Binary32", ieee754Binary32ScaleBBinary32Binary32, typeieee754binary32, typeieee754binary32, typeieee754binary32, typeempty},
{"ieee754Binary64ScaleBBinary64Binary32", ieee754Binary64ScaleBBinary64Binary32, typeieee754binary64, typeieee754binary64, typeieee754binary32, typeempty},
*/
{"ieee754IntLogBBinary32", ieee754IntLogBBinary32, typeint32_t, {typeieee754binary32, typeempty}},
{"ieee754IntLogBBinary64", ieee754IntLogBBinary64, typeint32_t, {typeieee754binary64, typeempty}},
/*
{"ieee754Binary32LogBBinary32", ieee754Binary32LogBBinary32, typeieee754binary32, typeieee754binary32, typeempty},
{"ieee754Binary64LogBBinary64", ieee754Binary64LogBBinary64, typeieee754binary64, typeieee754binary64, typeempty},
*/
{"ieee754Binary32NextUpBinary32", ieee754Binary32NextUpBinary32, typeieee754binary32, {typeieee754binary32, typeempty}},
{"ieee754Binary64NextUpBinary64", ieee754Binary64NextUpBinary64, typeieee754binary64, {typeieee754binary64, typeempty}},
{"ieee754Binary32NextDownBinary32", ieee754Binary32NextDownBinary32, typeieee754binary32, {typeieee754binary32, typeempty}},
{"ieee754Binary64NextDownBinary64", ieee754Binary64NextDownBinary64, typeieee754binary64, {typeieee754binary64, typeempty}},

{"ieee754Binary32MinNumBinary32Binary32", ieee754Binary32MinNumBinary32Binary32, typeieee754binary32, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754Binary64MinNumBinary64Binary64", ieee754Binary64MinNumBinary64Binary64, typeieee754binary64, {typeieee754binary64, typeieee754binary64, typeempty}},
{"ieee754Binary32MaxNumBinary32Binary32", ieee754Binary32MaxNumBinary32Binary32, typeieee754binary32, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754Binary64MaxNumBinary64Binary64", ieee754Binary64MaxNumBinary64Binary64, typeieee754binary64, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754Binary32MinNumMagBinary32Binary32", ieee754Binary32MinNumMagBinary32Binary32, typeieee754binary32, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754Binary64MinNumMagBinary64Binary64", ieee754Binary64MinNumMagBinary64Binary64, typeieee754binary64, {typeieee754binary64, typeieee754binary64, typeempty}},
{"ieee754Binary32MaxNumMagBinary32Binary32", ieee754Binary32MaxNumMagBinary32Binary32, typeieee754binary32, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754Binary64MaxNumMagBinary64Binary64", ieee754Binary64MaxNumMagBinary64Binary64, typeieee754binary64, {typeieee754binary64, typeieee754binary64, typeempty}},

{"ieee754Binary32RemainderBinary32Binary32", ieee754Binary32RemainderBinary32Binary32, typeieee754binary32, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754Binary64RemainderBinary64Binary64", ieee754Binary64RemainderBinary64Binary64, typeieee754binary64, {typeieee754binary64, typeieee754binary64, typeempty}},
/*
{"ieee754CopyBinary32", ieee754CopyBinary32, typeieee754binary32, typeieee754binary32, typeempty, typeempty},
{"ieee754CopyBinary64", ieee754CopyBinary64, typeieee754binary64, typeieee754binary64, typeempty, typeempty},
*/
{"ieee754NegateBinary32", ieee754NegateBinary32, typeieee754binary32, {typeieee754binary32, typeempty, typeempty}},
{"ieee754NegateBinary64", ieee754NegateBinary64, typeieee754binary64, {typeieee754binary64, typeempty, typeempty}},
{"ieee754AbsBinary32", ieee754AbsBinary32, typeieee754binary32, {typeieee754binary32, typeempty, typeempty}},
{"ieee754AbsBinary64", ieee754AbsBinary64, typeieee754binary64, {typeieee754binary64, typeempty, typeempty}},
{"ieee754CopySignBinary32Binary32", ieee754CopySignBinary32Binary32, typeieee754binary32, {typeieee754binary32, typeieee754binary32, typeempty}},
{"ieee754CopySignBinary64Binary64", ieee754CopySignBinary64Binary64, typeieee754binary64, {typeieee754binary64, typeieee754binary64, typeempty}},
};

ieee754FPClass fpClassNotationToFPClass(char* fpclass) {
  if (strcmp(fpclass,"qNaN") == 0 || strcmp(fpclass, "quietNaN") == 0)
    return quietNaN;
  if (strcmp(fpclass,"sNaN") == 0 || strcmp(fpclass, "signalingNaN") == 0)
    return signalingNaN;
  if (strcmp(fpclass,"+INF") == 0 || strcmp(fpclass, "+inf") == 0 || strcmp(fpclass,"INF") == 0 || strcmp(fpclass, "inf") == 0)
    return positiveInfinity;
  if (strcmp(fpclass,"-INF") == 0 || strcmp(fpclass, "-inf") == 0)
    return negativeInfinity;
  if (strcmp(fpclass,"negativeNormal") == 0 || strcmp(fpclass,"-Normal") == 0)
    return negativeNormal;
  if (strcmp(fpclass,"positiveNormal") == 0 || strcmp(fpclass,"Normal") == 0 || strcmp(fpclass,"+Normal") == 0)
   return positiveNormal;
  if (strcmp(fpclass,"negativeSubnormal") == 0 || strcmp(fpclass,"-Subnormal") == 0)
    return negativeSubnormal;
  if (strcmp(fpclass,"positiveSubnormal") == 0 || strcmp(fpclass,"Subnormal") == 0 || strcmp(fpclass,"+Subnormal") == 0)
    return positiveSubnormal;
  if (strcmp(fpclass,"positiveZero") == 0 || strcmp(fpclass,"Zero") == 0 || strcmp(fpclass,"+Zero") == 0 || strcmp(fpclass,"0") == 0 || strcmp(fpclass,"+0") == 0)
    return positiveZero;
  if (strcmp(fpclass,"negativeZero") == 0 || strcmp(fpclass,"-Zero") == 0 || strcmp(fpclass,"-0") == 0)
    return negativeZero;
}

int intBooleanNotationToInt(char* truefalse) {
  if (strcmp(truefalse, "true") == 0 || strcmp(truefalse, "1") == 0) {
    return 1;
  } else {  
    return 0;
  }
}

ieee754binary64 doubleMemoryNotationToDouble(uint64_t op) {
  bin64Caster xdb;
  xdb.l = op;  
  return xdb.d;
}

uint64_t doubleToMemoryNotation(ieee754binary64 bin64) {
  bin64Caster xdb;
  xdb.d = bin64;
  return xdb.l;
}

ieee754binary32 floatMemoryNotationToFloat(uint32_t op) {
  bin32Caster xdb;
  xdb.i = op;  
  return xdb.f;
}

uint32_t floatToMemoryNotation(ieee754binary32 bin32) {
  bin32Caster xdb;
  xdb.f = bin32;
  return xdb.i;
}


char* getFuncName(FILE* testFileName) {
   char* funcName = malloc(55); //I hope it would be enough
   int res =fscanf(testFileName, "%s\n", funcName);
   if (res == EOF || res == 0) {
     //     printf("end of file!\n");
     return NULL;
   }   
   return funcName;
}

ieee754IOType* getParameterTypes(char* funcName) {
   int arrLength = sizeof(funcMapTable) / sizeof(funcMapTable[0]);     
   int ind = 0;
   // printf("funcName = %s\n", funcName);
   while (ind < arrLength && strncmp(funcName, (funcMapTable[ind]).name, strlen(funcName)) != 0) {
     // printf("ind= %i\n", ind);
     //  printf("funcMapTable[%i]).name = %s\n", ind, funcMapTable[ind].name);
     ind++;     
   }      
   if (ind == arrLength){
     // printf("Not found your function %s\n", funcName);
     return NULL;    
   }
   //now we have correctly defined index in the function mapping array.
   ieee754IOType* typeArray = malloc(4 * sizeof(ieee754IOType));
   typeArray[0] = funcMapTable[ind].outputType;
   int i=1;
   for (i = 1; i < 4; i++) {
     typeArray[i] = funcMapTable[ind].inputTypes[i-1];
   }
   return typeArray;
}

paramWithType readParam(FILE* openedFile, ieee754IOType type) {
  paramWithType paramStruct;
  paramStruct.type = type;
  uint32_t uitmp32;
  uint8_t uitmp8;
  uint16_t uitmp16;
  uint64_t uitmp64;  
  int8_t itmp8;
  int16_t itmp16;
  int32_t itmp32;
  int64_t itmp64;
  int itmp;
  char* stringrepr = malloc(3000);
  switch (type) {
  case typeieee754binary32:     
    itmp16 = fscanf(openedFile, "%x", &uitmp32);
     if (itmp16 == 0 || itmp16 == EOF) {
       printf("Unexpected end of file \n");
       exit(EXIT_FAILURE);      
    }    
    paramStruct.param.pbin32 = floatMemoryNotationToFloat(uitmp32);
    break;
  case typeieee754binary64:
    itmp32 = fscanf(openedFile, "%Lx", &uitmp64);
    if (itmp32 == 0 || itmp32 == EOF) {
      printf("Unexpected end of file \n");
      exit(EXIT_FAILURE);      
    }    
    paramStruct.param.pbin64 = doubleMemoryNotationToDouble(uitmp64);
    break;
  case typeuint8_t:    
    itmp32 = fscanf(openedFile, "%u", &uitmp8);
    if (itmp32 == 0 || itmp32 == EOF) {
      printf("Unexpected end of file \n");
      exit(EXIT_FAILURE);
    }
    paramStruct.param.puint8 = (uint8_t) uitmp8;
    break;
  case typeuint16_t:   
    itmp32 = fscanf(openedFile, "%u", &uitmp16);
    if (itmp32 == 0 || itmp32 == EOF) {
      printf("Unexpected end of file \n");
      exit(EXIT_FAILURE);   
    }
    paramStruct.param.puint16 = (uint16_t) uitmp16;
    break;
  case typeuint32_t:    
    itmp16 = fscanf(openedFile, "%u", &uitmp32);
    if (itmp16 == 0 || itmp16 == EOF) {
      printf("Unexpected end of file \n");
      exit(EXIT_FAILURE);   
    }
    paramStruct.param.puint32 = (uint32_t) uitmp32;
    break;
  case typeuint64_t:   
    itmp32 = fscanf(openedFile, "%Lu", &uitmp64);
    if (itmp32 == 0 || itmp32 == EOF) {
      printf("Unexpected end of file \n");
      exit(EXIT_FAILURE);   
    }
    paramStruct.param.puint64 = (uint64_t) uitmp64;
    break;
  case typeint8_t:    
    itmp32 = fscanf(openedFile, "%i", &itmp8);
    if (itmp32 == 0 || itmp32 == EOF) {
      printf("Unexpected end of file \n");
      exit(EXIT_FAILURE);   
    }
    paramStruct.param.pint8 = (int8_t) itmp8;
    break;
  case typeint16_t:    
    itmp32 = fscanf(openedFile, "%i", &itmp16);
    if (itmp32 == 0 || itmp32 == EOF) {
      printf("Unexpected end of file \n");
      exit(EXIT_FAILURE);   
    }
    paramStruct.param.pint16 = (int16_t) itmp16;
    break;
  case typeint32_t:    
    itmp16 = fscanf(openedFile, "%i", &itmp32);
    if (itmp16 == 0 || itmp16 == EOF) {
      printf("Unexpected end of file \n");
      exit(EXIT_FAILURE);   
    }
    paramStruct.param.pint32 = (int32_t) itmp32;
    break;
  case typeint64_t:    
    itmp32 = fscanf(openedFile, "%21Li", &itmp64);
    if (itmp32 == 0 || itmp32 == EOF) {
      printf("Unexpected end of file \n");
      exit(EXIT_FAILURE);   
    }
    paramStruct.param.pint64 = (int64_t) itmp64;
    break;
  case typeexception: 
    itmp16 = fscanf(openedFile, "%u", &uitmp32); 
    if (itmp16 == 0 || itmp16 == EOF) {
      printf("Unexpected end of file \n");
      exit(EXIT_FAILURE);   
    }
    paramStruct.param.pexception = (uint32_t) uitmp32;
    break;   
  case typeint: 
    stringrepr = NULL;
    itmp32 = fscanf(openedFile, "%i", &itmp); 
    if (itmp32 == 0 || itmp32 == EOF) {
      printf("Unexpected end of file \n");
      exit(EXIT_FAILURE);   
    }
    paramStruct.param.pint = itmp; //intBooleanNotationToInt(stringrepr);    
    break;
  case typefpclass:   
    itmp32 = fscanf(openedFile, "%s", stringrepr);
    if (itmp32 == 0 || itmp32 == EOF) {
      printf("Unexpected end of file \n");
      exit(EXIT_FAILURE);   
    }
    paramStruct.param.pfpclass =fpClassNotationToFPClass(stringrepr);    
    break;
  case typecharptr:    
    itmp32 = fscanf(openedFile, "%s", stringrepr);
    if (itmp32 == 0 || itmp32 == EOF) {
      printf("Unexpected end of file \n");
      exit(EXIT_FAILURE);   
    }
    paramStruct.param.pcharptr = stringrepr;
    //    printf("geleste parameter = %s\n",     paramStruct.param.pcharptr);
    break;
  case typeempty:   
    break;
  }
  //  free(stringrepr);
  //  stringrepr = NULL;
  return paramStruct;  
}

int readRoundingModeAndFlags(FILE* openedFile, char* rmode, char* flags) {  
  int res = fscanf(openedFile, "%s3", rmode);
  if (res == 0 || res == EOF) {
    printf("Unexpected end of file!\n");
    return 1;
  }
  res = fscanf(openedFile, "%s6", flags); 
  if (res == 0 || res == EOF) {
    printf("Unexpected end of file!\n");
    return 1;
  }
  return 0;// 0 - success, other - fail
}


paramWithType runFunc(char* funcName, paramWithType* inputs, paramWithType output) {
   int arrLength = sizeof(funcMapTable) / sizeof (funcMapTable[0]);     
   int ind = 0;
   while (ind < arrLength && strcmp(funcName, (funcMapTable[ind]).name) != 0) {
     ind++;     
   }   
   possibleParams realOut;
   paramWithType result;
   result.type = output.type;
   void* funcPtr = funcMapTable[ind].funcPointer;
   //CAST!!!!!!!!!!!
   switch (output.type) {
   case typeieee754binary32:
     switch (inputs[2].type) {
     case typeieee754binary32:
       //if third parameter is binaryXX, the only possible function is FMA, that's why we wouldn't check anything except binaryXX-s
       switch (inputs[1].type) {
       case typeieee754binary32:
	 switch (inputs[0].type) {
	 case typeieee754binary32:
	   realOut.pbin32 = (( ieee754binary32 (*)(ieee754binary32, ieee754binary32, ieee754binary32))(funcPtr))(inputs[0].param.pbin32, inputs[1].param.pbin32, inputs[2].param.pbin32);
	   break;
	 case typeieee754binary64:
	   realOut.pbin32 = (( ieee754binary32 (*)(ieee754binary64, ieee754binary32, ieee754binary32))(funcPtr))(inputs[0].param.pbin64, inputs[1].param.pbin32, inputs[2].param.pbin32);	  	   
	   break;
	 }//end switch for 1t param where 2d is bin32, 3d is bin32, out is bin32
	 break;
       case typeieee754binary64:	 
	 switch (inputs[0].type) {
	 case typeieee754binary32:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(ieee754binary32, ieee754binary64, ieee754binary32))(funcPtr))(inputs[0].param.pbin32, inputs[1].param.pbin64, inputs[2].param.pbin32);	  	   
	   break;
	 case typeieee754binary64:
	   realOut.pbin32 = (( ieee754binary32 (*)(ieee754binary64, ieee754binary64, ieee754binary32))(funcPtr))(inputs[0].param.pbin64, inputs[1].param.pbin64, inputs[2].param.pbin32);	  	   
	   break;
	 }//end switch for 1t param where 2d is bin64, 3d is bin32, out is bin32
	 break;
       }//end switch for 2d parameter where 3d is bin32 and output is bin32
       break;

     case typeieee754binary64:       
       //if third parameter is binaryXX, the only possible function is FMA, that's why we wouldn't check anything except binaryXX-s
       switch (inputs[1].type) {
       case typeieee754binary32:	 
	 switch (inputs[0].type) {
	 case typeieee754binary32:
	   realOut.pbin32 = (( ieee754binary32 (*)(ieee754binary32, ieee754binary32, ieee754binary64))(funcPtr))(inputs[0].param.pbin32, inputs[1].param.pbin32, inputs[2].param.pbin64);	      
	   break;
	 case typeieee754binary64:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(ieee754binary64, ieee754binary32, ieee754binary64))(funcPtr))(inputs[0].param.pbin64, inputs[1].param.pbin32, inputs[2].param.pbin64);	   	   
	   break;
	 }//end switch for 1t param where 2d is bin32, 3d is bin64, out is bin32
	 break;
       case typeieee754binary64:	 
	 switch (inputs[0].type) {
	 case typeieee754binary32:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(ieee754binary32, ieee754binary64, ieee754binary64))(funcPtr))(inputs[0].param.pbin32, inputs[1].param.pbin64, inputs[2].param.pbin64);	   	   
	   break;
	 case typeieee754binary64:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(ieee754binary64, ieee754binary64, ieee754binary64))(funcPtr))(inputs[0].param.pbin64, inputs[1].param.pbin64, inputs[2].param.pbin64);	   	   
	   break;
	 }//end switch for 1t param where 2d is bin64, 3d is bin64, out is bin32
	 break;
       }//end switch for 2d parameter where 3d is bin64 and output is bin32
       break; 

     case typeempty:       
       switch (inputs[1].type) {
       case typeieee754binary32:	
	 switch (inputs[0].type) {
	 case typeieee754binary32:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(ieee754binary32, ieee754binary32))(funcPtr))(inputs[0].param.pbin32, inputs[1].param.pbin32);	   
	   break;
	 case typeieee754binary64:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(ieee754binary64, ieee754binary32))(funcPtr))(inputs[0].param.pbin64, inputs[1].param.pbin32);
	   break;
	 case typeuint8_t:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(uint8_t, ieee754binary32))(funcPtr))(inputs[0].param.puint8, inputs[1].param.pbin32);	   
	   break;
	 case typeuint16_t: 	   
	   realOut.pbin32 = (( ieee754binary32 (*)(uint16_t, ieee754binary32))(funcPtr))(inputs[0].param.puint16, inputs[1].param.pbin32);	   
	   break;	  
	 case typeuint32_t:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(uint32_t, ieee754binary32))(funcPtr))(inputs[0].param.puint32, inputs[1].param.pbin32);	   
	   break;	   
	 case typeuint64_t:	  
	   realOut.pbin32 = (( ieee754binary32 (*)(uint64_t, ieee754binary32))(funcPtr))(inputs[0].param.puint64, inputs[1].param.pbin32);	   
	   break;
	 case typeint8_t:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(int8_t, ieee754binary32))(funcPtr))(inputs[0].param.pint8, inputs[1].param.pbin32);	   
	   break;
	 case typeint16_t:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(int16_t, ieee754binary32))(funcPtr))(inputs[0].param.pint16, inputs[1].param.pbin32);	   
	   break;
	 case typeint32_t:
	   realOut.pbin32 = (( ieee754binary32 (*)(int32_t, ieee754binary32))(funcPtr))(inputs[0].param.pint32, inputs[1].param.pbin32);
	   break;
	 case typeint64_t:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(int64_t, ieee754binary32))(funcPtr))(inputs[0].param.pint64, inputs[1].param.pbin32);	  
	   break;
	 case typecharptr:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(char*, ieee754binary32))(funcPtr))(inputs[0].param.pcharptr, inputs[1].param.pbin32);	   	   
	   break;	 
	 }//end switch for 1-parameter when 2nd is bin32, 3d is empty
	 break;
       case typeieee754binary64:	 
	 switch (inputs[0].type) {
	 case typeieee754binary32:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(ieee754binary32, ieee754binary64))(funcPtr))(inputs[0].param.pbin32, inputs[1].param.pbin64);	   
	   break;
	 case typeieee754binary64:	  
	   realOut.pbin32 = (( ieee754binary32 (*)(ieee754binary64, ieee754binary64))(funcPtr))(inputs[0].param.pbin64, inputs[1].param.pbin64);   	   
	   break;
	 case typeuint8_t:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(uint8_t, ieee754binary64))(funcPtr))(inputs[0].param.puint8, inputs[1].param.pbin64);	   
	   break;
	 case typeuint16_t: 	   
	   realOut.pbin32 = (( ieee754binary32 (*)(uint16_t, ieee754binary64))(funcPtr))(inputs[0].param.puint16, inputs[1].param.pbin64);	  
	   break;	  
	 case typeuint32_t:	  
	   realOut.pbin32 = (( ieee754binary32 (*)(uint32_t, ieee754binary64))(funcPtr))(inputs[0].param.puint32, inputs[1].param.pbin64);	   
	   break;	   
	 case typeuint64_t:
	   realOut.pbin32 = (( ieee754binary32 (*)(uint64_t, ieee754binary64))(funcPtr))(inputs[0].param.puint64, inputs[1].param.pbin64);
	   break;
	 case typeint8_t:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(int8_t, ieee754binary64))(funcPtr))(inputs[0].param.pint8, inputs[1].param.pbin64);	   
	   break;
	 case typeint16_t:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(int16_t, ieee754binary64))(funcPtr))(inputs[0].param.pint16, inputs[1].param.pbin64);	
	   break;
	 case typeint32_t:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(int32_t, ieee754binary64))(funcPtr))(inputs[0].param.pint32, inputs[1].param.pbin64);	  
	   break;
	 case typeint64_t:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(int64_t, ieee754binary64))(funcPtr))(inputs[0].param.pint64, inputs[1].param.pbin64);	   
	   break;
	 case typecharptr:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(char*, ieee754binary64))(funcPtr))(inputs[0].param.pcharptr, inputs[1].param.pbin64);	   
	   break;
	 }//end switch for 1-parameter when 2nd is bin64, 3d is empty
	 break;       
       case typeint32_t:	  
	 switch (inputs[0].type) {
	 case typeieee754binary32:	  
	   realOut.pbin32 = (( ieee754binary32 (*)(ieee754binary32, int32_t))(funcPtr))(inputs[0].param.pbin32, inputs[1].param.pint32);	   
	   break;
	 case typeieee754binary64:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(ieee754binary64, int32_t))(funcPtr))(inputs[0].param.pbin64, inputs[1].param.pint32);	   	   
	   break;
	 case typeuint8_t:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(uint8_t, int32_t))(funcPtr))(inputs[0].param.puint8, inputs[1].param.pint32);	   
	   break;
	 case typeuint16_t:	  
	   realOut.pbin32 = (( ieee754binary32 (*)(uint16_t, int32_t))(funcPtr))(inputs[0].param.puint16, inputs[1].param.pint32);	   
	   break;	  
	 case typeuint32_t:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(uint32_t, int32_t))(funcPtr))(inputs[0].param.puint32, inputs[1].param.pint32);	   
	   break;	   
	 case typeuint64_t:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(uint64_t, int32_t))(funcPtr))(inputs[0].param.puint64, inputs[1].param.pint32);	   
	   break;
	 case typeint8_t:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(int8_t, int32_t))(funcPtr))(inputs[0].param.pint8, inputs[1].param.pint32);	   
	   break;
	 case typeint16_t:
	   realOut.pbin32 = (( ieee754binary32 (*)(int16_t, int32_t))(funcPtr))(inputs[0].param.pint16, inputs[1].param.pint32);	   
	   break;
	 case typeint32_t:
	   realOut.pbin32 = (( ieee754binary32 (*)(int32_t, int32_t))(funcPtr))(inputs[0].param.pint32, inputs[1].param.pint32);	   
	   break;
	 case typeint64_t:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(int64_t, int32_t))(funcPtr))(inputs[0].param.pint64, inputs[1].param.pint32);	  
	   break;
	 case typecharptr:	  
	   realOut.pbin32 = (( ieee754binary32 (*)(char*, int32_t))(funcPtr))(inputs[0].param.pcharptr, inputs[1].param.pint32);	   	   
	   break;
	 }//end switch for 1-parameter when 2nd is int32, 3d is empty
	 break;
       case typeempty:
	 switch (inputs[0].type) {
	 case typeieee754binary32:	   	       
	   realOut.pbin32 = (( ieee754binary32 (*)(ieee754binary32, int32_t))(funcPtr))(inputs[0].param.pbin32, inputs[1].param.pint32);	    
	   break;
	 case typeieee754binary64:	 
	   realOut.pbin32 = (( ieee754binary32 (*)(ieee754binary64, int32_t))(funcPtr))(inputs[0].param.pbin64, inputs[1].param.pint32);	   	   
	   break;
	 case typeuint8_t:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(uint8_t, int32_t))(funcPtr))(inputs[0].param.puint8, inputs[1].param.pint32);	   
	   break;
	 case typeuint16_t: 	   
	   realOut.pbin32 = (( ieee754binary32 (*)(uint16_t, int32_t))(funcPtr))(inputs[0].param.puint16, inputs[1].param.pint32);	   
	   break;	  
	 case typeuint32_t:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(uint32_t, int32_t))(funcPtr))(inputs[0].param.puint32, inputs[1].param.pint32);	   
	   break;	   
	 case typeuint64_t:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(uint64_t, int32_t))(funcPtr))(inputs[0].param.puint64, inputs[1].param.pint32);	   
	   break;
	 case typeint8_t:
	   realOut.pbin32 = (( ieee754binary32 (*)(int8_t, int32_t))(funcPtr))(inputs[0].param.pint8, inputs[1].param.pint32);	   
	   break;
	 case typeint16_t:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(int16_t, int32_t))(funcPtr))(inputs[0].param.pint16, inputs[1].param.pint32);	   
	   break;
	 case typeint32_t:	   
	   realOut.pbin32 = (( ieee754binary32 (*)(int32_t, int32_t))(funcPtr))(inputs[0].param.pint32, inputs[1].param.pint32);	   
	   break;
	 case typeint64_t:	  
	   realOut.pbin32 = (( ieee754binary32 (*)(int64_t, int32_t))(funcPtr))(inputs[0].param.pint64, inputs[1].param.pint32);	   
	   break;
	 case typecharptr:	  
	   realOut.pbin32 = (( ieee754binary32 (*)(char*, int32_t))(funcPtr))(inputs[0].param.pcharptr, inputs[1].param.pint32);	   	   
	   break;
	 }//end switch for 1-parameter when 2nd is empty, 3d is empty 
	 break;
       }//end switch for 2-parameter where 3d is empty
       break;
     }//end switch for 3-parameter
     break; // end case for bin32 output

   case typeieee754binary64:

/////////////// bin64 output!!!!!!     
    
     switch (inputs[2].type) {
     case typeieee754binary32:       
       //if third parameter is binaryXX, the only possible function is FMA, that's why we wouldn't check anything except binaryXX-s
       switch (inputs[1].type) {
       case typeieee754binary32:	
	 switch (inputs[0].type) {
	 case typeieee754binary32:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(ieee754binary32, ieee754binary32, ieee754binary32))(funcPtr))(inputs[0].param.pbin32, inputs[1].param.pbin32, inputs[2].param.pbin32);	   	   
	   break;
	 case typeieee754binary64:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(ieee754binary64, ieee754binary32, ieee754binary32))(funcPtr))(inputs[0].param.pbin64, inputs[1].param.pbin32, inputs[2].param.pbin32);	   
	   break;
	 }//end switch for 1t param where 2d is bin32, 3d is bin32, out is bin64
	 break;
       case typeieee754binary64:	
	 switch (inputs[0].type) {
	 case typeieee754binary32:	  
	   realOut.pbin64 = (( ieee754binary64 (*)(ieee754binary32, ieee754binary64, ieee754binary32))(funcPtr))(inputs[0].param.pbin32, inputs[1].param.pbin64, inputs[2].param.pbin32);	   	   
	   break;
	 case typeieee754binary64:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(ieee754binary64, ieee754binary64, ieee754binary32))(funcPtr))(inputs[0].param.pbin64, inputs[1].param.pbin64, inputs[2].param.pbin32);	  	   
	   break;
	 }//end switch for 1t param where 2d is bin64, 3d is bin32, out is bin64
	 break;
       }//end switch for 2d parameter where 3d is bin32 and output is bin64
       break;

     case typeieee754binary64:       
       //if third parameter is binaryXX, the only possible function is FMA, that's why we wouldn't check anything except binaryXX-s
       switch (inputs[1].type) {
       case typeieee754binary32:	
	 switch (inputs[0].type) {
	 case typeieee754binary32:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(ieee754binary32, ieee754binary32, ieee754binary64))(funcPtr))(inputs[0].param.pbin32, inputs[1].param.pbin32, inputs[2].param.pbin64);	   	   
	   break;
	 case typeieee754binary64:
	   realOut.pbin64 = (( ieee754binary64 (*)(ieee754binary64, ieee754binary32, ieee754binary64))(funcPtr))(inputs[0].param.pbin64, inputs[1].param.pbin32, inputs[2].param.pbin64);	  	   
	   break;
	 }//end switch for 1t param where 2d is bin32, 3d is bin64, out is bin64
	 break;
       case typeieee754binary64:	
	 switch (inputs[0].type) {
	 case typeieee754binary32:	  
	   realOut.pbin64 = (( ieee754binary64 (*)(ieee754binary32, ieee754binary64, ieee754binary64))(funcPtr))(inputs[0].param.pbin32, inputs[1].param.pbin64, inputs[2].param.pbin64);	  	   
	   break;
	 case typeieee754binary64:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(ieee754binary64, ieee754binary64, ieee754binary64))(funcPtr))(inputs[0].param.pbin64, inputs[1].param.pbin64, inputs[2].param.pbin64);	   	   
	   break;
	 }//end switch for 1t param where 2d is bin64, 3d is bin64, out is bin64
	 break;
       }//end switch for 2d parameter where 3d is bin64 and output is bin64
       break; 

     case typeempty:       
       switch (inputs[1].type) {
       case typeieee754binary32:	 
	 switch (inputs[0].type) {
	 case typeieee754binary32:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(ieee754binary32, ieee754binary32))(funcPtr))(inputs[0].param.pbin32, inputs[1].param.pbin32);
	   break;
	 case typeieee754binary64:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(ieee754binary64, ieee754binary32))(funcPtr))(inputs[0].param.pbin64, inputs[1].param.pbin32);
	   break;
	 case typeuint8_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(uint8_t, ieee754binary32))(funcPtr))(inputs[0].param.puint8, inputs[1].param.pbin32);	   
	   break;
	 case typeuint16_t: 	   
	   realOut.pbin64 = (( ieee754binary64 (*)(uint16_t, ieee754binary32))(funcPtr))(inputs[0].param.puint16, inputs[1].param.pbin32);	   
	   break;	  
	 case typeuint32_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(uint32_t, ieee754binary32))(funcPtr))(inputs[0].param.puint32, inputs[1].param.pbin32);	   
	   break;	   
	 case typeuint64_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(uint64_t, ieee754binary32))(funcPtr))(inputs[0].param.puint64, inputs[1].param.pbin32);	   
	   break;
	 case typeint8_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(int8_t, ieee754binary32))(funcPtr))(inputs[0].param.pint8, inputs[1].param.pbin32);	   
	   break;
	 case typeint16_t:	  
	   realOut.pbin64 = (( ieee754binary64 (*)(int16_t, ieee754binary32))(funcPtr))(inputs[0].param.pint16, inputs[1].param.pbin32);	  
	   break;
	 case typeint32_t:	  
	   realOut.pbin64 = (( ieee754binary64 (*)(int32_t, ieee754binary32))(funcPtr))(inputs[0].param.pint32, inputs[1].param.pbin32);	   
	   break;
	 case typeint64_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(int64_t, ieee754binary32))(funcPtr))(inputs[0].param.pint64, inputs[1].param.pbin32);	   
	   break;
	 case typecharptr:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(char*, ieee754binary32))(funcPtr))(inputs[0].param.pcharptr, inputs[1].param.pbin32);	   	   
	   break;	 
	 }//end switch for 1-parameter when 2nd is bin32, 3d is empty
	 break;
       case typeieee754binary64:	  
	 switch (inputs[0].type) {
	 case typeieee754binary32:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(ieee754binary32, ieee754binary64))(funcPtr))(inputs[0].param.pbin32, inputs[1].param.pbin64);	   
	   break;
	 case typeieee754binary64:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(ieee754binary64, ieee754binary64))(funcPtr))(inputs[0].param.pbin64, inputs[1].param.pbin64); 	   
	   break;
	 case typeuint8_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(uint8_t, ieee754binary64))(funcPtr))(inputs[0].param.puint8, inputs[1].param.pbin64);	   
	   break;
	 case typeuint16_t: 	  
	   realOut.pbin64 = (( ieee754binary64 (*)(uint16_t, ieee754binary64))(funcPtr))(inputs[0].param.puint16, inputs[1].param.pbin64);	   
	   break;	  
	 case typeuint32_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(uint32_t, ieee754binary64))(funcPtr))(inputs[0].param.puint32, inputs[1].param.pbin64);	   
	   break;	   
	 case typeuint64_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(uint64_t, ieee754binary64))(funcPtr))(inputs[0].param.puint64, inputs[1].param.pbin64);	   
	   break;
	 case typeint8_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(int8_t, ieee754binary64))(funcPtr))(inputs[0].param.pint8, inputs[1].param.pbin64);	  
	   break;
	 case typeint16_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(int16_t, ieee754binary64))(funcPtr))(inputs[0].param.pint16, inputs[1].param.pbin64);	  
	   break;
	 case typeint32_t:	
	   realOut.pbin64 = (( ieee754binary64 (*)(int32_t, ieee754binary64))(funcPtr))(inputs[0].param.pint32, inputs[1].param.pbin64);	   
	   break;
	 case typeint64_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(int64_t, ieee754binary64))(funcPtr))(inputs[0].param.pint64, inputs[1].param.pbin64);	   
	   break;
	 case typecharptr:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(char*, ieee754binary64))(funcPtr))(inputs[0].param.pcharptr, inputs[1].param.pbin64);	   	   
	   break;
	 }//end switch for 1-parameter when 2nd is bin64, 3d is empty
	 break;       
       case typeint32_t:
	  switch (inputs[0].type) {
	 case typeieee754binary32:
	   realOut.pbin64 = (( ieee754binary64 (*)(ieee754binary32, int32_t))(funcPtr))(inputs[0].param.pbin32, inputs[1].param.pint32);	   
	   break;
	 case typeieee754binary64:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(ieee754binary64, int32_t))(funcPtr))(inputs[0].param.pbin64, inputs[1].param.pint32);	    
	   break;
	 case typeuint8_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(uint8_t, int32_t))(funcPtr))(inputs[0].param.puint8, inputs[1].param.pint32);
	   break;
	 case typeuint16_t: 
	   realOut.pbin64 = (( ieee754binary64 (*)(uint16_t, int32_t))(funcPtr))(inputs[0].param.puint16, inputs[1].param.pint32);	   
	   break;	  
	 case typeuint32_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(uint32_t, int32_t))(funcPtr))(inputs[0].param.puint32, inputs[1].param.pint32);	  
	   break;	   
	 case typeuint64_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(uint64_t, int32_t))(funcPtr))(inputs[0].param.puint64, inputs[1].param.pint32);	   
	   break;
	 case typeint8_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(int8_t, int32_t))(funcPtr))(inputs[0].param.pint8, inputs[1].param.pint32);	   
	   break;
	 case typeint16_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(int16_t, int32_t))(funcPtr))(inputs[0].param.pint16, inputs[1].param.pint32);	  
	   break;
	 case typeint32_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(int32_t, int32_t))(funcPtr))(inputs[0].param.pint32, inputs[1].param.pint32);	   
	   break;
	 case typeint64_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(int64_t, int32_t))(funcPtr))(inputs[0].param.pint64, inputs[1].param.pint32);	   
	   break;
	 case typecharptr:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(char*, int32_t))(funcPtr))(inputs[0].param.pcharptr, inputs[1].param.pint32);	   	   
	   break;
	 }//end switch for 1-parameter when 2nd is int32, 3d is empty
	 break;
       case typeempty:
	 switch (inputs[0].type) {
	 case typeieee754binary32:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(ieee754binary32))(funcPtr))(inputs[0].param.pbin32);	   
	   break;
	 case typeieee754binary64:	   	   
	   realOut.pbin64 = (( ieee754binary64 (*)(ieee754binary64))(funcPtr))(inputs[0].param.pbin64);	   	   
     	   break;
	 case typeuint8_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(uint8_t))(funcPtr))(inputs[0].param.puint8);	  
	   break;
	 case typeuint16_t: 	  
	   realOut.pbin64 = (( ieee754binary64 (*)(uint16_t))(funcPtr))(inputs[0].param.puint16);	   
	   break;	  
	 case typeuint32_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(uint32_t))(funcPtr))(inputs[0].param.puint32);	   
	   break;	   
	 case typeuint64_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(uint64_t))(funcPtr))(inputs[0].param.puint64);	   
	   break;
	 case typeint8_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(int8_t))(funcPtr))(inputs[0].param.pint8);	   
	   break;
	 case typeint16_t:	  
	   realOut.pbin64 = (( ieee754binary64 (*)(int16_t))(funcPtr))(inputs[0].param.pint16);	   
	   break;
	 case typeint32_t:	  
	   realOut.pbin64 = (( ieee754binary64 (*)(int32_t))(funcPtr))(inputs[0].param.pint32);	   
	   break;
	 case typeint64_t:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(int64_t))(funcPtr))(inputs[0].param.pint64);
	   break;
	 case typecharptr:	   
	   realOut.pbin64 = (( ieee754binary64 (*)(char*))(funcPtr))(inputs[0].param.pcharptr);	   	   
	   break;
	 }//end switch for 1-parameter when 2nd is empty, 3d is empty 
	 break;
       }//end switch for 2-parameter where 3d is empty
       break;
     }//end switch for 3-parameter     
     
/////////////// end of bin64 output!!!!!!     
     break;// end case for bin64 output

     //output is (u)intXX_t only for 1-parameter functions
   case typeuint8_t:     
     	 switch (inputs[0].type) {
	 case typeieee754binary32:	   
	   realOut.puint8 =((uint8_t(*)(ieee754binary32))(funcPtr))(inputs[0].param.pbin32);
	   break;
	 case typeieee754binary64:
	   realOut.puint8 = ((uint8_t (*)(ieee754binary64))(funcPtr))(inputs[0].param.pbin64);	  	   
	   break;
	 case typeuint8_t:	   
	   realOut.puint8 = (( uint8_t (*)(uint8_t))(funcPtr))(inputs[0].param.puint8);	   
	   break;
	 case typeuint16_t: 	   
	   realOut.puint8 = (( uint8_t (*)(uint16_t))(funcPtr))(inputs[0].param.puint16);	   
	   break;	  
	 case typeuint32_t:	  
	   realOut.puint8 = (( uint8_t (*)(uint32_t))(funcPtr))(inputs[0].param.puint32);	   
	   break;	   
	 case typeuint64_t:	   
	   realOut.puint8 = (( uint8_t (*)(uint64_t))(funcPtr))(inputs[0].param.puint64);
	   break;
	 case typeint8_t:	   
	   realOut.puint8 = (( uint8_t (*)(int8_t))(funcPtr))(inputs[0].param.pint8);
	   break;
	 case typeint16_t:
	   realOut.puint8 = (( uint8_t (*)(int16_t))(funcPtr))(inputs[0].param.pint16);	   
	   break;
	 case typeint32_t:	  
	   realOut.puint8 = (( uint8_t (*)(int32_t))(funcPtr))(inputs[0].param.pint32);	  
	   break;
	 case typeint64_t:	   
	   realOut.puint8 = (( uint8_t (*)(int64_t))(funcPtr))(inputs[0].param.pint64);	  
	   break;
	 case typecharptr:	  
	   realOut.puint8 = (( uint8_t (*)(char*))(funcPtr))(inputs[0].param.pcharptr);	   	   
	   break;
	 }//end switch for 1-parameter when 2nd is empty, 3d is empty 
     break;// end case for uint8 output

   case typeuint16_t:
     switch (inputs[0].type) {
	 case typeieee754binary32:	   
	   realOut.puint16 = (( uint16_t (*)(ieee754binary32))(funcPtr))(inputs[0].param.pbin32);	   
	   break;
	 case typeieee754binary64:	   
	   realOut.puint16 = (( uint16_t (*)(ieee754binary64))(funcPtr))(inputs[0].param.pbin64);	   	   
	   break;
	 case typeuint8_t:	   
	   realOut.puint16 = (( uint16_t (*)(uint8_t))(funcPtr))(inputs[0].param.puint8);	   
	   break;
	 case typeuint16_t: 
	   realOut.puint16 = (( uint16_t (*)(uint16_t))(funcPtr))(inputs[0].param.puint16);	  
	   break;	  
	 case typeuint32_t:	   
	   realOut.puint16 = (( uint16_t (*)(uint32_t))(funcPtr))(inputs[0].param.puint32);	   
	   break;	   
	 case typeuint64_t:	   
	   realOut.puint16 = (( uint16_t (*)(uint64_t))(funcPtr))(inputs[0].param.puint64);
	   break;
	 case typeint8_t:	   
	   realOut.puint16 = (( uint16_t (*)(int8_t))(funcPtr))(inputs[0].param.pint8);	  
	   break;
	 case typeint16_t:	   
	   realOut.puint16 = (( uint16_t (*)(int16_t))(funcPtr))(inputs[0].param.pint16);	   
	   break;
	 case typeint32_t:	  
	   realOut.puint16 = (( uint16_t (*)(int32_t))(funcPtr))(inputs[0].param.pint32);	   
	   break;
	 case typeint64_t:	   
	   realOut.puint16 = (( uint16_t (*)(int64_t))(funcPtr))(inputs[0].param.pint64);	  
	   break;
	 case typecharptr:	   
	   realOut.puint16 = (( uint16_t (*)(char*))(funcPtr))(inputs[0].param.pcharptr);	   	   
	   break;
	 }//end switch for 1-parameter when 2nd is empty, 3d is empty
     break;// end case for uint16 output
   case typeuint32_t:
     switch (inputs[0].type) {
	 case typeieee754binary32:	   
	   realOut.puint32 = (( uint32_t (*)(ieee754binary32))(funcPtr))(inputs[0].param.pbin32);	   
	   break;
	 case typeieee754binary64:	   
	   realOut.puint32 = (( uint32_t (*)(ieee754binary64))(funcPtr))(inputs[0].param.pbin64);	   	   
	   break;
	 case typeuint8_t:	   
	   realOut.puint32 = (( uint32_t (*)(uint8_t))(funcPtr))(inputs[0].param.puint8);	   
	   break;
	 case typeuint16_t: 	   
	   realOut.puint32 = (( uint32_t (*)(uint16_t))(funcPtr))(inputs[0].param.puint16);
	   break;	  
	 case typeuint32_t:	   
	   realOut.puint32 = (( uint32_t (*)(uint32_t))(funcPtr))(inputs[0].param.puint32);	  
	   break;	   
	 case typeuint64_t:	  
	   realOut.puint32 = (( uint32_t (*)(uint64_t))(funcPtr))(inputs[0].param.puint64);	  
	   break;
	 case typeint8_t:	   
	   realOut.puint32 = (( uint32_t (*)(int8_t))(funcPtr))(inputs[0].param.pint8);	  
	   break;
	 case typeint16_t:	   
	   realOut.puint32 = (( uint32_t (*)(int16_t))(funcPtr))(inputs[0].param.pint16);	   
	   break;
	 case typeint32_t:	  
	   realOut.puint32 = (( uint32_t (*)(int32_t))(funcPtr))(inputs[0].param.pint32);	   
	   break;
	 case typeint64_t:	   
	   realOut.puint32 = (( uint32_t (*)(int64_t))(funcPtr))(inputs[0].param.pint64);	  
	   break;
	 case typecharptr:	   
	   realOut.puint32 = (( uint32_t (*)(char*))(funcPtr))(inputs[0].param.pcharptr);	   	   
	   break;
	 }//end switch for 1-parameter when 2nd is empty, 3d is empty
     break;// end case for uint32 output
   case typeuint64_t:
      switch (inputs[0].type) {
	 case typeieee754binary32:	   
	   realOut.puint64 = (( uint64_t (*)(ieee754binary32))(funcPtr))(inputs[0].param.pbin32);	  
	   break;
	 case typeieee754binary64:	  
	   realOut.puint64 = (( uint64_t (*)(ieee754binary64))(funcPtr))(inputs[0].param.pbin64);	   	   
	   break;
	 case typeuint8_t:	 
	   realOut.puint64 = (( uint64_t (*)(uint8_t))(funcPtr))(inputs[0].param.puint8);	  
	   break;
	 case typeuint16_t: 	 
	   realOut.puint64 = (( uint64_t (*)(uint16_t))(funcPtr))(inputs[0].param.puint16);	   
	   break;	  
	 case typeuint32_t:
	   realOut.puint64 = (( uint64_t (*)(uint32_t))(funcPtr))(inputs[0].param.puint32);	   
	   break;	   
	 case typeuint64_t:	   
	   realOut.puint64 = (( uint64_t (*)(uint64_t))(funcPtr))(inputs[0].param.puint64);	  
	   break;
	 case typeint8_t:	   
	   realOut.puint64 = (( uint64_t (*)(int8_t))(funcPtr))(inputs[0].param.pint8);	   
	   break;
	 case typeint16_t:	   
	   realOut.puint64 = (( uint64_t (*)(int16_t))(funcPtr))(inputs[0].param.pint16);	  
	   break;
	 case typeint32_t:	   
	   realOut.puint64 = (( uint64_t (*)(int32_t))(funcPtr))(inputs[0].param.pint32);	  
	   break;
	 case typeint64_t:	  
	   realOut.puint64 = (( uint64_t (*)(int64_t))(funcPtr))(inputs[0].param.pint64);	  
	   break;
	 case typecharptr:	  
	   realOut.puint64 = (( uint64_t (*)(char*))(funcPtr))(inputs[0].param.pcharptr);	   	   
	   break;
	 }//end switch for 1-parameter when 2nd is empty, 3d is empty
     break;// end case for uint64 output
   case typeint8_t:
     switch (inputs[0].type) {
	 case typeieee754binary32:	   
	   realOut.pint8 = (( int8_t (*)(ieee754binary32))(funcPtr))(inputs[0].param.pbin32);	  
	   break;
	 case typeieee754binary64:	  
	   realOut.pint8 = (( int8_t (*)(ieee754binary64))(funcPtr))(inputs[0].param.pbin64);	     
	   break;
	 case typeuint8_t:	  
	   realOut.pint8 = (( int8_t (*)(uint8_t))(funcPtr))(inputs[0].param.puint8);	  
	   break;
	 case typeuint16_t: 	  
	   realOut.pint8 = (( int8_t (*)(uint16_t))(funcPtr))(inputs[0].param.puint16);	 
	   break;	  
	 case typeuint32_t:	   
	   realOut.pint8 = (( int8_t (*)(uint32_t))(funcPtr))(inputs[0].param.puint32);	   
	   break;	   
	 case typeuint64_t:	   
	   realOut.pint8 = (( int8_t (*)(uint64_t))(funcPtr))(inputs[0].param.puint64);
	   break;
	 case typeint8_t:	  
	   realOut.pint8 = (( int8_t (*)(int8_t))(funcPtr))(inputs[0].param.pint8);	  
	   break;
	 case typeint16_t:	   
	   realOut.pint8 = (( int8_t (*)(int16_t))(funcPtr))(inputs[0].param.pint16);
	   break;
	 case typeint32_t:
	   realOut.pint8 = (( int8_t (*)(int32_t))(funcPtr))(inputs[0].param.pint32);
	   break;
	 case typeint64_t:	   
	   realOut.pint8 = (( int8_t (*)(int64_t))(funcPtr))(inputs[0].param.pint64);	  
	   break;
	 case typecharptr:	  
	   realOut.pint8 = (( int8_t (*)(char*))(funcPtr))(inputs[0].param.pcharptr);
	   break;
	 }//end switch for 1-parameter when 2nd is empty, 3d is empty 
     break;// end case for int8 output
   case typeint16_t:
     switch (inputs[0].type) {
	 case typeieee754binary32:	  
	   realOut.pint16 = (( int16_t (*)(ieee754binary32))(funcPtr))(inputs[0].param.pbin32);	   
	   break;
	 case typeieee754binary64:	   
	   realOut.pint16 = (( int16_t (*)(ieee754binary64))(funcPtr))(inputs[0].param.pbin64);	  	   
	   break;
	 case typeuint8_t:	   
	   realOut.pint16 = (( int16_t (*)(uint8_t))(funcPtr))(inputs[0].param.puint8);
	   break;
	 case typeuint16_t: 	  
	   realOut.pint16 = (( int16_t (*)(uint16_t))(funcPtr))(inputs[0].param.puint16);	 
	   break;	  
	 case typeuint32_t:	  
	   realOut.pint16 = (( int16_t (*)(uint32_t))(funcPtr))(inputs[0].param.puint32);	  
	   break;	   
	 case typeuint64_t:
	   realOut.pint16 = (( int16_t (*)(uint64_t))(funcPtr))(inputs[0].param.puint64);	  
	   break;
	 case typeint8_t:	  
	   realOut.pint16 = (( int16_t (*)(int8_t))(funcPtr))(inputs[0].param.pint8);	  
	   break;
	 case typeint16_t:	   
	   realOut.pint16 = (( int16_t (*)(int16_t))(funcPtr))(inputs[0].param.pint16);	  
	   break;
	 case typeint32_t:	  
	   realOut.pint16 = (( int16_t (*)(int32_t))(funcPtr))(inputs[0].param.pint32);	   
	   break;
	 case typeint64_t:
	   realOut.pint16 = (( int16_t (*)(int64_t))(funcPtr))(inputs[0].param.pint64);	   
	   break;
	 case typecharptr:	   
	   realOut.pint16 = (( int16_t (*)(char*))(funcPtr))(inputs[0].param.pcharptr);	  	   
	   break;
	 }//end switch for 1-parameter when 2nd is empty, 3d is empty 
     break;// end case for int16 output
   case typeint32_t:
     switch (inputs[0].type) {
	 case typeieee754binary32:	   
	   realOut.pint32 = (( int32_t (*)(ieee754binary32))(funcPtr))(inputs[0].param.pbin32);	   
	   break;
	 case typeieee754binary64:	  
	   realOut.pint32 = (( int32_t (*)(ieee754binary64))(funcPtr))(inputs[0].param.pbin64);	   	   
	   //	   printf("excepts in runFunc = %i \n", fetestexcept(FE_ALL_EXCEPT) );
	   break;
	 case typeuint8_t:	  
	   realOut.pint32 = (( int32_t (*)(uint8_t))(funcPtr))(inputs[0].param.puint8);	  
	   break;
	 case typeuint16_t: 	   
	   realOut.pint32 = (( int32_t (*)(uint16_t))(funcPtr))(inputs[0].param.puint16);	  
	   break;	  
	 case typeuint32_t:	   
	   realOut.pint32 = (( int32_t (*)(uint32_t))(funcPtr))(inputs[0].param.puint32);	   
	   break;	   
	 case typeuint64_t:	  
	   realOut.pint32 = (( int32_t (*)(uint64_t))(funcPtr))(inputs[0].param.puint64);	   
	   break;
	 case typeint8_t:	   
	   realOut.pint32 = (( int32_t (*)(int8_t))(funcPtr))(inputs[0].param.pint8);	  
	   break;
	 case typeint16_t:	   
	   realOut.pint32 = (( int32_t (*)(int16_t))(funcPtr))(inputs[0].param.pint16);	   
	   break;
	 case typeint32_t:	   
	   realOut.pint32 = (( int32_t (*)(int32_t))(funcPtr))(inputs[0].param.pint32);	  
	   break;
	 case typeint64_t:	  
	   realOut.pint32 = (( int32_t (*)(int64_t))(funcPtr))(inputs[0].param.pint64);	  
	   break;
	 case typecharptr:	   
	   realOut.pint32 = (( int32_t (*)(char*))(funcPtr))(inputs[0].param.pcharptr);	  	   
	   break;
	 }//end switch for 1-parameter when 2nd is empty, 3d is empty    
     break;// end case for int32 output
   case typeint64_t:
       switch (inputs[0].type) {
	 case typeieee754binary32:	  
	   realOut.pint64 = (( int64_t (*)(ieee754binary32))(funcPtr))(inputs[0].param.pbin32);	   
	   break;
	 case typeieee754binary64:	   
	   realOut.pint64 = (( int64_t (*)(ieee754binary64))(funcPtr))(inputs[0].param.pbin64);	     
	   break;
	 case typeuint8_t:	  
	   realOut.pint64 = (( int64_t (*)(uint8_t))(funcPtr))(inputs[0].param.puint8);	  
	   break;
	 case typeuint16_t: 	   
	   realOut.pint64 = (( int64_t (*)(uint16_t))(funcPtr))(inputs[0].param.puint16);	   
	   break;	  
	 case typeuint32_t:	  
	   realOut.pint64 = (( int64_t (*)(uint32_t))(funcPtr))(inputs[0].param.puint32);	  
	   break;	   
	 case typeuint64_t:
	   realOut.pint64 = (( int64_t (*)(uint64_t))(funcPtr))(inputs[0].param.puint64);
	   break;
	 case typeint8_t:	   
	   realOut.pint64 = (( int64_t (*)(int8_t))(funcPtr))(inputs[0].param.pint8);	  
	   break;
	 case typeint16_t:	   
	   realOut.pint64 = (( int64_t (*)(int16_t))(funcPtr))(inputs[0].param.pint16);	   
	   break;
	 case typeint32_t:	   
	   realOut.pint64 = (( int64_t (*)(int32_t))(funcPtr))(inputs[0].param.pint32);	  
	   break;
	 case typeint64_t:	   
	   realOut.pint64 = (( int64_t (*)(int64_t))(funcPtr))(inputs[0].param.pint64);	   
	   break;
	 case typecharptr:	   
	   realOut.pint64 = (( int64_t (*)(char*))(funcPtr))(inputs[0].param.pcharptr);	  	   
	   break;
	 }//end switch for 1-parameter when 2nd is empty, 3d is empty    
     break;// end case for int64 output
   case typefpclass:
     //there are 2 functions with the 1 parameter only (binXX)
     switch (inputs[0].type) {
     case typeieee754binary32:         
	 realOut.pfpclass = ((ieee754FPClass (*)(ieee754binary32))(funcPtr))(inputs[0].param.pbin32);	 
       break;
     case typeieee754binary64:       
       realOut.pfpclass = ((ieee754FPClass (*)(ieee754binary64))(funcPtr))(inputs[0].param.pbin32);       
       break;            
     }
     break;//end case for fpclass output
   case typeint:
     //possible variants: 0 params; 1 binXX param, 2 binXX params
     switch (inputs[1].type) {
     case typeieee754binary32:
       switch (inputs[0].type) {
	 case typeieee754binary32:
	   realOut.pint = ((int (*)(ieee754binary32, ieee754binary32))(funcPtr))(inputs[0].param.pbin32, inputs[1].param.pbin32);	  
	   break;
       case typeieee754binary64:
	 realOut.pint = ((int (*)(ieee754binary64, ieee754binary32))(funcPtr))(inputs[0].param.pbin64, inputs[1].param.pbin32);	
	 break;       
       }
       break;
     case typeieee754binary64:
        switch (inputs[0].type) {
	case typeieee754binary32:
	   realOut.pint = (( int (*)(ieee754binary32, ieee754binary64))(funcPtr))(inputs[0].param.pbin32, inputs[1].param.pbin64);	  
	   break;
	case typeieee754binary64:
	 realOut.pint = (( int (*)(ieee754binary64, ieee754binary32))(funcPtr))(inputs[0].param.pbin64, inputs[1].param.pbin64);	
	 break;       
       }       
       break;
     case typeempty:
        switch (inputs[0].type) {
	case typeieee754binary32:
	   realOut.pint = (( int (*)(ieee754binary32))(funcPtr))(inputs[0].param.pbin32);	   
	   break;
	case typeieee754binary64:
	 realOut.pint = (( int (*)(ieee754binary64))(funcPtr))(inputs[0].param.pbin64);	 
	 break;       
	case typeempty:
	  realOut.pint = (( int (*)())(funcPtr))();	 
	  break;
       }       
       break;
     }     
     break;//end case for int output
   case typeempty: //attention! returns void, but does smth with inputs!
     //possible variants: 1 param exceptionGroup, 2 param: char* and binXX
     switch (inputs[0].type) {
     case typecharptr:
       switch (inputs[1].type) {
       case typeieee754binary32:
      	 ((void(*)(char*, ieee754binary32))(funcPtr))(realOut.pcharptr, inputs[1].param.pbin32);
	 output.param.pcharptr = inputs[0].param.pcharptr;
	 result.type = typecharptr;
	 break;
       case typeieee754binary64:      
	 ((void(*)(char*, ieee754binary64))(funcPtr))(realOut.pcharptr, inputs[1].param.pbin64);
	 output.param.pcharptr = inputs[0].param.pcharptr;
	 result.type = typecharptr;
	 break;
       }
       break;
     case typeexception:
       ((void(*)(ieee754ExceptionGroupType))(funcPtr))(inputs[0].param.pexception);
       break;
     }
     break; // end of void output
   }
   result.param = realOut;
   return result;
}

char* fpClassToStr(ieee754FPClass fpclass) {
  char* name = malloc(20);
  switch (fpclass) {
  case signalingNaN: name = "signalingNaN"; break;
  case quietNaN: name = "quietNaN"; break;
  case negativeInfinity: name = "negativeInfinity"; break;
  case negativeNormal: name = "negativeNormal"; break;
  case negativeSubnormal: name = "negativeSubnormal"; break;
  case negativeZero: name = "negativeZero"; break;
  case positiveZero: name = "positiveZero"; break;
  case positiveSubnormal: name = "positiveSubnormal"; break;
  case positiveNormal: name = "positiveNormal"; break;
  case positiveInfinity: name = "positiveInfinity"; break;
  }
  return name;
}


char* showRealExceptFlags() {
  char* result = calloc(5,sizeof(char));  
  int exceptions;

  exceptions = fetestexcept(FE_ALL_EXCEPT);
  if (exceptions & FE_INVALID)
    result[0] = 'I';
  else 
    result[0] = 'i';

  if (exceptions & FE_UNDERFLOW)
    result[1] = 'U';
  else 
    result[1] = 'u';

  if (exceptions & FE_OVERFLOW)
    result[2] = 'O';
  else 
    result[2] = 'o';

  if (exceptions & FE_DIVBYZERO)
    result[3] = 'D';
  else 
    result[3] = 'd';

  if (exceptions & FE_INEXACT)
    result[4] = 'P';
  else 
    result[4] = 'p';

  return result;
}


char* printParams(paramWithType inputs []) {
  char* string = malloc(1000);
  string[0] = '\0';
  char* param = malloc(600);
  int i;
  for (i = 0; i < 3 && inputs[i].type != typeempty; i++) {
    switch (inputs[i].type) {
    case typeieee754binary32:
      if (ieee754IsNaNBinary32(inputs[i].param.pbin32)) {
	strcat(string, "NaN");
      } else {
	sprintf(param, "%f", inputs[i].param.pbin32);     
	strcat(string, param);
      }
      strcat(string, " hex: 0x");
      sprintf(param, "%x", inputs[i].param.pint32);     
      break;
    case typeieee754binary64:
      if (ieee754IsNaNBinary64(inputs[i].param.pbin64)) {
	strcat(string, "NaN");
      } else {	
	sprintf(param, "%1.30e", inputs[i].param.pbin64);     
	strcat(string, param);
      }
      strcat(string, " hex: 0x");
      sprintf(param, "%Lx", inputs[i].param.pint64);     
      break;
    case typeuint8_t:
      sprintf(param, "%u", inputs[i].param.puint8);
      break;
    case typeuint16_t:
      sprintf(param, "%u", inputs[i].param.puint16);
      break;
    case typeuint32_t:
      sprintf(param, "%u", inputs[i].param.puint32);
      break;
    case typeuint64_t:
      sprintf(param, "%Lu", inputs[i].param.puint64);
      break;
    case typeint8_t:
      sprintf(param, "%i", inputs[i].param.pint8);
      break;
    case typeint16_t:
      sprintf(param, "%i", inputs[i].param.pint16);
      break;
    case typeint32_t:
      sprintf(param, "%i", inputs[i].param.pint32);
      break;
    case typeint64_t:
      sprintf(param, "%Li", inputs[i].param.pint64);
      break;
    case typecharptr:
      param = inputs[i].param.pcharptr;     
      break;
    case typefpclass:
      param = fpClassToStr(inputs[i].param.pexception);
      break;
    }    
    strcat(string, param);
    strcat(string, "; ");
  }
  //  free(param);
  //  param = NULL;
  return string;
}

int testLine(FILE* openedFile) {
  char* funcName = getFuncName(openedFile);
  if (funcName == NULL) {
    return -1;
  }
  ieee754IOType* types = getParameterTypes(funcName);
  paramWithType inparams[3];
  paramWithType outparam;
  char* irmode;
  char* ormode;
  char* oflags;
  char* iflags;
  int i;
  int compareRes;
  int compareFlags;
  int compareRoundings;

  if (types == NULL) {
    printf("Operation %s is not supported\n", funcName);
    return 1; //Nothing to test, but there could be some lines more
  }  
  //read input Flags
  irmode = malloc(3);  
  iflags = malloc(6);
  if (readRoundingModeAndFlags(openedFile, irmode, iflags) != 0) {
    return -1;
  } 
  //read inputs
  for (i = 1; i < 4 /*&& types[i] != typeempty*/; i++) {
    inparams[i-1] = readParam(openedFile, types[i]);
  }
  //read output Flags
  ormode = malloc(3);  
  oflags = malloc(6);
  if (readRoundingModeAndFlags(openedFile, ormode, oflags) != 0) {
    return -1;
  }
  //read output
  outparam = readParam(openedFile, types[0]);  
  if (setexceptflags(iflags) != 0) {
    return 1;
  }  
  if (setRoundingModes(irmode) != 0) {
    return 1;
  }

  paramWithType realOut = runFunc(funcName, inparams, outparam); 

  compareRes = cmpFuncRes(realOut, outparam, funcName);  

  char * realExceptions = showRealExceptFlags();  
  //  printf("compareRes=%i\n", compareRes);  
  if (compareRes == 1) {
    char * paramstring = printParams(inparams);
    printf("\tfunction parameters:%s\n", paramstring);
    free(paramstring);
  }  



  if (compareFlags = checkexceptflags(oflags) != 0) {
    printf("Output exception flags for function %s are incorrect or the operation changed them! Input flags: %s; output flags: %s; real flags are: %s\n", funcName, iflags, oflags, realExceptions);
    char * paramstring = printParams(inparams);
    printf("\tfunction parameters:%s\n", paramstring);
    free(paramstring);
  }
  if (compareRoundings = checkRoundingModes(ormode) != 0) {
    printf("Output rounding mode for function %s is incorrect, or the operation changed it! Input rounding mode: %s; output rounding mode: %s\n", funcName, irmode, ormode);
    char * paramstring = printParams(inparams);
    printf("\tfunction parameters:%s\n", paramstring);
    free(paramstring);
  }  
  free(irmode);
  free(iflags);
  free(ormode);
  free(oflags);
  free(funcName);  
  free(realExceptions);
  return compareRes;//0-file has more lines, test passed successfully, 1 - file has more lines, test failed, -1 - end of file
}

int testFile(FILE* testFileName) {   
  //here should be a loop
  uint8_t boolFlag = 1; // test success
  uint8_t repeatLoop = 1;
  int8_t testRes;
  while (repeatLoop) {        
    testRes = testLine(testFileName);    
    boolFlag = boolFlag & (testRes == 0);
    repeatLoop = (testRes != -1);
  }
  return 1;
}

int main(int argc, char **argv) {  
  FILE* testFileName;  
   if (argc == 0) {
     printf("specify the filename\n");
   exit(EXIT_FAILURE);
  } else {      
       testFileName = fopen(argv[1], "r");
       if (testFileName == NULL) {
	 printf("Error opening file! Test failed\n");
	 exit(EXIT_FAILURE);
       } else {	
	 testFile(testFileName);
	 return 0;
       }   
   }
}

int setexceptflags(char* strflags) {
#pragma STDC FENV_ACCESS ON  
  int excepts = 0;
  feclearexcept(FE_ALL_EXCEPT);
  if (strstr(strflags, "D") != NULL)    
    excepts = excepts | FE_DIVBYZERO;  
  if (strstr(strflags, "U") != NULL) 
    excepts = excepts | FE_UNDERFLOW;    
  if (strstr(strflags, "I") != NULL)        
    excepts = excepts | FE_INVALID;    
  if (strstr(strflags, "P") != NULL)
    excepts = excepts | FE_INEXACT;  
  if (strstr(strflags, "O") != NULL)
    excepts = excepts | FE_OVERFLOW;  
  feraiseexcept(excepts);
  return 0;
}

int checkexceptflags(char* strflags) {
#pragma STDC FENV_ACCESS ON
  int excepts = 0;  
  if (strstr(strflags, "D") != NULL)
    excepts = excepts | FE_DIVBYZERO;
  if (strstr(strflags, "U") != NULL)
    excepts = excepts | FE_UNDERFLOW;
  if (strstr(strflags, "I") != NULL)
    excepts = excepts | FE_INVALID;
  if (strstr(strflags, "P") != NULL)
    excepts = excepts | FE_INEXACT;
  if (strstr(strflags, "O") != NULL)
    excepts = excepts | FE_OVERFLOW;
  int exceptActual = fetestexcept(FE_ALL_EXCEPT);  
 if (excepts != exceptActual) {  
    return 1;  
 }       
 return 0;
}

int setRoundingModes(char* rmode) {
#pragma STDC FENV_ACCESS ON
  fesetround(0);
  int rounding = FE_TONEAREST; 
  if (strstr(rmode, "RN") != NULL)
    rounding = FE_TONEAREST;
  if (strstr(rmode, "RD") != NULL)
    rounding = FE_DOWNWARD;
  if (strstr(rmode, "RZ") != NULL)
    rounding = FE_TOWARDZERO;
  if (strstr(rmode, "RU") != NULL)
    rounding = FE_UPWARD;
  if (fesetround(rounding) != 0) {
    printf("problems with setting rounding mode\n");
    return 1;
  }
return 0;  
}

int checkRoundingModes(char* rmode) {
#pragma STDC FENV_ACCESS ON
  int savedRounding;
  int rounding = FE_TONEAREST; 
  if (strstr(rmode, "RN") != NULL)
    rounding = FE_TONEAREST;
  if (strstr(rmode, "RD") != NULL)
    rounding = FE_DOWNWARD;
  if (strstr(rmode, "RZ") != NULL)
    rounding = FE_TOWARDZERO;
  if (strstr(rmode, "RU") != NULL)
    rounding = FE_UPWARD;
  savedRounding = fegetround();  
  if (savedRounding != rounding) {    
    return -1;  
  }
  return 0;
}

int cmpFuncRes(paramWithType realOut, paramWithType fromFile, char* funcName) { 
  uint64_t d1, d2;
  uint32_t f1, f2;
  switch (realOut.type) {
  case typeieee754binary32:    
    f1 = floatToMemoryNotation(realOut.param.pbin32);
    f2 = floatToMemoryNotation(fromFile.param.pbin32);    
    if (f1 == f2) {      
      return 0;
    } else {         
      int nan1, nan2;
      IS_NAN_QUIET32(realOut.param.pbin32, &nan1);
      IS_NAN_QUIET32(fromFile.param.pbin32, &nan2);
      if (!nan1 && ! nan2)
	printf("Expected result for function %s is %f(0x%x), but your value is %f(0x%x)\n", funcName, realOut.param.pbin32, f1, fromFile.param.pbin32, f2);
      else 
	if (nan1 && nan2)
	  printf("Expected result for function %s is 0x%x, but your value is 0x%x\n", funcName, f1, f2);
	else if (nan1)
	  printf("Expected result for function %s is 0x%x, but your value is %f(0x%x)\n", funcName, f1, fromFile.param.pbin32, f2);
	else 
	  printf("Expected result for function %s is %f(0x%x), but your value is 0x%x\n", funcName, realOut.param.pbin32, f1, f2);
      
    } 
    break;
  case typeieee754binary64:    
    d1 = doubleToMemoryNotation(realOut.param.pbin64);
    d2 = doubleToMemoryNotation(fromFile.param.pbin64);
    if (d1 == d2) {           
      return 0;
    } else {
      int nan1, nan2;   
      IS_NAN_QUIET64(realOut.param.pbin64, &nan1);
      IS_NAN_QUIET64(fromFile.param.pbin64, &nan2);     
      if (!nan1 && ! nan2) {
	printf("Expected result for function %s is %1.30e(0x%Lx), but your value is %1.30e(0x%Lx)\n", funcName,  realOut.param.pbin64, d1, fromFile.param.pbin64, d2);
      }
      else 
	if (nan1 & nan2)
	  printf("Expected result for function %s is 0x%Lx, but your value is 0x%Lx\n", funcName, d1, d2);
	else if (nan1) 
	  printf("Expected result for function %s is 0x%Lx, but your value is %1.30e(0x%Lx)\n", funcName, d1, fromFile.param.pbin64, d2);
	else
	  printf("Expected result for function %s is %1.30e(0x%Lx), but your value is 0x%Lx\n", funcName, realOut.param.pbin64, d1, d2);
    }
    break;
  case typeuint8_t:
    if (realOut.param.puint8 == fromFile.param.puint8) {
      return 0;
    } else {
      printf("Expected result for function %s is %u, but your value is %u\n", funcName, realOut.param.puint8, fromFile.param.puint8);
    }
    break;
  case typeuint16_t:
    if (realOut.param.puint16 == fromFile.param.puint16) {
      return 0;
    } else {
      printf("Expected result for function %s is %u, but your value is %u\n", funcName, realOut.param.puint16, fromFile.param.puint16);
    }
    break;
  case typeuint32_t:
    if (realOut.param.puint32 == fromFile.param.puint32) {
      return 0;
    } else {
      printf("Expected result for function %s is %u, but your value is %u\n", funcName, realOut.param.puint32, fromFile.param.puint32);
    }
    break;
  case typeuint64_t:
    if (realOut.param.puint64 == fromFile.param.puint64) {
      return 0;
    } else {
      printf("Expected result for function %s is %Lu, but your value is %Lu\n", funcName, realOut.param.puint64, fromFile.param.puint64);
    }
    break;
  case typeint8_t:
    if (realOut.param.pint8 == fromFile.param.pint8) {
      return 0;
    } else {
      printf("Expected result for function %s is %i, but your value is %i\n", funcName, realOut.param.pint8, fromFile.param.pint8);  
    }  
    break;
  case typeint16_t:
    if (realOut.param.pint16 == fromFile.param.pint16) {
      return 0;
    } else {
      printf("Expected result for function %s is %i, but your value is %i\n", funcName, realOut.param.pint16, fromFile.param.pint16);
    }
    break;
  case typeint32_t:
    if (realOut.param.pint32 == fromFile.param.pint32) {
      return 0;
    } else {
      printf("Expected result for function %s is %i, but your value is %i\n", funcName, realOut.param.pint32, fromFile.param.pint32);
    }
    break;
  case typeint64_t:
    if (realOut.param.pint64 == fromFile.param.pint64) {
      return 0;
    } else {
      printf("Expected result for function %s is %li, but your value is %li\n", funcName, realOut.param.pint64, fromFile.param.pint64);
    }
    break; 
  case typecharptr:
    if (strcmp(realOut.param.pcharptr, fromFile.param.pcharptr)) {
      return 0;
    } else {
      printf("Expected result for function %s is %i, but your value is %i\n", funcName, realOut.param.pcharptr, fromFile.param.pcharptr);
    }
    break;
  case typefpclass:
    if (realOut.param.pfpclass == fromFile.param.pfpclass) {
      return 0;
    } else {
      printf("Expected result for function %s is %s, but your value is %s\n", funcName, fpClassToStr(realOut.param.pfpclass), fpClassToStr(fromFile.param.pfpclass));
    }
    break;
  case typeint:
    if (realOut.param.pint == fromFile.param.pint) {
      return 0;
    } else {
      printf("Expected result for function %s is %i, but your value is %i\n", funcName, realOut.param.pint, fromFile.param.pint);
    }
    break;
  }  
  return 1;//0 if equal, 1 otherwise;  
}


