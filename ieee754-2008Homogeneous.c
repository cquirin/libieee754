//This is a file for implementation the homogeneous operations, declared in IEEE754-2008 standard

#include <ieee754-2008.h>
#include <ieee754-2008-macros.h>


ieee754binary32 ieee754Binary32RemainderBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
	return REMAINDER32(b1, b2);
}
ieee754binary64 ieee754Binary64RemainderBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2){
	return REMAINDER64(b1, b2);
}

ieee754binary32 ieee754Binary32NextUpBinary32(ieee754binary32 b) {
	return NEXT_UP32(b);
}
ieee754binary64 ieee754Binary64NextUpBinary64(ieee754binary64 b) {
	return NEXT_UP64(b);
}

ieee754binary32 ieee754Binary32NextDownBinary32(ieee754binary32 b) {
	return -NEXT_UP32(-b);
}
ieee754binary64 ieee754Binary64NextDownBinary64(ieee754binary64 b) {
	return -NEXT_UP64(-b);
}

ieee754binary32 ieee754Binary32MinNumBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
	return MIN32(b1, b2);
}
ieee754binary64 ieee754Binary64MinNumBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
	return MIN64(b1, b2);
}

ieee754binary32 ieee754Binary32MaxNumBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
	return MAX32(b1, b2);
}
ieee754binary64 ieee754Binary64MaxNumBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
	return MAX64(b1, b2);
}

ieee754binary32 ieee754Binary32MinNumMagBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
	if (ABS32(b1) < ABS32(b2)) {
		return b1;
	} else if (ABS32(b1) > ABS32(b2)) {
		return b2;
	} else return ieee754Binary32MinNumBinary32Binary32(b1, b2);
}
ieee754binary64 ieee754Binary64MinNumMagBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
	if (ABS64(b1) < ABS64(b2)) {
		return b1;
	} else if (ABS64(b1) > ABS64(b2)) {
		return b2;
	} else return ieee754Binary64MinNumBinary64Binary64(b1, b2);
}

ieee754binary32 ieee754Binary32MaxNumMagBinary32Binary32(ieee754binary32 b1, ieee754binary32 b2) {
	if (ABS32(b1) < ABS32(b2)) {
		return b2;
	} else if (ABS32(b1) > ABS32(b2)) {
		return b1;
	} else return ieee754Binary32MaxNumBinary32Binary32(b1, b2);
}
ieee754binary64 ieee754Binary64MaxNumMagBinary64Binary64(ieee754binary64 b1, ieee754binary64 b2) {
	if (ABS64(b1) < ABS64(b2)) {
		return b2;
	} else if (ABS64(b1) > ABS64(b2)) {
		return b1;
	} else return ieee754Binary64MaxNumBinary64Binary64(b1, b2);
}
